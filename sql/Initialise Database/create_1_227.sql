-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.30 - MySQL Community Server (GPL) by Remi
-- Server OS:                    Linux
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2013-04-15 17:29:34
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table create_new.accessory
CREATE TABLE IF NOT EXISTS `accessory` (
  `AccessoryID` int(11) NOT NULL AUTO_INCREMENT,
  `AccessoryName` varchar(60) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UnitTypeID` int(11) NOT NULL,
  PRIMARY KEY (`AccessoryID`),
  KEY `IDX_unit_type_ModifiedUserID_FK` (`ModifiedUserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.accessory: ~0 rows (approximately)
/*!40000 ALTER TABLE `accessory` DISABLE KEYS */;
/*!40000 ALTER TABLE `accessory` ENABLE KEYS */;


-- Dumping structure for table create_new.accessory_model
CREATE TABLE IF NOT EXISTS `accessory_model` (
  `AccessoryModelID` int(10) NOT NULL AUTO_INCREMENT,
  `ModelID` int(10) NOT NULL DEFAULT '0',
  `AccessoryID` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`AccessoryModelID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.accessory_model: ~0 rows (approximately)
/*!40000 ALTER TABLE `accessory_model` DISABLE KEYS */;
/*!40000 ALTER TABLE `accessory_model` ENABLE KEYS */;


-- Dumping structure for table create_new.alternative_fields
CREATE TABLE IF NOT EXISTS `alternative_fields` (
  `alternativeFieldID` int(11) NOT NULL AUTO_INCREMENT,
  `primaryFieldID` int(11) NOT NULL,
  `alternativeFieldName` varchar(250) NOT NULL,
  `status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `brandID` int(11) NOT NULL,
  PRIMARY KEY (`alternativeFieldID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.alternative_fields: ~0 rows (approximately)
/*!40000 ALTER TABLE `alternative_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `alternative_fields` ENABLE KEYS */;


-- Dumping structure for table create_new.appointment
CREATE TABLE IF NOT EXISTS `appointment` (
  `AppointmentID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) DEFAULT NULL,
  `UserID` int(11) DEFAULT NULL,
  `AppointmentDate` date DEFAULT NULL,
  `AppointmentTime` varchar(8) DEFAULT NULL COMMENT 'May be AM, PM, ANY or hh:mm:ss',
  `AppointmentType` varchar(30) DEFAULT NULL,
  `EngineerCode` char(3) DEFAULT NULL,
  `OutCardLeft` tinyint(1) NOT NULL DEFAULT '0',
  `SBAppointID` int(11) DEFAULT NULL,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `NonSkylineJobID` int(11) DEFAULT NULL,
  `DiaryAllocationID` int(11) DEFAULT NULL,
  `AppointmentStartTime` time DEFAULT NULL,
  `AppointmentEndTime` time DEFAULT NULL,
  `ServiceProviderEngineerID` int(11) DEFAULT NULL,
  `CreatedUserID` int(11) DEFAULT NULL,
  `AppointmentOrphaned` enum('Yes','No') DEFAULT NULL,
  `AppointmentStatusID` int(11) DEFAULT NULL,
  `Notes` varchar(1000) DEFAULT NULL,
  `AppointmentChangeReason` varchar(1000) DEFAULT NULL,
  `WallMount` tinyint(1) DEFAULT NULL,
  `MenRequired` tinyint(2) DEFAULT NULL,
  `BookedBy` varchar(60) DEFAULT NULL,
  `ServiceProviderSkillsetID` int(11) DEFAULT NULL,
  `Duration` int(11) DEFAULT NULL,
  `ScreenSize` tinyint(3) DEFAULT NULL,
  `orphan` enum('Yes','No') DEFAULT 'No',
  `importance` tinyint(2) DEFAULT '0',
  `ViamenteStartTime` datetime DEFAULT NULL,
  `CreatedTimeStamp` datetime DEFAULT NULL,
  `ViamenteTravelTime` int(11) DEFAULT NULL,
  `SBusercode` varchar(6) DEFAULT NULL,
  `ViamenteUnreached` tinyint(4) DEFAULT '0',
  `OnlineDiarySkillSet` int(11) DEFAULT NULL,
  `ViamenteDepartTime` datetime DEFAULT NULL,
  `ViamenteServiceTime` datetime DEFAULT NULL,
  `ViamenteReturnTime` datetime DEFAULT NULL,
  `ForceEngineerToViamente` tinyint(1) DEFAULT '0',
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `AppointmentStartTime2` time DEFAULT NULL,
  `AppointmentEndTime2` time DEFAULT NULL,
  `CompletedBy` enum('Remote Engineer','Manual','Out Card Left') DEFAULT NULL,
  `CompletionEngineerUserCode` char(3) DEFAULT NULL,
  `ArrivalDateTime` datetime DEFAULT NULL,
  `PartsAttached` tinyint(1) NOT NULL DEFAULT '0',
  `CompletionDateTime` datetime DEFAULT NULL,
  `Latitude` decimal(17,14) DEFAULT NULL,
  `Longitude` decimal(17,14) DEFAULT NULL,
  `ViamenteExclude` enum('Yes','No') DEFAULT 'No',
  `BreakStartSec` int(11) DEFAULT NULL,
  `BreakDurationSec` int(11) DEFAULT NULL,
  `CustContactTime` int(3) DEFAULT '60',
  `CustContactType` enum('None','SMS','Phone') DEFAULT 'SMS',
  `SortOrder` int(2) DEFAULT NULL,
  `ConfirmedByUser` enum('Confirmed','Not Confirmed') DEFAULT 'Not Confirmed',
  `TempSpecified` enum('Yes','No') NOT NULL DEFAULT 'No',
  `GeoTagModiefiedByUser` enum('Yes','No') NOT NULL DEFAULT 'No',
  `ForceAddReason` varchar(100) DEFAULT NULL,
  `SamsungOneTouchChangeReason` varchar(100) DEFAULT NULL,
  `LockPartsEngineer` enum('Yes','No') NOT NULL DEFAULT 'No',
  `Cancelled` enum('No','Yes') DEFAULT 'No',
  PRIMARY KEY (`AppointmentID`),
  KEY `IDX_appointment_1` (`AppointmentDate`),
  KEY `IDX_appointment_2` (`ServiceProviderID`),
  KEY `IDX_appointment_3` (`NonSkylineJobID`),
  KEY `IDX_appointment_4` (`DiaryAllocationID`),
  KEY `IDX_appointment_5` (`ServiceProviderEngineerID`),
  KEY `IDX_appointment_6` (`ServiceProviderID`),
  KEY `IDX_appointment_7` (`NonSkylineJobID`),
  KEY `IDX_appointment_8` (`DiaryAllocationID`),
  KEY `IDX_appointment_9` (`AppointmentDate`),
  KEY `IDX_appointment_10` (`ServiceProviderEngineerID`),
  KEY `IDX_appointment_JobID_FK` (`JobID`),
  KEY `IDX_appointment_UserID_FK` (`UserID`),
  CONSTRAINT `job_TO_appointment` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`),
  CONSTRAINT `user_TO_appointment` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.appointment: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment` ENABLE KEYS */;


-- Dumping structure for table create_new.appointment_allocation_slot
CREATE TABLE IF NOT EXISTS `appointment_allocation_slot` (
  `AppointmentAllocationSlotID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(50) DEFAULT NULL,
  `TimeFrom` time DEFAULT NULL,
  `TimeTo` time DEFAULT NULL,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `Type` enum('AM','PM','ANY') DEFAULT NULL,
  `Colour` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`AppointmentAllocationSlotID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.appointment_allocation_slot: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_allocation_slot` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_allocation_slot` ENABLE KEYS */;


-- Dumping structure for table create_new.appointment_source
CREATE TABLE IF NOT EXISTS `appointment_source` (
  `AppointmentSourceID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`AppointmentSourceID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.appointment_source: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_source` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_source` ENABLE KEYS */;


-- Dumping structure for table create_new.appointment_status
CREATE TABLE IF NOT EXISTS `appointment_status` (
  `AppointmentStatusID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`AppointmentStatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.appointment_status: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_status` ENABLE KEYS */;


-- Dumping structure for table create_new.appointment_type
CREATE TABLE IF NOT EXISTS `appointment_type` (
  `AppointmentTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(60) DEFAULT NULL,
  `Status` enum('Active','In-active') DEFAULT 'Active',
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`AppointmentTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.appointment_type: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_type` DISABLE KEYS */;
INSERT IGNORE INTO `appointment_type` (`AppointmentTypeID`, `Type`, `Status`, `CreatedDate`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 'Delivery', 'Active', '2012-10-08 14:40:24', NULL, '0000-00-00 00:00:00'),
	(2, 'Installation', 'Active', '2012-10-30 13:28:27', NULL, '0000-00-00 00:00:00'),
	(3, 'Inspection', 'Active', '2012-10-30 13:28:31', NULL, '0000-00-00 00:00:00'),
	(4, 'Collection', 'Active', '2012-10-29 14:01:59', NULL, '0000-00-00 00:00:00'),
	(5, 'Repair', 'Active', '2012-10-08 14:41:48', NULL, '0000-00-00 00:00:00'),
	(6, 'Send-in', 'Active', '2012-10-08 14:42:08', NULL, '0000-00-00 00:00:00'),
	(7, 'Wallmount', 'Active', '2012-10-08 14:53:39', 1, '0000-00-00 00:00:00'),
	(8, 'Delivery (2 Person)', 'Active', '2012-10-29 13:59:54', NULL, '0000-00-00 00:00:00'),
	(9, 'Collection (2 Person)', 'Active', '2012-10-29 14:00:28', NULL, '0000-00-00 00:00:00'),
	(10, 'Inspection (2 Person)', 'Active', '2012-10-30 13:44:10', NULL, '0000-00-00 00:00:00'),
	(11, 'Installation (2 Person)', 'Active', '2012-10-30 13:44:14', NULL, '0000-00-00 00:00:00'),
	(12, 'Wall Mount (2 Person)', 'Active', '2012-10-29 14:01:23', NULL, '0000-00-00 00:00:00'),
	(13, 'Repair (2 Person)', 'Active', '2012-10-29 14:01:38', NULL, '0000-00-00 00:00:00'),
	(14, 'Return', 'Active', '2012-11-22 12:09:41', NULL, '0000-00-00 00:00:00'),
	(15, 'Fit Parts', 'Active', '2012-11-26 14:51:37', NULL, '0000-00-00 00:00:00'),
	(16, 'Fit Parts (2 Person)', 'Active', '2012-11-26 14:51:50', NULL, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `appointment_type` ENABLE KEYS */;


-- Dumping structure for table create_new.assigned_permission
CREATE TABLE IF NOT EXISTS `assigned_permission` (
  `PermissionID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  `AccessLevel` char(6) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PermissionID`,`RoleID`),
  UNIQUE KEY `TUC_assigned_permission_1` (`RoleID`,`PermissionID`),
  KEY `IDX_assigned_permission_PermissionID_FK` (`PermissionID`),
  KEY `IDX_assigned_permission_RoleID_FK` (`RoleID`),
  KEY `IDX_assigned_permission_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `permission_TO_assigned_permission` FOREIGN KEY (`PermissionID`) REFERENCES `permission` (`PermissionID`),
  CONSTRAINT `role_TO_assigned_permission` FOREIGN KEY (`RoleID`) REFERENCES `role` (`RoleID`),
  CONSTRAINT `user_TO_assigned_permission` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.assigned_permission: ~0 rows (approximately)
/*!40000 ALTER TABLE `assigned_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `assigned_permission` ENABLE KEYS */;


-- Dumping structure for table create_new.associated_manufacturers
CREATE TABLE IF NOT EXISTS `associated_manufacturers` (
  `AssociatedManufacturersID` int(10) NOT NULL AUTO_INCREMENT,
  `ParentManufacturerID` int(10) DEFAULT NULL,
  `ChildManufacturerID` int(10) DEFAULT NULL,
  PRIMARY KEY (`AssociatedManufacturersID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.associated_manufacturers: ~0 rows (approximately)
/*!40000 ALTER TABLE `associated_manufacturers` DISABLE KEYS */;
/*!40000 ALTER TABLE `associated_manufacturers` ENABLE KEYS */;


-- Dumping structure for table create_new.audit
CREATE TABLE IF NOT EXISTS `audit` (
  `AuditID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) DEFAULT NULL,
  `AuditTrailActionID` int(11) NOT NULL,
  `Description` varchar(40) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`AuditID`),
  KEY `IDX_audit_JobID_FK` (`JobID`),
  KEY `IDX_audit_AuditTrailActionID_FK` (`AuditTrailActionID`),
  KEY `IDX_audit_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `audit_trail_action_TO_audit` FOREIGN KEY (`AuditTrailActionID`) REFERENCES `audit_trail_action` (`AuditTrailActionID`),
  CONSTRAINT `job_TO_audit` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`),
  CONSTRAINT `user_TO_audit` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.audit: ~0 rows (approximately)
/*!40000 ALTER TABLE `audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit` ENABLE KEYS */;


-- Dumping structure for table create_new.audit_new
CREATE TABLE IF NOT EXISTS `audit_new` (
  `AuditID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `TableName` varchar(100) NOT NULL,
  `Action` enum('update','delete','insert') NOT NULL,
  `PrimaryID` varchar(50) NOT NULL,
  `FieldName` varchar(100) DEFAULT NULL,
  `OldValue` text,
  `NewValue` text,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`AuditID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.audit_new: ~0 rows (approximately)
/*!40000 ALTER TABLE `audit_new` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_new` ENABLE KEYS */;


-- Dumping structure for table create_new.audit_trail_action
CREATE TABLE IF NOT EXISTS `audit_trail_action` (
  `AuditTrailActionID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `Action` varchar(255) NOT NULL,
  `ActionCode` int(11) NOT NULL,
  `Type` enum('Process','Action','Edit') NOT NULL,
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`AuditTrailActionID`),
  KEY `IDX_audit_trail_action_BrandID_FK` (`BrandID`),
  CONSTRAINT `brand_TO_audit_trail_action` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.audit_trail_action: ~0 rows (approximately)
/*!40000 ALTER TABLE `audit_trail_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_trail_action` ENABLE KEYS */;


-- Dumping structure for table create_new.bought_out_guarantee
CREATE TABLE IF NOT EXISTS `bought_out_guarantee` (
  `BoughtOutGuaranteeID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) NOT NULL,
  `ClientID` int(11) NOT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `RepairSkillID` int(11) DEFAULT NULL,
  `UnitTypeID` int(11) DEFAULT NULL,
  `ModelID` int(11) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`BoughtOutGuaranteeID`),
  KEY `IDX_bought_out_guarantee_NetworkID_FK` (`NetworkID`),
  KEY `IDX_bought_out_guarantee_ClientID_FK` (`ClientID`),
  KEY `IDX_bought_out_guarantee_ManufacturerID_FK` (`ManufacturerID`),
  KEY `IDX_bought_out_guarantee_RepairSkillID_FK` (`RepairSkillID`),
  KEY `IDX_bought_out_guarantee_UnitTypeID_FK` (`UnitTypeID`),
  KEY `IDX_bought_out_guarantee_ModelID_FK` (`ModelID`),
  KEY `IDX_bought_out_guarantee_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `client_TO_bought_out_guarantee` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `manufacturer_TO_bought_out_guarantee` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `model_TO_bought_out_guarantee` FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`),
  CONSTRAINT `network_TO_bought_out_guarantee` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `repair_skill_TO_bought_out_guarantee` FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`),
  CONSTRAINT `unit_type_TO_bought_out_guarantee` FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`),
  CONSTRAINT `user_TO_bought_out_guarantee` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.bought_out_guarantee: ~0 rows (approximately)
/*!40000 ALTER TABLE `bought_out_guarantee` DISABLE KEYS */;
/*!40000 ALTER TABLE `bought_out_guarantee` ENABLE KEYS */;


-- Dumping structure for table create_new.branch
CREATE TABLE IF NOT EXISTS `branch` (
  `BranchID` int(11) NOT NULL AUTO_INCREMENT,
  `BranchName` varchar(40) DEFAULT NULL,
  `BranchNumber` varchar(40) DEFAULT NULL,
  `BranchType` enum('Store','Extra','Call Centre','Website') DEFAULT NULL,
  `BuildingNameNumber` varchar(40) DEFAULT NULL,
  `Street` varchar(40) DEFAULT NULL,
  `LocalArea` varchar(40) DEFAULT NULL,
  `TownCity` varchar(60) DEFAULT NULL,
  `CountyID` int(11) DEFAULT NULL,
  `CountryID` int(11) DEFAULT NULL,
  `PostalCode` varchar(10) DEFAULT NULL,
  `ContactPhone` varchar(40) DEFAULT NULL,
  `ContactPhoneExt` varchar(10) DEFAULT NULL,
  `ServiceManager` varchar(250) DEFAULT NULL,
  `ContactEmail` varchar(40) DEFAULT NULL,
  `UseAddressProgram` enum('Yes','No') DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ServiceAppraisalRequired` enum('Yes','No') NOT NULL DEFAULT 'No',
  `DefaultServiceProvider` int(11) DEFAULT NULL,
  `ThirdPartyServiceProvider` int(11) DEFAULT NULL,
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ContactFax` varchar(40) DEFAULT NULL,
  `AccountNo` varchar(40) DEFAULT NULL,
  `CurrentLocationOfProduct` enum('Customer','Branch','Service Provider','Supplier','other') DEFAULT NULL,
  `OriginalRetailerFormElement` enum('FreeText','Dropdown') DEFAULT 'FreeText',
  PRIMARY KEY (`BranchID`),
  KEY `IDX_branch_CountyID_FK` (`CountyID`),
  KEY `IDX_branch_CountryID_FK` (`CountryID`),
  KEY `IDX_branch_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `FK_branch_service_provider` (`DefaultServiceProvider`),
  KEY `branch_TO_service_provider1` (`ThirdPartyServiceProvider`),
  CONSTRAINT `branch_TO_service_provider1` FOREIGN KEY (`ThirdPartyServiceProvider`) REFERENCES `service_provider` (`ServiceProviderID`),
  CONSTRAINT `country_TO_branch` FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `county_TO_branch` FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`),
  CONSTRAINT `FK_branch_service_provider` FOREIGN KEY (`DefaultServiceProvider`) REFERENCES `service_provider` (`ServiceProviderID`),
  CONSTRAINT `user_TO_branch` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.branch: ~0 rows (approximately)
/*!40000 ALTER TABLE `branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `branch` ENABLE KEYS */;


-- Dumping structure for table create_new.brand
CREATE TABLE IF NOT EXISTS `brand` (
  `BrandID` int(11) NOT NULL,
  `NetworkID` int(11) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  `BrandName` varchar(40) DEFAULT NULL,
  `Acronym` varchar(30) DEFAULT NULL,
  `BrandLogo` varchar(255) DEFAULT NULL,
  `Skin` varchar(20) DEFAULT NULL,
  `AutoSendEmails` enum('1','0') DEFAULT '1',
  `EmailType` enum('Generic','CRM','Tracker') NOT NULL DEFAULT 'Generic',
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CustomerTermsConditions` text,
  PRIMARY KEY (`BrandID`),
  KEY `IDX_brand_ClientID_FK` (`ClientID`),
  KEY `IDX_brand_NetworkID_FK` (`NetworkID`),
  KEY `IDX_brand_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `client_TO_brand` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `network_TO_brand` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `user_TO_brand` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.brand: ~0 rows (approximately)
/*!40000 ALTER TABLE `brand` DISABLE KEYS */;
/*!40000 ALTER TABLE `brand` ENABLE KEYS */;


-- Dumping structure for table create_new.brand_branch
CREATE TABLE IF NOT EXISTS `brand_branch` (
  `BrandBranchID` int(11) NOT NULL AUTO_INCREMENT,
  `BranchID` int(11) NOT NULL,
  `BrandID` int(11) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`BrandBranchID`),
  UNIQUE KEY `TUC_brand_branch_1` (`BrandID`,`BranchID`),
  KEY `IDX_brand_branch_BrandID_FK` (`BrandID`),
  KEY `IDX_brand_branch_BranchID_FK` (`BranchID`),
  KEY `IDX_brand_branch_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `branch_TO_brand_branch` FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`),
  CONSTRAINT `brand_TO_brand_branch` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`) ON UPDATE CASCADE,
  CONSTRAINT `user_TO_brand_branch` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.brand_branch: ~0 rows (approximately)
/*!40000 ALTER TABLE `brand_branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `brand_branch` ENABLE KEYS */;


-- Dumping structure for table create_new.central_service_allocation
CREATE TABLE IF NOT EXISTS `central_service_allocation` (
  `CentralServiceAllocationID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) NOT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `RepairSkillID` int(11) NOT NULL,
  `ServiceProviderID` int(11) NOT NULL,
  `JobTypeID` int(11) DEFAULT NULL,
  `ServiceTypeID` int(11) DEFAULT NULL,
  `UnitTypeID` int(11) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  `CountyID` int(11) DEFAULT NULL,
  `CountryID` int(11) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CentralServiceAllocationID`),
  KEY `IDX_central_service_allocation_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_central_service_allocation_NetworkID_FK` (`NetworkID`),
  KEY `IDX_central_service_allocation_ManufacturerID_FK` (`ManufacturerID`),
  KEY `IDX_central_service_allocation_RepairSkillID_FK` (`RepairSkillID`),
  KEY `IDX_central_service_allocation_ServiceProviderID_FK` (`ServiceProviderID`),
  KEY `IDX_central_service_allocation_JobTypeID_FK` (`JobTypeID`),
  KEY `IDX_central_service_allocation_ServiceTypeID_FK` (`ServiceTypeID`),
  KEY `IDX_central_service_allocation_UnitTypeID_FK` (`UnitTypeID`),
  KEY `IDX_central_service_allocation_ClientID_FK` (`ClientID`),
  KEY `IDX_central_service_allocation_CountyID_FK` (`CountyID`),
  KEY `IDX_central_service_allocation_CountryID_FK` (`CountryID`),
  CONSTRAINT `client_TO_central_service_allocation` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `country_TO_central_service_allocation` FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `county_TO_central_service_allocation` FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`),
  CONSTRAINT `job_type_TO_central_service_allocation` FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`),
  CONSTRAINT `manufacturer_TO_central_service_allocation` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `network_TO_central_service_allocation` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `repair_skill_TO_central_service_allocation` FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`),
  CONSTRAINT `service_provider_TO_central_service_allocation` FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`),
  CONSTRAINT `service_type_TO_central_service_allocation` FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`),
  CONSTRAINT `unit_type_TO_central_service_allocation` FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`),
  CONSTRAINT `user_TO_central_service_allocation` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.central_service_allocation: ~0 rows (approximately)
/*!40000 ALTER TABLE `central_service_allocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `central_service_allocation` ENABLE KEYS */;


-- Dumping structure for table create_new.claim_response
CREATE TABLE IF NOT EXISTS `claim_response` (
  `ClaimResponseID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) NOT NULL,
  `ErrorCode` varchar(18) DEFAULT NULL,
  `ErrorDescription` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ClaimResponseID`),
  KEY `IDX_claim_response_JobID_FK` (`JobID`),
  CONSTRAINT `job_TO_claim_response` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.claim_response: ~0 rows (approximately)
/*!40000 ALTER TABLE `claim_response` DISABLE KEYS */;
/*!40000 ALTER TABLE `claim_response` ENABLE KEYS */;


-- Dumping structure for table create_new.client
CREATE TABLE IF NOT EXISTS `client` (
  `ClientID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `DefaultBranchID` int(11) DEFAULT NULL,
  `ClientName` varchar(40) DEFAULT NULL,
  `AccountNumber` text,
  `GroupName` varchar(40) DEFAULT NULL,
  `InvoiceNetwork` enum('Yes','No') DEFAULT NULL,
  `VATRateID` int(11) DEFAULT NULL,
  `VATNumber` varchar(40) DEFAULT NULL,
  `PaymentDiscount` decimal(10,2) DEFAULT '0.00',
  `PaymentTerms` int(11) DEFAULT NULL,
  `VendorNumber` varchar(40) DEFAULT NULL,
  `PurchaseOrderNumber` varchar(40) DEFAULT NULL,
  `SageAccountNumber` varchar(40) DEFAULT NULL,
  `SageNominalCode` int(11) DEFAULT NULL,
  `ForcePolicyNo` char(1) DEFAULT 'Y',
  `PromptETD` char(1) DEFAULT 'Y',
  `TradeSpecificUnitTypes` char(1) DEFAULT 'Y',
  `TradeSpecificManufacturers` char(1) DEFAULT 'Y',
  `TradeSpecificCallTypes` enum('Y','N') NOT NULL DEFAULT 'N',
  `TradeSpecificJobTypes` enum('Y','N') NOT NULL DEFAULT 'N',
  `EnableCompletionStatus` char(1) DEFAULT 'Y',
  `EnableProductDatabase` enum('Yes','No') NOT NULL DEFAULT 'No',
  `EnableCatalogueSearch` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `DefaultTurnaroundTime` int(2) DEFAULT NULL,
  `ClientShortName` varchar(9) DEFAULT NULL,
  `BuildingNameNumber` varchar(40) DEFAULT NULL,
  `PostalCode` varchar(10) DEFAULT NULL,
  `Street` varchar(40) DEFAULT NULL,
  `LocalArea` varchar(40) DEFAULT NULL,
  `TownCity` varchar(60) DEFAULT NULL,
  `CountyID` int(11) DEFAULT NULL,
  `CountryID` int(11) DEFAULT NULL,
  `ContactPhone` varchar(40) DEFAULT NULL,
  `ContactPhoneExt` varchar(10) DEFAULT NULL,
  `ContactEmail` varchar(40) DEFAULT NULL,
  `InvoicedCompanyName` varchar(40) DEFAULT NULL,
  `InvoicedBuilding` varchar(40) DEFAULT NULL,
  `InvoicedStreet` varchar(40) DEFAULT NULL,
  `InvoicedArea` varchar(40) DEFAULT NULL,
  `InvoicedTownCity` varchar(40) DEFAULT NULL,
  `InvoicedCountyID` int(11) DEFAULT NULL,
  `InvoicedCountryID` int(11) DEFAULT NULL,
  `InvoicedPostalCode` varchar(40) DEFAULT NULL,
  `Skin` varchar(20) DEFAULT NULL,
  `ForceReferralNumber` enum('Yes','No') DEFAULT 'No',
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ClientID`),
  KEY `IDX_client_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_client_ServiceProviderID_FK` (`ServiceProviderID`),
  KEY `IDX_client_DefaultBranchID_FK` (`DefaultBranchID`),
  CONSTRAINT `branch_TO_client` FOREIGN KEY (`DefaultBranchID`) REFERENCES `branch` (`BranchID`),
  CONSTRAINT `service_provider_TO_client` FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`),
  CONSTRAINT `user_TO_client` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.client: ~0 rows (approximately)
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
/*!40000 ALTER TABLE `client` ENABLE KEYS */;


-- Dumping structure for table create_new.client_branch
CREATE TABLE IF NOT EXISTS `client_branch` (
  `ClientBranchID` int(11) NOT NULL AUTO_INCREMENT,
  `ClientID` int(11) NOT NULL,
  `BranchID` int(11) NOT NULL,
  `NetworkID` int(11) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ClientBranchID`),
  UNIQUE KEY `TUC_client_branch_1` (`NetworkID`,`BranchID`,`ClientID`),
  KEY `IDX_client_branch_ClientID_FK` (`ClientID`),
  KEY `IDX_client_branch_BranchID_FK` (`BranchID`),
  KEY `IDX_client_branch_NetworkID_FK` (`NetworkID`),
  KEY `IDX_client_branch_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `branch_TO_client_branch` FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`),
  CONSTRAINT `client_TO_client_branch` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `network_TO_client_branch` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `user_TO_client_branch` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.client_branch: ~0 rows (approximately)
/*!40000 ALTER TABLE `client_branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_branch` ENABLE KEYS */;


-- Dumping structure for table create_new.client_manufacturer
CREATE TABLE IF NOT EXISTS `client_manufacturer` (
  `ClientManufacturerID` int(11) NOT NULL AUTO_INCREMENT,
  `ClientID` int(11) NOT NULL,
  `ManufacturerID` int(11) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ClientManufacturerID`),
  UNIQUE KEY `TUC_client_manufacturer_1` (`ClientID`,`ManufacturerID`),
  KEY `IDX_client_manufacturer_ManufacturerID_FK` (`ManufacturerID`),
  KEY `IDX_client_manufacturer_ClientID_FK` (`ClientID`),
  KEY `IDX_client_manufacturer_ModifiedUserID_FK` (`ModifiedUserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.client_manufacturer: ~0 rows (approximately)
/*!40000 ALTER TABLE `client_manufacturer` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_manufacturer` ENABLE KEYS */;


-- Dumping structure for table create_new.client_purchase_order
CREATE TABLE IF NOT EXISTS `client_purchase_order` (
  `ClientPurchaseOrder` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) DEFAULT NULL,
  `PurchaseOrderNumber` varchar(40) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ClientPurchaseOrder`),
  KEY `IDX_client_purchase_order_BrandID_FK` (`BrandID`),
  KEY `IDX_client_purchase_order_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `brand_TO_client_purchase_order` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `user_TO_client_purchase_order` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.client_purchase_order: ~0 rows (approximately)
/*!40000 ALTER TABLE `client_purchase_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_purchase_order` ENABLE KEYS */;


-- Dumping structure for table create_new.colour
CREATE TABLE IF NOT EXISTS `colour` (
  `ColourID` int(10) NOT NULL AUTO_INCREMENT,
  `ColourName` varchar(50) DEFAULT NULL,
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`ColourID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.colour: ~0 rows (approximately)
/*!40000 ALTER TABLE `colour` DISABLE KEYS */;
/*!40000 ALTER TABLE `colour` ENABLE KEYS */;


-- Dumping structure for table create_new.colour_model
CREATE TABLE IF NOT EXISTS `colour_model` (
  `ColourModelID` int(10) NOT NULL AUTO_INCREMENT,
  `ColourID` int(10) NOT NULL DEFAULT '0',
  `ModelID` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ColourModelID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.colour_model: ~0 rows (approximately)
/*!40000 ALTER TABLE `colour_model` DISABLE KEYS */;
/*!40000 ALTER TABLE `colour_model` ENABLE KEYS */;


-- Dumping structure for table create_new.completion_status
CREATE TABLE IF NOT EXISTS `completion_status` (
  `CompletionStatusID` int(11) NOT NULL AUTO_INCREMENT,
  `CompletionStatus` varchar(20) DEFAULT NULL,
  `Description` varchar(50) DEFAULT NULL,
  `Sort` int(11) DEFAULT NULL,
  `BrandID` int(11) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CompletionStatusID`),
  KEY `IDX_completion_status_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_completion_status_BrandID_FK` (`BrandID`),
  CONSTRAINT `brand_TO_completion_status` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `user_TO_completion_status` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.completion_status: ~0 rows (approximately)
/*!40000 ALTER TABLE `completion_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `completion_status` ENABLE KEYS */;


-- Dumping structure for table create_new.contact_history
CREATE TABLE IF NOT EXISTS `contact_history` (
  `ContactHistoryID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) DEFAULT NULL,
  `ContactHistoryActionID` int(11) NOT NULL,
  `BrandID` int(11) DEFAULT '1000',
  `ContactDate` date DEFAULT NULL,
  `ContactTime` time DEFAULT NULL,
  `UserCode` varchar(15) DEFAULT NULL,
  `Subject` varchar(50) DEFAULT NULL,
  `Note` varchar(254) DEFAULT NULL,
  `IsDownloadedToSC` tinyint(1) NOT NULL DEFAULT '0',
  `SystemAllocated` enum('yes','no') DEFAULT NULL,
  PRIMARY KEY (`ContactHistoryID`),
  KEY `IDX_contact_history_JobID_FK` (`JobID`),
  KEY `IDX_contact_history_ContactHistoryActionID_FK` (`ContactHistoryActionID`),
  CONSTRAINT `contact_history_action_TO_contact_history` FOREIGN KEY (`ContactHistoryActionID`) REFERENCES `contact_history_action` (`ContactHistoryActionID`),
  CONSTRAINT `job_TO_contact_history` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.contact_history: ~0 rows (approximately)
/*!40000 ALTER TABLE `contact_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact_history` ENABLE KEYS */;


-- Dumping structure for table create_new.contact_history_action
CREATE TABLE IF NOT EXISTS `contact_history_action` (
  `ContactHistoryActionID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `Action` varchar(255) NOT NULL,
  `ActionCode` int(11) NOT NULL,
  `Type` enum('User Defined','System Defined') NOT NULL,
  `Status` enum('Active','In-active') NOT NULL,
  PRIMARY KEY (`ContactHistoryActionID`),
  KEY `IDX_contact_history_action_BrandID_FK` (`BrandID`),
  CONSTRAINT `brand_TO_contact_history_action` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1 COMMENT='This table contains Contact History Actions details.';

-- Dumping data for table create_new.contact_history_action: ~0 rows (approximately)
/*!40000 ALTER TABLE `contact_history_action` DISABLE KEYS */;
INSERT IGNORE INTO `contact_history_action` (`ContactHistoryActionID`, `BrandID`, `Action`, `ActionCode`, `Type`, `Status`) VALUES
	(1, 1000, 'PHONE IN', 1, 'System Defined', 'Active'),
	(2, 1000, 'PHONE OUT', 2, 'System Defined', 'Active'),
	(3, 1000, 'FAXED IN', 3, 'System Defined', 'Active'),
	(4, 1000, 'FAXED OUT', 4, 'System Defined', 'Active'),
	(5, 1000, 'E-MAIL IN', 5, 'System Defined', 'Active'),
	(6, 1000, 'E-MAIL OUT', 6, 'System Defined', 'Active'),
	(7, 1000, 'VISIT IN', 7, 'System Defined', 'Active'),
	(8, 1000, 'VISIT OUT', 8, 'System Defined', 'Active'),
	(9, 1000, 'LETTER IN', 9, 'System Defined', 'Active'),
	(10, 1000, 'LETTER OUT', 10, 'System Defined', 'Active'),
	(11, 1000, 'A GENERAL NOTE', 11, 'System Defined', 'Active'),
	(12, 1000, 'WAITING FOR TECHNICAL', 12, 'System Defined', 'Active'),
	(13, 1000, 'WAITING FOR CUSTOMER', 13, 'System Defined', 'Active'),
	(14, 1000, 'INFO RECEIVED', 14, 'System Defined', 'Active'),
	(15, 1000, 'RMA Tracker', 15, 'System Defined', 'Active'),
	(17, 1000, 'CANCEL REQUEST', 17, 'System Defined', 'Active');
/*!40000 ALTER TABLE `contact_history_action` ENABLE KEYS */;


-- Dumping structure for table create_new.contact_us_messages
CREATE TABLE IF NOT EXISTS `contact_us_messages` (
  `ContactUsMessageID` int(11) DEFAULT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `ContactUsSubjectID` int(11) DEFAULT NULL,
  `Message` text,
  `CreatedDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Status` enum('Active','In-active') DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.contact_us_messages: ~0 rows (approximately)
/*!40000 ALTER TABLE `contact_us_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact_us_messages` ENABLE KEYS */;


-- Dumping structure for table create_new.contact_us_subject
CREATE TABLE IF NOT EXISTS `contact_us_subject` (
  `ContactUsSubjectID` int(11) DEFAULT NULL,
  `Subject` varchar(200) DEFAULT NULL,
  `PriorityOrder` int(11) DEFAULT NULL,
  `CreatedDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Status` enum('Active','In-active') DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.contact_us_subject: ~0 rows (approximately)
/*!40000 ALTER TABLE `contact_us_subject` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact_us_subject` ENABLE KEYS */;


-- Dumping structure for table create_new.country
CREATE TABLE IF NOT EXISTS `country` (
  `CountryID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `Name` varchar(40) NOT NULL,
  `CountryCode` int(11) DEFAULT NULL,
  `InternationalCode` varchar(3) NOT NULL,
  `Currency` varchar(20) DEFAULT NULL,
  `CurrencySymbol` varchar(1) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CountryID`),
  KEY `IDX_country_BrandID_FK` (`BrandID`),
  KEY `IDX_country_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `brand_TO_country` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `user_TO_country` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table contains country details.';

-- Dumping data for table create_new.country: ~0 rows (approximately)
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
/*!40000 ALTER TABLE `country` ENABLE KEYS */;


-- Dumping structure for table create_new.country_vat_rate
CREATE TABLE IF NOT EXISTS `country_vat_rate` (
  `CountryID` int(11) NOT NULL,
  `VatRateID` int(11) NOT NULL,
  PRIMARY KEY (`CountryID`,`VatRateID`),
  KEY `IDX_country_vat_rate_VatRateID_FK` (`VatRateID`),
  KEY `IDX_country_vat_rate_CountryID_FK` (`CountryID`),
  CONSTRAINT `country_TO_country_vat_rate` FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `vat_rate_TO_country_vat_rate` FOREIGN KEY (`VatRateID`) REFERENCES `vat_rate` (`VatRateID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.country_vat_rate: ~0 rows (approximately)
/*!40000 ALTER TABLE `country_vat_rate` DISABLE KEYS */;
/*!40000 ALTER TABLE `country_vat_rate` ENABLE KEYS */;


-- Dumping structure for table create_new.county
CREATE TABLE IF NOT EXISTS `county` (
  `CountyID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `CountryID` int(11) NOT NULL,
  `Name` varchar(40) NOT NULL,
  `CountyCode` int(11) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CountyID`),
  KEY `IDX_county_BrandID_FK` (`BrandID`),
  KEY `IDX_county_CountryID_FK` (`CountryID`),
  KEY `IDX_county_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `brand_TO_county` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `country_TO_county` FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `user_TO_county` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table contains county details of each country.';

-- Dumping data for table create_new.county: ~0 rows (approximately)
/*!40000 ALTER TABLE `county` DISABLE KEYS */;
/*!40000 ALTER TABLE `county` ENABLE KEYS */;


-- Dumping structure for table create_new.courier
CREATE TABLE IF NOT EXISTS `courier` (
  `CourierID` int(11) NOT NULL AUTO_INCREMENT,
  `CourierName` varchar(100) NOT NULL,
  `BrandID` int(11) DEFAULT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `Online` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `AccountNo` varchar(40) NOT NULL,
  `IPAddress` varchar(32) DEFAULT NULL,
  `UserName` varchar(40) DEFAULT NULL,
  `Password` varchar(40) DEFAULT NULL,
  `FilePrefix` varchar(30) DEFAULT NULL,
  `FileCount` int(11) DEFAULT NULL,
  `ReportFailureDays` tinyint(2) DEFAULT NULL,
  `CommunicateIndividualConsignments` enum('No','Yes') NOT NULL DEFAULT 'No',
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CourierID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.courier: ~0 rows (approximately)
/*!40000 ALTER TABLE `courier` DISABLE KEYS */;
/*!40000 ALTER TABLE `courier` ENABLE KEYS */;


-- Dumping structure for table create_new.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `CustomerID` int(11) NOT NULL AUTO_INCREMENT,
  `BuildingNameNumber` varchar(40) DEFAULT NULL,
  `PostalCode` varchar(10) DEFAULT NULL,
  `Street` varchar(40) DEFAULT NULL,
  `LocalArea` varchar(40) DEFAULT NULL,
  `TownCity` varchar(60) DEFAULT NULL,
  `CountyID` int(11) DEFAULT NULL,
  `CountryID` int(11) DEFAULT NULL,
  `CustomerTitleID` int(11) DEFAULT NULL,
  `ContactFirstName` varchar(40) DEFAULT NULL,
  `ContactLastName` varchar(40) DEFAULT NULL,
  `ContactHomePhone` varchar(40) DEFAULT NULL,
  `ContactWorkPhoneExt` varchar(10) DEFAULT NULL,
  `ContactWorkPhone` varchar(40) DEFAULT NULL,
  `ContactFax` varchar(40) DEFAULT NULL,
  `ContactMobile` varchar(40) DEFAULT NULL,
  `ContactEmail` varchar(40) DEFAULT NULL,
  `DataProtection` enum('Yes','No') DEFAULT NULL,
  `DataProtectionEmail` enum('Marketing','SVCCampaign','Both') DEFAULT NULL,
  `DataProtectionLetter` enum('Marketing','SVCCampaign','Both') DEFAULT NULL,
  `DataProtectionSMS` enum('Marketing','SVCCampaign','Both') DEFAULT NULL,
  `DataProtectionMobile` enum('Marketing','SVCCampaign','Both') DEFAULT NULL,
  `DataProtectionHome` enum('Marketing','SVCCampaign','Both') DEFAULT NULL,
  `DataProtectionOffice` enum('Marketing','SVCCampaign','Both') DEFAULT NULL,
  `SamsungCustomerRef` varchar(10) DEFAULT NULL,
  `CustomerType` varchar(10) DEFAULT NULL,
  `CompanyName` varchar(100) DEFAULT NULL,
  `Password` varchar(256) DEFAULT NULL,
  `Username` varchar(100) DEFAULT NULL,
  `LastLoggedIn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SecurityQuestionID` int(11) DEFAULT NULL,
  `SecurityQuestionAnswer` varchar(256) DEFAULT NULL,
  `EmailConfirmed` tinyint(1) NOT NULL DEFAULT '0',
  `EmailAuthKey` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`CustomerID`),
  KEY `IDX_customer_1` (`PostalCode`),
  KEY `IDX_customer_2` (`ContactLastName`),
  KEY `IDX_customer_3` (`ContactFirstName`),
  KEY `IDX_customer_CountyID_FK` (`CountyID`),
  KEY `IDX_customer_CountryID_FK` (`CountryID`),
  KEY `IDX_customer_CustomerTitleID_FK` (`CustomerTitleID`),
  CONSTRAINT `country_TO_customer` FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `county_TO_customer` FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`),
  CONSTRAINT `customer_title_TO_customer` FOREIGN KEY (`CustomerTitleID`) REFERENCES `customer_title` (`CustomerTitleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.customer: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;


-- Dumping structure for table create_new.customer_title
CREATE TABLE IF NOT EXISTS `customer_title` (
  `CustomerTitleID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `TitleCode` int(11) DEFAULT NULL,
  `Type` enum('Male','Female','Unknown') DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CustomerTitleID`),
  KEY `IDX_customer_title_BrandID_FK` (`BrandID`),
  KEY `IDX_customer_title_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_Title` (`Title`),
  CONSTRAINT `brand_TO_customer_title` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `user_TO_customer_title` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='This table contains Customer Titles details.';

-- Dumping data for table create_new.customer_title: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_title` DISABLE KEYS */;
INSERT IGNORE INTO `customer_title` (`CustomerTitleID`, `BrandID`, `Title`, `TitleCode`, `Type`, `CreatedDate`, `EndDate`, `Status`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 1000, 'Mr', 1, 'Male', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(2, 1000, 'Mrs', 2, 'Female', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(3, 1000, 'Miss', 3, 'Female', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(4, 1000, 'Ms', 4, 'Female', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(5, 1000, 'Dr', 5, 'Unknown', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2012-09-11 19:45:58');
/*!40000 ALTER TABLE `customer_title` ENABLE KEYS */;


-- Dumping structure for table create_new.datatables_custom_columns
CREATE TABLE IF NOT EXISTS `datatables_custom_columns` (
  `DatatablesCustomColumnsID` int(10) NOT NULL AUTO_INCREMENT,
  `UserID` int(10) DEFAULT NULL,
  `PageID` int(10) DEFAULT NULL,
  `ColumnString` varchar(500) DEFAULT NULL,
  `ColumnDisplayString` varchar(500) DEFAULT NULL,
  `ColumnOrderString` varchar(500) DEFAULT NULL,
  `ColumnNameString` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`DatatablesCustomColumnsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.datatables_custom_columns: ~0 rows (approximately)
/*!40000 ALTER TABLE `datatables_custom_columns` DISABLE KEYS */;
/*!40000 ALTER TABLE `datatables_custom_columns` ENABLE KEYS */;


-- Dumping structure for table create_new.datatables_custom_default_columns
CREATE TABLE IF NOT EXISTS `datatables_custom_default_columns` (
  `DatatablesCustomDefaultColumnsID` int(10) NOT NULL AUTO_INCREMENT,
  `UserID` int(10) DEFAULT NULL,
  `PageID` int(10) DEFAULT NULL,
  `ColumnDisplayString` varchar(500) DEFAULT NULL,
  `ColumnOrderString` varchar(500) DEFAULT NULL,
  `ColumnNameString` varchar(5000) DEFAULT NULL,
  `ColumnStatusString` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`DatatablesCustomDefaultColumnsID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.datatables_custom_default_columns: ~0 rows (approximately)
/*!40000 ALTER TABLE `datatables_custom_default_columns` DISABLE KEYS */;
/*!40000 ALTER TABLE `datatables_custom_default_columns` ENABLE KEYS */;


-- Dumping structure for table create_new.deferred_postcode
CREATE TABLE IF NOT EXISTS `deferred_postcode` (
  `DeferredPostcodeID` int(10) NOT NULL AUTO_INCREMENT,
  `Postcode` varchar(8) DEFAULT NULL,
  `ServiceProviderID` int(10) NOT NULL DEFAULT '0',
  `DpType` enum('Working','WeekEnd') NOT NULL DEFAULT 'Working',
  `DeferredTimeSlotID` int(11) DEFAULT NULL,
  PRIMARY KEY (`DeferredPostcodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.deferred_postcode: ~0 rows (approximately)
/*!40000 ALTER TABLE `deferred_postcode` DISABLE KEYS */;
/*!40000 ALTER TABLE `deferred_postcode` ENABLE KEYS */;


-- Dumping structure for table create_new.deferred_time_slot
CREATE TABLE IF NOT EXISTS `deferred_time_slot` (
  `DeferredTimeSlotID` int(10) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(10) DEFAULT '0',
  `EarliestWorkday` time DEFAULT NULL,
  `LatestWorkday` time DEFAULT NULL,
  `EarliestWeekend` time DEFAULT NULL,
  `LatestWeekend` time DEFAULT NULL,
  PRIMARY KEY (`DeferredTimeSlotID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.deferred_time_slot: ~0 rows (approximately)
/*!40000 ALTER TABLE `deferred_time_slot` DISABLE KEYS */;
/*!40000 ALTER TABLE `deferred_time_slot` ENABLE KEYS */;


-- Dumping structure for table create_new.diary_allocation
CREATE TABLE IF NOT EXISTS `diary_allocation` (
  `DiaryAllocationID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `AllocatedDate` date DEFAULT NULL,
  `AppointmentAllocationSlotID` int(11) DEFAULT NULL,
  `TimeFrom` time DEFAULT NULL,
  `NumberOfAllocations` int(11) DEFAULT NULL,
  `ServiceProviderEngineerID` int(11) DEFAULT NULL,
  `SlotsLeft` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`DiaryAllocationID`),
  KEY `IDX_diary_allocation_1` (`ServiceProviderID`),
  KEY `IDX_diary_allocation_2` (`AllocatedDate`),
  KEY `IDX_diary_allocation_3` (`AppointmentAllocationSlotID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.diary_allocation: ~0 rows (approximately)
/*!40000 ALTER TABLE `diary_allocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `diary_allocation` ENABLE KEYS */;


-- Dumping structure for table create_new.diary_faq
CREATE TABLE IF NOT EXISTS `diary_faq` (
  `DiaryFaqID` int(10) NOT NULL AUTO_INCREMENT,
  `DiaryFaqCategoryID` int(10) DEFAULT NULL,
  `FaqQuestion` varchar(500) DEFAULT NULL,
  `FaqAnswer` varchar(5000) DEFAULT NULL,
  `Status` enum('Active','In-Active') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`DiaryFaqID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.diary_faq: ~0 rows (approximately)
/*!40000 ALTER TABLE `diary_faq` DISABLE KEYS */;
/*!40000 ALTER TABLE `diary_faq` ENABLE KEYS */;


-- Dumping structure for table create_new.diary_faq_category
CREATE TABLE IF NOT EXISTS `diary_faq_category` (
  `DiaryFaqCategoryID` int(10) NOT NULL AUTO_INCREMENT,
  `CategoryName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`DiaryFaqCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.diary_faq_category: ~0 rows (approximately)
/*!40000 ALTER TABLE `diary_faq_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `diary_faq_category` ENABLE KEYS */;


-- Dumping structure for table create_new.diary_holiday_diary
CREATE TABLE IF NOT EXISTS `diary_holiday_diary` (
  `DiaryHolidayID` int(10) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(10) NOT NULL DEFAULT '0',
  `ServiceProviderEngineerID` int(10) NOT NULL DEFAULT '0',
  `StartTime` datetime DEFAULT NULL,
  `EndTime` datetime DEFAULT NULL,
  `Reason` varchar(2000) DEFAULT NULL,
  `Created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`DiaryHolidayID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.diary_holiday_diary: ~0 rows (approximately)
/*!40000 ALTER TABLE `diary_holiday_diary` DISABLE KEYS */;
/*!40000 ALTER TABLE `diary_holiday_diary` ENABLE KEYS */;


-- Dumping structure for table create_new.diary_holiday_slots
CREATE TABLE IF NOT EXISTS `diary_holiday_slots` (
  `DiaryHolidaySlotsID` int(10) NOT NULL AUTO_INCREMENT,
  `DiaryHolidayDiaryID` int(10) DEFAULT NULL,
  `HolidayDate` date DEFAULT NULL,
  `StartTimeSec` int(11) DEFAULT NULL,
  `EndTimeSec` int(11) DEFAULT NULL,
  `TotalTimeSec` int(11) DEFAULT NULL,
  `ServiceProviderEngineerID` int(11) DEFAULT NULL,
  PRIMARY KEY (`DiaryHolidaySlotsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.diary_holiday_slots: ~0 rows (approximately)
/*!40000 ALTER TABLE `diary_holiday_slots` DISABLE KEYS */;
/*!40000 ALTER TABLE `diary_holiday_slots` ENABLE KEYS */;


-- Dumping structure for table create_new.diary_postcode_allocation
CREATE TABLE IF NOT EXISTS `diary_postcode_allocation` (
  `DiaryPostcodeAllocationID` int(11) NOT NULL AUTO_INCREMENT,
  `DiaryAllocationID` int(11) DEFAULT NULL,
  `Postcode` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`DiaryPostcodeAllocationID`),
  KEY `IDX_diary_postcode_allocation_1` (`DiaryAllocationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.diary_postcode_allocation: ~0 rows (approximately)
/*!40000 ALTER TABLE `diary_postcode_allocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `diary_postcode_allocation` ENABLE KEYS */;


-- Dumping structure for table create_new.diary_sp_map
CREATE TABLE IF NOT EXISTS `diary_sp_map` (
  `diarySPMapID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `MapDate` date DEFAULT NULL,
  `MapUUID` varchar(64) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`diarySPMapID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.diary_sp_map: ~0 rows (approximately)
/*!40000 ALTER TABLE `diary_sp_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `diary_sp_map` ENABLE KEYS */;


-- Dumping structure for table create_new.diary_town_allocation
CREATE TABLE IF NOT EXISTS `diary_town_allocation` (
  `DiaryTownAllocationID` int(11) NOT NULL AUTO_INCREMENT,
  `DiaryAllocationID` int(11) DEFAULT NULL,
  `Town` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`DiaryTownAllocationID`),
  KEY `IDX_diary_town_allocation_1` (`DiaryAllocationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.diary_town_allocation: ~0 rows (approximately)
/*!40000 ALTER TABLE `diary_town_allocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `diary_town_allocation` ENABLE KEYS */;


-- Dumping structure for table create_new.doc_api
CREATE TABLE IF NOT EXISTS `doc_api` (
  `DocApiID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Version` varchar(10) NOT NULL,
  `Author` varchar(50) NOT NULL,
  `Description` text NOT NULL,
  `Baseuri` varchar(50) NOT NULL,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`DocApiID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.doc_api: ~0 rows (approximately)
/*!40000 ALTER TABLE `doc_api` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_api` ENABLE KEYS */;


-- Dumping structure for table create_new.doc_categories
CREATE TABLE IF NOT EXISTS `doc_categories` (
  `DocCategoriesID` int(11) NOT NULL AUTO_INCREMENT,
  `DocApiID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Description` text NOT NULL,
  `Path` varchar(50) NOT NULL,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`DocCategoriesID`),
  KEY `IDX_doc_categories_DocApiID_FK` (`DocApiID`),
  CONSTRAINT `doc_api_TO_doc_categories` FOREIGN KEY (`DocApiID`) REFERENCES `doc_api` (`DocApiID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.doc_categories: ~0 rows (approximately)
/*!40000 ALTER TABLE `doc_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_categories` ENABLE KEYS */;


-- Dumping structure for table create_new.doc_method_response
CREATE TABLE IF NOT EXISTS `doc_method_response` (
  `DocMethodResponseID` int(11) NOT NULL AUTO_INCREMENT,
  `DocProceduresID` int(11) NOT NULL,
  `AnswerCode` int(11) NOT NULL,
  `Response` text NOT NULL,
  `Description` text,
  `CDATA` text,
  PRIMARY KEY (`DocMethodResponseID`),
  KEY `IDX_doc_method_response_DocProceduresID_FK` (`DocProceduresID`),
  CONSTRAINT `doc_procedures_TO_doc_method_response` FOREIGN KEY (`DocProceduresID`) REFERENCES `doc_procedures` (`DocProceduresID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.doc_method_response: ~0 rows (approximately)
/*!40000 ALTER TABLE `doc_method_response` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_method_response` ENABLE KEYS */;


-- Dumping structure for table create_new.doc_procedures
CREATE TABLE IF NOT EXISTS `doc_procedures` (
  `DocProceduresID` int(11) NOT NULL AUTO_INCREMENT,
  `DocCategoriesID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Description` text NOT NULL,
  `Path` varchar(50) DEFAULT NULL,
  `methods` varchar(100) NOT NULL,
  `formats` varchar(100) NOT NULL,
  `auth_types` varchar(100) DEFAULT NULL,
  `DocApiID` int(11) DEFAULT NULL,
  PRIMARY KEY (`DocProceduresID`),
  KEY `IDX_doc_procedures_DocCategoriesID_FK` (`DocCategoriesID`),
  CONSTRAINT `doc_categories_TO_doc_procedures` FOREIGN KEY (`DocCategoriesID`) REFERENCES `doc_categories` (`DocCategoriesID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.doc_procedures: ~0 rows (approximately)
/*!40000 ALTER TABLE `doc_procedures` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_procedures` ENABLE KEYS */;


-- Dumping structure for table create_new.doc_procedure_parameters
CREATE TABLE IF NOT EXISTS `doc_procedure_parameters` (
  `DocProcedureParametersID` int(11) NOT NULL AUTO_INCREMENT,
  `DocProceduresID` int(11) NOT NULL,
  `CodeLanguage` varchar(50) NOT NULL,
  `CDATA` text NOT NULL,
  `Default` varchar(50) NOT NULL,
  `Option` varchar(50) NOT NULL,
  `Type` varchar(50) DEFAULT NULL,
  `Size` int(11) DEFAULT NULL,
  `Example` text NOT NULL,
  `response_ex` text NOT NULL,
  `Required` tinyint(1) NOT NULL,
  PRIMARY KEY (`DocProcedureParametersID`),
  KEY `IDX_doc_procedure_parameters_DocProceduresID_FK` (`DocProceduresID`),
  CONSTRAINT `doc_procedures_TO_doc_procedure_parameters` FOREIGN KEY (`DocProceduresID`) REFERENCES `doc_procedures` (`DocProceduresID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.doc_procedure_parameters: ~0 rows (approximately)
/*!40000 ALTER TABLE `doc_procedure_parameters` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_procedure_parameters` ENABLE KEYS */;


-- Dumping structure for table create_new.email
CREATE TABLE IF NOT EXISTS `email` (
  `EmailID` int(11) NOT NULL AUTO_INCREMENT,
  `EmailCode` varchar(64) DEFAULT NULL,
  `Title` varchar(100) DEFAULT NULL,
  `Message` text,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ModifiedUserID` int(11) DEFAULT NULL,
  PRIMARY KEY (`EmailID`),
  KEY `IDX_email_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `user_TO_email` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.email: ~16 rows (approximately)
/*!40000 ALTER TABLE `email` DISABLE KEYS */;
INSERT IGNORE INTO `email` (`EmailID`, `EmailCode`, `Title`, `Message`, `ModifiedDate`, `ModifiedUserID`) VALUES
	(1, 'job_booking', 'Service Request Details <Job No>', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n<title>[PAGE_TITLE]</title>\n</head>\n\n<body>\n<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">\n	<tbody><tr>\n		<td bgcolor="#ffffff" align="center">\n        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">\n            	<tbody>\n                \n                <!-- #HEADER\n        		================================================== -->\n            	<tr>\n                	<td width="640"><!-- End top bar table -->\n                    </td>\n                </tr>\n                <tr>\n                <td width="640" bgcolor="#52aae0" align="center">\n    \n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="2" valign="bottom">SERVICE REQUEST</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'">Email us: <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p></td>\n        </tr>\n        \n    </tbody></table>\n    \n    <!-- End header -->\n</td>\n                </tr>\n                <!-- #CONTENT\n        		================================================== -->\n                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>\n                <tr>\n                <td width="640" bgcolor="#ffffff">\n    <table width="640" cellspacing="6" cellpadding="6" border="0">\n        <tbody><tr>\n            <td width="30"></td>\n            <td width="580">\n                <!-- #TEXT ONLY\n                ================================================== -->\n                <table width="580" cellspacing="6" cellpadding="6" border="0">\n                    <tbody><tr>\n                        <td width="405">\n                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>\n                            \n							\n				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">Thank you for registering your service request with us today. We will arrange for a service agent located within your proximity to contact you to discuss your particular requirement and if necessary, arrange a mutually convenient date and time for a service call-out.</p>\n                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'"></p>\n                                \n\n                        </td>\n                        <td width="175">\n						                        \n                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />\n                       \n					    </td>\n                    </tr>\n                   \n                </tbody></table>\n                <table>\n                <tbody>\n                 <tr><td width="580" height="5"></td></tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                    <tr><td width="580" height="5"></td></tr>\n                    </tbody>\n                    </table>\n                                        \n                <!-- #TEXT WITH ADDRESS\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="2" border="0">\n                    <tbody>\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;"  >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="150" colspan="2" valign="bottom">ADDRESS DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;"  >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_TITLE_FIRST_LAST_NAME], <br>[CUSTOMER_ADDRESS]</p>\n                        </td>\n                    </tr>\n					\n					\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">CONTACT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_PHONE_NO][CUSTOMER_MOBILE_NO][CUSTOMER_EMAIL_PREFIX]<a href="mailto:[CUSTOMER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[CUSTOMER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;font-size:13px;font-family:\'Calibri\'">SERVICE CENTRE</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[SERVICE_CENTRE_ADDRESS].<br>[SERVICE_PROVIDER_TELNO_HTML][SERVICE_CENTRE_EMAIL_PREFIX]<a href="mailto:[SERVICE_PROVIDER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[SERVICE_PROVIDER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Job Number: [SC_JOB_NUMBER]&nbsp;&nbsp;&nbsp;&nbsp;Job Booked: [BOOKED_DATE] <br>Manufacturer: [MANUFACTURER_NAME]&nbsp;&nbsp;&nbsp;&nbsp;<br>Product: [UNIT_TYPE_NAME]&nbsp;&nbsp;&nbsp;&nbsp; <br>Model No: [MODEL_NO] <br>Service Type: [SERVICE_TYPE]&nbsp;&nbsp;&nbsp;&nbsp;Location: [PRODUCT_LOCATION]</p>\n                        </td>\n                    </tr>\n			    </tbody></table>\n                                        \n           \n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                </tbody></table>\n                                        \n                <!-- #TEXT FOOTER\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="100">\n                    \n\n<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:\'Calibri\'">Please email <a style="font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a> immediately if there are any discrepancies within the information. Alternatively, if you prefer to talk to a person please call [SERVICE_PROVIDER_NAME] \non [SERVICE_PROVIDER_TELNO] and quote [SC_JOB_NUMBER]    \n<br><br></p>\n<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'">Assuring you of our best intention at all times.</p>\n\n<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'">Yours Sincerely, [BRAND_NAME] Customer Service Team</p>\n                    </td>\n                    </tr>\n                </tbody></table>\n                                                                                \n                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY\n                ================================================== -->\n               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td valign="top">\n                          \n                        </td>\n                       \n                    </tr>\n                </tbody></table>-->\n                                        \n                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE\n                ================================================== -->\n                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td width="180" valign="top">\n                          \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                       \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                            \n                        </td>\n                    </tr>\n                </tbody></table>-->\n            </td>\n            <td width="30"></td>\n        </tr>\n    </tbody></table>\n</td>\n                </tr>\n                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->\n                <!-- #FOOTER\n   				 ================================================== -->\n                <tr>\n                <td width="640">\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">\n <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'">If you have any queries contact us on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[BRANCH_EMAIL]">[BRANCH_EMAIL]</a></p>\n            </td>\n            </tr>\n\n    </tbody></table>\n</td>\n                </tr>\n               \n            </tbody></table>\n        </td>\n	</tr>\n</tbody></table>\n</body>\n</html>\n', '2013-04-04 13:35:16', 1),
	(5, 'cancelled_job_notification', 'Job No: <Job No> Cancelled', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n<title>[PAGE_TITLE]</title>\n</head>\n\n<body>\n<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">\n	<tbody><tr>\n		<td bgcolor="#ffffff" align="center">\n        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">\n            	<tbody>\n                \n                <!-- #HEADER\n        		================================================== -->\n            	<tr>\n                	<td width="640"><!-- End top bar table -->\n                    </td>\n                </tr>\n                <tr>\n                <td width="640" bgcolor="#52aae0" align="center">\n    \n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        \n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="3" valign="bottom">CANCELLED JOB NOTIFICATION</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; text-transform:capitalize;font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'"><a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:mail@skylinecms.co.uk"><!--mail@skylinecms.co.uk--></a></p></td>\n        </tr>\n        \n    </tbody></table>\n    \n    <!-- End header -->\n</td>\n                </tr>\n                <!-- #CONTENT\n        		================================================== -->\n                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>\n                <tr>\n                <td width="640" bgcolor="#ffffff">\n    <table width="640" cellspacing="6" cellpadding="6" border="0">\n        <tbody><tr>\n            <td width="30"></td>\n            <td width="580">\n                <!-- #TEXT ONLY\n                ================================================== -->\n                <table width="580" cellspacing="6" cellpadding="6" border="0">\n                    <tbody><tr>\n                        <td width="405">\n                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [SERVICE_MANAGER]</p>\n                            \n							\n				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'; font-weight:bold;">Please be aware that the job detailed below has been Cancelled.</p>\n                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">It is <u>important</u> that you update your repair management system to reflect this new status.</p>\n                                \n\n                        </td>\n                        <td width="175">\n						                        \n                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />\n                       \n					    </td>\n                    </tr>\n                   \n                </tbody></table>\n                <table>\n                <tbody>\n                 <tr><td width="580" height="5"></td></tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                    <tr><td width="580" height="5"></td></tr>\n                    </tbody>\n                    </table>\n                                        \n                <!-- #TEXT WITH ADDRESS\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="2" border="0">\n                    <tbody>\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;"  >\n                          <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="150" colspan="3" valign="bottom">ADDRESS DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;"  >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_TITLE_FIRST_LAST_NAME], <br>[CUSTOMER_ADDRESS]</p>\n                        </td>\n                    </tr>\n					\n					\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          \n                          <td width="150" height="5" colspan="3" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">CONTACT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_PHONE_NUMBER][CUSTOMER_MOBILE_NUMBER][CUSTOMER_EMAIL_PREFIX]<a href="mailto:[CUSTOMER_EMAIL]" target="_balnk" >[CUSTOMER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          \n                          <td width="150" height="5" colspan="3" valign="bottom" style="background:#3576bc; color:#ffffff;font-size:13px;font-family:\'Calibri\'">SERVICE CENTRE</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[SERVICE_CENTRE_ADDRESS].<br>[SERVICE_CENTRE_PHONE_NUMBER][SERVICE_CENTRE_EMAIL_PREFIX]<a href="mailto:[SERVICE_CENTRE_EMAIL]" target="_balnk" >[SERVICE_CENTRE_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                          <table cellspacing="0" cellpadding="5" border="0">\n                          <td width="150" height="5" colspan="3" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Job Number: [SC_JOB_NUMBER]&nbsp;&nbsp;&nbsp;&nbsp;Job Booked: [BOOKED_DATE] <br>Product: [MANUFACTURER_NAME]&nbsp;[UNIT_TYPE_NAME]&nbsp;&nbsp;&nbsp;&nbsp; <br>Model No: [MODEL_NO] <br>Service Type: [SERVICE_TYPE]</p>\n                        </td>\n                    </tr>\n			    </tbody></table>\n                                        \n           \n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                </tbody></table>\n                                        \n                <!-- #TEXT FOOTER\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="100">\n                    \n\n<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:\'Calibri\'">Assuring you of our best attention at all times.<br><br></p>\n<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'">Yours sincerely</p>\n\n<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'">[BRAND_NAME] Customer Service Team</p>\n                    </td>\n                    </tr>\n                </tbody></table>\n                                                                                \n                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY\n                ================================================== -->\n               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td valign="top">\n                          \n                        </td>\n                       \n                    </tr>\n                </tbody></table>-->\n                                        \n                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE\n                ================================================== -->\n                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td width="180" valign="top">\n                          \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                       \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                            \n                        </td>\n                    </tr>\n                </tbody></table>-->\n            </td>\n            <td width="30"></td>\n        </tr>\n    </tbody></table>\n</td>\n                </tr>\n                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->\n                <!-- #FOOTER\n   				 ================================================== -->\n                <tr>\n                <td width="640">\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">\n <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'"><!--If you have any queries get in touch on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:mail@skylinecms.co.uk">mail@skylinecms.co.uk</a>--></p>\n            </td>\n            </tr>\n\n    </tbody></table>\n</td>\n                </tr>\n               \n            </tbody></table>\n        </td>\n	</tr>\n</tbody></table>\n</body>\n</html>\n', '2013-04-04 13:31:42', 1),
	(8, 'job_booking_crm', 'Service Request Details <Job No>', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n<title>[PAGE_TITLE]</title>\n</head>\n\n<body>\n<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">\n	<tbody><tr>\n		<td bgcolor="#ffffff" align="center">\n        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">\n            	<tbody>\n                \n                <!-- #HEADER\n        		================================================== -->\n            	<tr>\n                	<td width="640"><!-- End top bar table -->\n                    </td>\n                </tr>\n                <tr>\n                <td width="640" bgcolor="#52aae0" align="center">\n    \n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="2" valign="bottom">SERVICE REQUEST</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'">Email us: <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p></td>\n        </tr>\n        \n    </tbody></table>\n    \n    <!-- End header -->\n</td>\n                </tr>\n                <!-- #CONTENT\n        		================================================== -->\n                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>\n                <tr>\n                <td width="640" bgcolor="#ffffff">\n    <table width="640" cellspacing="6" cellpadding="6" border="0">\n        <tbody><tr>\n            <td width="30"></td>\n            <td width="580">\n                <!-- #TEXT ONLY\n                ================================================== -->\n                <table width="580" cellspacing="6" cellpadding="6" border="0">\n                    <tbody><tr>\n                        <td width="405">\n                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>\n                            \n							\n				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">Thank you for registering your service request with us today. We will arrange for a service agent located within your area to contact you to discuss your particular requirement and if necessary, arrange a mutually convenient date and time for a service call-out.</p>\n                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Please be aware that you can access <a style="font-family:\'Calibri\'" href="[CUSTOMER_WEBSITE_URL]" target="_blank" >[CUSTOMER_WEBSITE_NAME]</a> to view a web page specifically created to allow you to manage your service life cycle on-line and receive real time progress reports.  If you require a site visit you will be able to monitor your engineers progress when he is on route to your location and you can also login to <a style="font-family:\'Calibri\'" href="[CUSTOMER_WEBSITE_URL]" target="_blank" >[CUSTOMER_WEBSITE_NAME]</a> if you wish to cancel or re-schedule your appointment.</p>\n                                \n\n                        </td>\n                        <td width="175">\n						                        \n                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />\n                       \n					    </td>\n                    </tr>\n                   \n                </tbody></table>\n                <table>\n                <tbody>\n                 <tr><td width="580" height="5"></td></tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                    <tr><td width="580" height="5"></td></tr>\n                    </tbody>\n                    </table>\n                                        \n                <!-- #TEXT WITH ADDRESS\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="2" border="0">\n                    <tbody>\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;"  >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="150" colspan="2" valign="bottom">ADDRESS DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;"  >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_TITLE_FIRST_LAST_NAME], <br>[CUSTOMER_ADDRESS]</p>\n                        </td>\n                    </tr>\n					\n					\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">CONTACT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_PHONE_NO][CUSTOMER_MOBILE_NO][CUSTOMER_EMAIL_PREFIX]<a href="mailto:[CUSTOMER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[CUSTOMER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;font-size:13px;font-family:\'Calibri\'">SERVICE CENTRE</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[SERVICE_CENTRE_ADDRESS].<br>[SERVICE_PROVIDER_TELNO_HTML][SERVICE_CENTRE_EMAIL_PREFIX]<a href="mailto:[SERVICE_PROVIDER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[SERVICE_PROVIDER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Job Number: [SC_JOB_NUMBER]&nbsp;&nbsp;&nbsp;&nbsp;Job Booked: [BOOKED_DATE] <br>Manufacturer: [MANUFACTURER_NAME]&nbsp;&nbsp;&nbsp;&nbsp;<br>Product: [UNIT_TYPE_NAME]&nbsp;&nbsp;&nbsp;&nbsp; <br>Model No: [MODEL_NO] <br>Service Type: [SERVICE_TYPE]&nbsp;&nbsp;&nbsp;&nbsp;Location: [PRODUCT_LOCATION]</p>\n                        </td>\n                    </tr>\n			    </tbody></table>\n                                        \n           \n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                </tbody></table>\n                                        \n                <!-- #TEXT FOOTER\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="100">\n                    \n\n<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:\'Calibri\'">Please email <a style="font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a> immediately if there are any discrepancies within the information. Alternatively, if you prefer to talk to a person please call [SERVICE_PROVIDER_NAME] \non [SERVICE_PROVIDER_TELNO] and quote [SC_JOB_NUMBER]    \n<br><br></p>\n<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'">Assuring you of our best intention at all times.</p>\n\n<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'">Yours Sincerely, [BRAND_NAME] Customer Service Team</p>\n                    </td>\n                    </tr>\n                </tbody></table>\n                                                                                \n                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY\n                ================================================== -->\n               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td valign="top">\n                          \n                        </td>\n                       \n                    </tr>\n                </tbody></table>-->\n                                        \n                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE\n                ================================================== -->\n                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td width="180" valign="top">\n                          \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                       \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                            \n                        </td>\n                    </tr>\n                </tbody></table>-->\n            </td>\n            <td width="30"></td>\n        </tr>\n    </tbody></table>\n</td>\n                </tr>\n                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->\n                <!-- #FOOTER\n   				 ================================================== -->\n                <tr>\n                <td width="640">\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">\n <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'">If you have any queries contact us on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[BRANCH_EMAIL]">[BRANCH_EMAIL]</a></p>\n            </td>\n            </tr>\n\n    </tbody></table>\n</td>\n                </tr>\n               \n            </tbody></table>\n        </td>\n	</tr>\n</tbody></table>\n</body>\n</html>\n', '2013-04-04 13:54:44', 1),
	(9, 'job_booking_smart_service_tracker', 'Service Request Details <Job No>', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n<title>[PAGE_TITLE]</title>\n</head>\n\n<body>\n<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">\n	<tbody><tr>\n		<td bgcolor="#ffffff" align="center">\n        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">\n            	<tbody>\n                \n                <!-- #HEADER\n        		================================================== -->\n            	<tr>\n                	<td width="640"><!-- End top bar table -->\n                    </td>\n                </tr>\n                <tr>\n                <td width="640" bgcolor="#52aae0" align="center">\n    \n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="2" valign="bottom">SERVICE REQUEST</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'">Email us: <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p></td>\n        </tr>\n        \n    </tbody></table>\n    \n    <!-- End header -->\n</td>\n                </tr>\n                <!-- #CONTENT\n        		================================================== -->\n                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>\n                <tr>\n                <td width="640" bgcolor="#ffffff">\n    <table width="640" cellspacing="6" cellpadding="6" border="0">\n        <tbody><tr>\n            <td width="30"></td>\n            <td width="580">\n                <!-- #TEXT ONLY\n                ================================================== -->\n                <table width="580" cellspacing="6" cellpadding="6" border="0">\n                    <tbody><tr>\n                        <td width="405">\n                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>\n                            \n							\n				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">Thank you for registering your service request with us today. We will arrange for a service agent located within your proximity to contact you to discuss your particular issue and if necessary, arrange a mutually convenient date and time for a service call-out.</p>\n                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Although, we will communicate regularly confirming each stage of your service lifecycle you can check progress anytime at <a style="font-family:\'Calibri\'" href="[CUSTOMER_WEBSITE_URL]" target="_blank" >[CUSTOMER_WEBSITE_NAME]</a></p>\n                                \n\n                        </td>\n                        <td width="175">\n						                        \n                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />\n                       \n					    </td>\n                    </tr>\n                   \n                </tbody></table>\n                <table>\n                <tbody>\n                 <tr><td width="580" height="5"></td></tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                    <tr><td width="580" height="5"></td></tr>\n                    </tbody>\n                    </table>\n                                        \n                <!-- #TEXT WITH ADDRESS\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="2" border="0">\n                    <tbody>\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;"  >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="150" colspan="2" valign="bottom">ADDRESS DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;"  >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_TITLE_FIRST_LAST_NAME], <br>[CUSTOMER_ADDRESS]</p>\n                        </td>\n                    </tr>\n					\n					\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">CONTACT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_PHONE_NO][CUSTOMER_MOBILE_NO][CUSTOMER_EMAIL_PREFIX]<a href="mailto:[CUSTOMER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[CUSTOMER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;font-size:13px;font-family:\'Calibri\'">SERVICE CENTRE</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[SERVICE_CENTRE_ADDRESS].<br>[SERVICE_PROVIDER_TELNO_HTML][SERVICE_CENTRE_EMAIL_PREFIX]<a href="mailto:[SERVICE_PROVIDER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[SERVICE_PROVIDER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Job Number: [SC_JOB_NUMBER]&nbsp;&nbsp;&nbsp;&nbsp;Job Booked: [BOOKED_DATE] <br>Manufacturer: [MANUFACTURER_NAME]&nbsp;&nbsp;&nbsp;&nbsp;<br>Product: [UNIT_TYPE_NAME]&nbsp;&nbsp;&nbsp;&nbsp; <br>Model No: [MODEL_NO] <br>Service Type: [SERVICE_TYPE]&nbsp;&nbsp;&nbsp;&nbsp;Location: [PRODUCT_LOCATION]</p>\n                        </td>\n                    </tr>\n			    </tbody></table>\n                                        \n           \n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                </tbody></table>\n                                        \n                <!-- #TEXT FOOTER\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="100">\n                    \n\n<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:\'Calibri\'">Please email <a style="font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a> immediately if there are any discrepancies within the information. Alternatively, if you prefer to talk to a person please call [SERVICE_PROVIDER_NAME] \non [SERVICE_PROVIDER_TELNO] and quote [SC_JOB_NUMBER]    \n<br><br></p>\n<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'">Assuring you of our best intention at all times.</p>\n\n<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'">Yours Sincerely, [BRAND_NAME] Customer Service Team</p>\n                    </td>\n                    </tr>\n                </tbody></table>\n                                                                                \n                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY\n                ================================================== -->\n               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td valign="top">\n                          \n                        </td>\n                       \n                    </tr>\n                </tbody></table>-->\n                                        \n                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE\n                ================================================== -->\n                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td width="180" valign="top">\n                          \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                       \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                            \n                        </td>\n                    </tr>\n                </tbody></table>-->\n            </td>\n            <td width="30"></td>\n        </tr>\n    </tbody></table>\n</td>\n                </tr>\n                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->\n                <!-- #FOOTER\n   				 ================================================== -->\n                <tr>\n                <td width="640">\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">\n <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'">If you have any queries contact us on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[BRANCH_EMAIL]">[BRANCH_EMAIL]</a></p>\n            </td>\n            </tr>\n\n    </tbody></table>\n</td>\n                </tr>\n               \n            </tbody></table>\n        </td>\n	</tr>\n</tbody></table>\n</body>\n</html>\n', '2013-04-04 14:00:09', 1),
	(10, 'one_touch_job_cancellation', 'One Touch - Job Cancellation <customer>', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n<title>[PAGE_TITLE]</title>\n</head>\n\n<body>\n<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">\n	<tbody><tr>\n		<td bgcolor="#ffffff" align="center">\n        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">\n            	<tbody>\n                \n                <!-- #HEADER\n        		================================================== -->\n            	<tr>\n                	<td width="640"><!-- End top bar table -->\n                    </td>\n                </tr>\n                <tr>\n                <td width="640" bgcolor="#52aae0" align="center">\n    \n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        \n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="3" valign="bottom">JOB CANCELLATION</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; text-transform:capitalize;font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'"><a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:mail@skylinecms.co.uk"><!--mail@skylinecms.co.uk--></a></p></td>\n        </tr>\n        \n    </tbody></table>\n    \n    <!-- End header -->\n</td>\n                </tr>\n                <!-- #CONTENT\n        		================================================== -->\n                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>\n                <tr>\n                <td width="640" bgcolor="#ffffff">\n    <table width="640" cellspacing="6" cellpadding="6" border="0">\n        <tbody><tr>\n            <td width="30"></td>\n            <td width="580">\n                <!-- #TEXT ONLY\n                ================================================== -->\n                <table width="580" cellspacing="6" cellpadding="6" border="0">\n                    <tbody><tr>\n                        <td width="405">\n                            <p style="font-size:18px;font-family:\'Calibri\';color:#444444;font-weight:bold;">Job Cancellation [SKILL_TYPE]</p>\n                            \n							\n				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Please be aware that a [BRAND_NAME] One-Touch  job has been cancelled and removed from the Skyline Diary.</p>\n                                \n                        </td>\n                        <td width="175">\n						                        \n                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />\n                       \n					    </td>\n                    </tr>\n					\n					<tr>\n						<td colspan="2">\n							 <p style="font-size:16px; color:#444444;line-height:115%;font-family:\'Calibri\';font-weight:bold;"><span style="float:left;" >Customer Name: [CUSTOMER_TITLE_FIRST_LAST_NAME]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="float:right;" >Postcode:  [POSTCODE]</span></p>\n                                \n						</td>\n					</tr>\n                   \n                </tbody></table>\n                <table>\n                <tbody>\n                 <tr><td width="580" height="5"></td></tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                    <tr><td width="580" height="5"></td></tr>\n                    </tbody>\n                    </table>\n                                        \n                <!-- #TEXT WITH ADDRESS\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="2" border="0">\n                    <tbody>\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;"  >\n                          <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="150" colspan="3" valign="bottom">ADDRESS DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;"  >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_ADDRESS]</p>\n                        </td>\n                    </tr>\n					\n					\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          \n                          <td width="150" height="5" colspan="3" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">BOOKING REFERENCE</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Manufacturer Order Reference Number: [ORDER_REFERENCE_NO]</a><BR>\n								Job Booked: [BOOKED_DATE]\n							</p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          \n                          <td width="150" height="5" colspan="3" valign="bottom" style="background:#3576bc; color:#ffffff;font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">[MANUFACTURER_NAME][UNIT_TYPE_NAME]\n							Model Number: [MODEL_NO]\n						</p>\n                        </td>\n                    </tr>\n					\n                   \n			    </tbody></table>\n                                        \n           \n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                </tbody></table>\n                                        \n                <!-- #TEXT FOOTER\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="100">\n                    \n\n<p style="font-size:16px;color:#444444;margin-bottom:0px;line-height:115%;font-family:\'Calibri\';font-weight:bold;">Important Information</p>\n\n<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'"></p>\n\n<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'">To view all One-Touch appointments click <a href="[SKYLINE_LOGIN_LINK]" target="_blank" >here</a></p>\n                    </td>\n                    </tr>\n                </tbody></table>\n                                                                                \n                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY\n                ================================================== -->\n               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td valign="top">\n                          \n                        </td>\n                       \n                    </tr>\n                </tbody></table>-->\n                                        \n                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE\n                ================================================== -->\n                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td width="180" valign="top">\n                          \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                       \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                            \n                        </td>\n                    </tr>\n                </tbody></table>-->\n            </td>\n            <td width="30"></td>\n        </tr>\n    </tbody></table>\n</td>\n                </tr>\n                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->\n                <!-- #FOOTER\n   				 ================================================== -->\n                <tr>\n                <td width="640">\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">\n <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'"><!--If you have any queries get in touch on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:mail@skylinecms.co.uk">mail@skylinecms.co.uk</a>--></p>\n            </td>\n            </tr>\n\n    </tbody></table>\n</td>\n                </tr>\n               \n            </tbody></table>\n        </td>\n	</tr>\n</tbody></table>\n</body>\n</html>\n\n', '2013-04-04 07:46:18', 1),
	(11, 'appointment_reminder', 'Appointment Reminder <Job No>', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n<title>[PAGE_TITLE]</title>\n</head>\n\n<body>\n<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">\n	<tbody><tr>\n		<td bgcolor="#ffffff" align="center">\n        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">\n            	<tbody>\n                \n                <!-- #HEADER\n        		================================================== -->\n            	<tr>\n                	<td width="640"><!-- End top bar table -->\n                    </td>\n                </tr>\n                <tr>\n                <td width="640" bgcolor="#52aae0" align="center">\n    \n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="2" valign="bottom">REMINDER</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'">Email us: <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p></td>\n        </tr>\n        \n    </tbody></table>\n    \n    <!-- End header -->\n</td>\n                </tr>\n                <!-- #CONTENT\n        		================================================== -->\n                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>\n                <tr>\n                <td width="640" bgcolor="#ffffff">\n    <table width="640" cellspacing="6" cellpadding="6" border="0">\n        <tbody><tr>\n            <td width="30"></td>\n            <td width="580">\n                <!-- #TEXT ONLY\n                ================================================== -->\n                <table width="580" cellspacing="6" cellpadding="6" border="0">\n                    <tbody><tr>\n                        <td width="405">\n                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>\n                            \n							\n				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">We are pleased to confirm the site visit on behalf of [BRAND_NAME] will take place on [APPOINTMENT_DATE]. The time slot allocated is [TIME_SLOT] and although we will endeavour to arrive on time traffic conditions can make this difficult to achieve, therefore, we will confirm the actual appointment time by [CUST_CONTACT_TYPE] [CUST_CONTACT_TIME] minutes before we are due to arrive.</p>\n                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'"></p>\n                                \n\n                        </td>\n                        <td width="175">\n						                        \n                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />\n                       \n					    </td>\n                    </tr>\n                   \n                </tbody></table>\n                <table>\n                <tbody>\n                 <tr><td width="580" height="5"></td></tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                    <tr><td width="580" height="5"></td></tr>\n                    </tbody>\n                    </table>\n                                        \n                <!-- #TEXT WITH ADDRESS\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="2" border="0">\n                    <tbody>\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;"  >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="150" colspan="2" valign="bottom">ADDRESS DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;"  >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_TITLE_FIRST_LAST_NAME], <br>[CUSTOMER_ADDRESS]</p>\n                        </td>\n                    </tr>\n					\n					\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">CONTACT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_PHONE_NO][CUSTOMER_MOBILE_NO][CUSTOMER_EMAIL_PREFIX]<a href="mailto:[CUSTOMER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[CUSTOMER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;font-size:13px;font-family:\'Calibri\'">SERVICE CENTRE</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[SERVICE_CENTRE_ADDRESS].<br>[SERVICE_PROVIDER_TELNO_HTML][SERVICE_CENTRE_EMAIL_PREFIX]<a href="mailto:[SERVICE_PROVIDER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[SERVICE_PROVIDER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Job Number: [SC_JOB_NUMBER]&nbsp;&nbsp;&nbsp;&nbsp;Job Booked: [BOOKED_DATE] <br>Manufacturer: [MANUFACTURER_NAME]&nbsp;&nbsp;&nbsp;&nbsp;<br>Product: [UNIT_TYPE_NAME]&nbsp;&nbsp;&nbsp;&nbsp; <br>Model No: [MODEL_NO] <br>Service Type: [SERVICE_TYPE]&nbsp;&nbsp;&nbsp;&nbsp;Location: [PRODUCT_LOCATION]</p>\n                        </td>\n                    </tr>\n			    </tbody></table>\n                                        \n           \n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                </tbody></table>\n                                        \n                <!-- #TEXT FOOTER\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="100">\n                    \n\n<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:\'Calibri\'">Please email <a style="font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a> immediately if there are any discrepancies within the information. Alternatively, if you prefer to talk to a person please call [SERVICE_PROVIDER_NAME] \non [SERVICE_PROVIDER_TELNO] and quote [SC_JOB_NUMBER]    \n<br><br></p>\n<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'">Assuring you of our best intention at all times.</p>\n\n<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'">Yours Sincerely, [BRAND_NAME] Customer Service Team</p>\n                    </td>\n                    </tr>\n                </tbody></table>\n                                                                                \n                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY\n                ================================================== -->\n               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td valign="top">\n                          \n                        </td>\n                       \n                    </tr>\n                </tbody></table>-->\n                                        \n                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE\n                ================================================== -->\n                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td width="180" valign="top">\n                          \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                       \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                            \n                        </td>\n                    </tr>\n                </tbody></table>-->\n            </td>\n            <td width="30"></td>\n        </tr>\n    </tbody></table>\n</td>\n                </tr>\n                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->\n                <!-- #FOOTER\n   				 ================================================== -->\n                <tr>\n                <td width="640">\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">\n <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'">If you have any queries contact us on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p>\n            </td>\n            </tr>\n\n    </tbody></table>\n</td>\n                </tr>\n               \n            </tbody></table>\n        </td>\n	</tr>\n</tbody></table>\n</body>\n</html>\n', '2013-04-04 11:03:56', 1),
	(12, 'appointment_reminder_crm', 'Appointment Reminder <Job No>', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n<title>[PAGE_TITLE]</title>\n</head>\n\n<body>\n<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">\n	<tbody><tr>\n		<td bgcolor="#ffffff" align="center">\n        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">\n            	<tbody>\n                \n                <!-- #HEADER\n        		================================================== -->\n            	<tr>\n                	<td width="640"><!-- End top bar table -->\n                    </td>\n                </tr>\n                <tr>\n                <td width="640" bgcolor="#52aae0" align="center">\n    \n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="2" valign="bottom">REMINDER</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'">Email us: <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p></td>\n        </tr>\n        \n    </tbody></table>\n    \n    <!-- End header -->\n</td>\n                </tr>\n                <!-- #CONTENT\n        		================================================== -->\n                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>\n                <tr>\n                <td width="640" bgcolor="#ffffff">\n    <table width="640" cellspacing="6" cellpadding="6" border="0">\n        <tbody><tr>\n            <td width="30"></td>\n            <td width="580">\n                <!-- #TEXT ONLY\n                ================================================== -->\n                <table width="580" cellspacing="6" cellpadding="6" border="0">\n                    <tbody><tr>\n                        <td width="405">\n                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>\n                            \n							\n				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">We are pleased to confirm the site visit on behalf of [BRAND_NAME] will take place on [APPOINTMENT_DATE]. The time slot allocated is [TIME_SLOT] and although we will endeavour to arrive on time traffic conditions can make this difficult to achieve, therefore, we will confirm the actual appointment time by [CUST_CONTACT_TYPE] [CUST_CONTACT_TIME] minutes before we are due to arrive.</p>\n                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Please be aware that you can access <a style="font-family:\'Calibri\'" href="[CUSTOMER_WEBSITE_URL]" target="_blank" >[CUSTOMER_WEBSITE_NAME]</a> to view a web page specifically created to allow you to manage your service life cycle on-line and receive real time progress reports.  You will be able to monitor your engineers progress when he is on route to your location and you can also login to <a style="font-family:\'Calibri\'" href="[CUSTOMER_WEBSITE_URL]" target="_blank" >[CUSTOMER_WEBSITE_NAME]</a> if you wish to cancel or re-schedule your appointment.</p>\n                                \n\n                        </td>\n                        <td width="175">\n						                        \n                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />\n                       \n					    </td>\n                    </tr>\n                   \n                </tbody></table>\n                <table>\n                <tbody>\n                 <tr><td width="580" height="5"></td></tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                    <tr><td width="580" height="5"></td></tr>\n                    </tbody>\n                    </table>\n                                        \n                <!-- #TEXT WITH ADDRESS\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="2" border="0">\n                    <tbody>\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;"  >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="150" colspan="2" valign="bottom">ADDRESS DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;"  >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_TITLE_FIRST_LAST_NAME], <br>[CUSTOMER_ADDRESS]</p>\n                        </td>\n                    </tr>\n					\n					\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">CONTACT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_PHONE_NO][CUSTOMER_MOBILE_NO][CUSTOMER_EMAIL_PREFIX]<a href="mailto:[CUSTOMER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[CUSTOMER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;font-size:13px;font-family:\'Calibri\'">SERVICE CENTRE</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[SERVICE_CENTRE_ADDRESS].<br>[SERVICE_PROVIDER_TELNO_HTML][SERVICE_CENTRE_EMAIL_PREFIX]<a href="mailto:[SERVICE_PROVIDER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[SERVICE_PROVIDER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Job Number: [SC_JOB_NUMBER]&nbsp;&nbsp;&nbsp;&nbsp;Job Booked: [BOOKED_DATE] <br>Manufacturer: [MANUFACTURER_NAME]&nbsp;&nbsp;&nbsp;&nbsp;<br>Product: [UNIT_TYPE_NAME]&nbsp;&nbsp;&nbsp;&nbsp; <br>Model No: [MODEL_NO] <br>Service Type: [SERVICE_TYPE]&nbsp;&nbsp;&nbsp;&nbsp;Location: [PRODUCT_LOCATION]</p>\n                        </td>\n                    </tr>\n			    </tbody></table>\n                                        \n           \n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                </tbody></table>\n                                        \n                <!-- #TEXT FOOTER\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="100">\n                    \n\n<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:\'Calibri\'">Please email <a style="font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a> immediately if there are any discrepancies within the information. Alternatively, if you prefer to talk to a person please call [SERVICE_PROVIDER_NAME] \non [SERVICE_PROVIDER_TELNO] and quote [SC_JOB_NUMBER]    \n<br><br></p>\n<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'">Assuring you of our best intention at all times.</p>\n\n<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'">Yours Sincerely, [BRAND_NAME] Customer Service Team</p>\n                    </td>\n                    </tr>\n                </tbody></table>\n                                                                                \n                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY\n                ================================================== -->\n               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td valign="top">\n                          \n                        </td>\n                       \n                    </tr>\n                </tbody></table>-->\n                                        \n                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE\n                ================================================== -->\n                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td width="180" valign="top">\n                          \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                       \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                            \n                        </td>\n                    </tr>\n                </tbody></table>-->\n            </td>\n            <td width="30"></td>\n        </tr>\n    </tbody></table>\n</td>\n                </tr>\n                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->\n                <!-- #FOOTER\n   				 ================================================== -->\n                <tr>\n                <td width="640">\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">\n <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'">If you have any queries contact us on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p>\n            </td>\n            </tr>\n\n    </tbody></table>\n</td>\n                </tr>\n               \n            </tbody></table>\n        </td>\n	</tr>\n</tbody></table>\n</body>\n</html>\n', '2013-04-04 12:42:03', 1),
	(13, 'appointment_reminder_smart_service_tracker', 'Appointment Reminder <Job No>', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n<title>[PAGE_TITLE]</title>\n</head>\n\n<body>\n<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">\n	<tbody><tr>\n		<td bgcolor="#ffffff" align="center">\n        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">\n            	<tbody>\n                \n                <!-- #HEADER\n        		================================================== -->\n            	<tr>\n                	<td width="640"><!-- End top bar table -->\n                    </td>\n                </tr>\n                <tr>\n                <td width="640" bgcolor="#52aae0" align="center">\n    \n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="2" valign="bottom">REMINDER</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'">Email us: <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p></td>\n        </tr>\n        \n    </tbody></table>\n    \n    <!-- End header -->\n</td>\n                </tr>\n                <!-- #CONTENT\n        		================================================== -->\n                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>\n                <tr>\n                <td width="640" bgcolor="#ffffff">\n    <table width="640" cellspacing="6" cellpadding="6" border="0">\n        <tbody><tr>\n            <td width="30"></td>\n            <td width="580">\n                <!-- #TEXT ONLY\n                ================================================== -->\n                <table width="580" cellspacing="6" cellpadding="6" border="0">\n                    <tbody><tr>\n                        <td width="405">\n                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>\n                            \n							\n				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">We are pleased to confirm the site visit on behalf of [BRAND_NAME] will take place on [APPOINTMENT_DATE]. The time slot allocated is [TIME_SLOT] and although we will endeavour to arrive on time traffic conditions can make this difficult to achieve, therefore, we will confirm the actual appointment time by [CUST_CONTACT_TYPE] [CUST_CONTACT_TIME] minutes before we are due to arrive.</p>\n                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Although, we will communicate regularly confirming each stage of your service lifecycle you can check progress anytime at <a style="font-family:\'Calibri\'" href="[CUSTOMER_WEBSITE_URL]" target="_blank" >[CUSTOMER_WEBSITE_NAME]</a></p>\n                                \n\n                        </td>\n                        <td width="175">\n						                        \n                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />\n                       \n					    </td>\n                    </tr>\n                   \n                </tbody></table>\n                <table>\n                <tbody>\n                 <tr><td width="580" height="5"></td></tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                    <tr><td width="580" height="5"></td></tr>\n                    </tbody>\n                    </table>\n                                        \n                <!-- #TEXT WITH ADDRESS\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="2" border="0">\n                    <tbody>\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;"  >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="150" colspan="2" valign="bottom">ADDRESS DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;"  >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_TITLE_FIRST_LAST_NAME], <br>[CUSTOMER_ADDRESS]</p>\n                        </td>\n                    </tr>\n					\n					\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">CONTACT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_PHONE_NO][CUSTOMER_MOBILE_NO][CUSTOMER_EMAIL_PREFIX]<a href="mailto:[CUSTOMER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[CUSTOMER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;font-size:13px;font-family:\'Calibri\'">SERVICE CENTRE</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[SERVICE_CENTRE_ADDRESS].<br>[SERVICE_PROVIDER_TELNO_HTML][SERVICE_CENTRE_EMAIL_PREFIX]<a href="mailto:[SERVICE_PROVIDER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[SERVICE_PROVIDER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Job Number: [SC_JOB_NUMBER]&nbsp;&nbsp;&nbsp;&nbsp;Job Booked: [BOOKED_DATE] <br>Manufacturer: [MANUFACTURER_NAME]&nbsp;&nbsp;&nbsp;&nbsp;<br>Product: [UNIT_TYPE_NAME]&nbsp;&nbsp;&nbsp;&nbsp; <br>Model No: [MODEL_NO] <br>Service Type: [SERVICE_TYPE]&nbsp;&nbsp;&nbsp;&nbsp;Location: [PRODUCT_LOCATION]</p>\n                        </td>\n                    </tr>\n			    </tbody></table>\n                                        \n           \n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                </tbody></table>\n                                        \n                <!-- #TEXT FOOTER\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="100">\n                    \n\n<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:\'Calibri\'">Please email <a style="font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a> immediately if there are any discrepancies within the information. Alternatively, if you prefer to talk to a person please call [SERVICE_PROVIDER_NAME] \non [SERVICE_PROVIDER_TELNO] and quote [SC_JOB_NUMBER]    \n<br><br></p>\n<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'">Assuring you of our best intention at all times.</p>\n\n<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'">Yours Sincerely, [BRAND_NAME] Customer Service Team</p>\n                    </td>\n                    </tr>\n                </tbody></table>\n                                                                                \n                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY\n                ================================================== -->\n               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td valign="top">\n                          \n                        </td>\n                       \n                    </tr>\n                </tbody></table>-->\n                                        \n                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE\n                ================================================== -->\n                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td width="180" valign="top">\n                          \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                       \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                            \n                        </td>\n                    </tr>\n                </tbody></table>-->\n            </td>\n            <td width="30"></td>\n        </tr>\n    </tbody></table>\n</td>\n                </tr>\n                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->\n                <!-- #FOOTER\n   				 ================================================== -->\n                <tr>\n                <td width="640">\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">\n <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'">If you have any queries contact us on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p>\n            </td>\n            </tr>\n\n    </tbody></table>\n</td>\n                </tr>\n               \n            </tbody></table>\n        </td>\n	</tr>\n</tbody></table>\n</body>\n</html>\n', '2013-04-04 12:46:53', 1),
	(14, 'unable_to_authorise_repair', 'Unable To Authorise Repair <Job No>', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n<title>[PAGE_TITLE]</title>\n</head>\n\n<body>\n<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">\n	<tbody><tr>\n		<td bgcolor="#ffffff" align="center">\n        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">\n            	<tbody>\n                \n                <!-- #HEADER\n        		================================================== -->\n            	<tr>\n                	<td width="640"><!-- End top bar table -->\n                    </td>\n                </tr>\n                <tr>\n                <td width="640" bgcolor="#52aae0" align="center">\n    \n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        \n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="3" valign="bottom">UNABLE TO AUTHORISE REPAIR</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'">Email us: <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[WA_MANAGER_EMAIL]" >[WA_MANAGER_EMAIL]</a></p></td>\n        </tr>\n        \n    </tbody></table>\n    \n    <!-- End header -->\n</td>\n                </tr>\n                <!-- #CONTENT\n        		================================================== -->\n                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>\n                <tr>\n                <td width="640" bgcolor="#ffffff">\n    <table width="640" cellspacing="6" cellpadding="6" border="0">\n        <tbody><tr>\n            <td width="30"></td>\n            <td width="580">\n                <!-- #TEXT ONLY\n                ================================================== -->\n                <table width="580" cellspacing="6" cellpadding="6" border="0">\n                    <tbody><tr>\n                        <td width="405">\n                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>\n                            \n							\n				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">[MANUFACTURER_NAME] has been unable to authorise your request for a repair under the manufacturer\'s warranty scheme for the customer detailed below. </p>\n                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Please check the booking system to confirm if any further action is required.</p>\n                                \n\n                        </td>\n                        <td width="175">\n						                        \n                          <img width="[MANUFACTURER_LOGO_WIDTH]" height="[MANUFACTURER_LOGO_HEIGHT]" alt="[MANUFACTURER_NAME]"  title="[MANUFACTURER_NAME]" src="[MANUFACTURER_LOGO]" />\n                       \n					    </td>\n                    </tr>\n                   \n                </tbody></table>\n                <table>\n                <tbody>\n                 <tr><td width="580" height="5"></td></tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                    <tr><td width="580" height="5"></td></tr>\n                    </tbody>\n                    </table>\n                                        \n                <!-- #TEXT WITH ADDRESS\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="2" border="0">\n                    <tbody>\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;"  >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="181" colspan="3" valign="bottom">ADDRESS DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;"  >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_TITLE_FIRST_LAST_NAME], <br>[CUSTOMER_ADDRESS]</p>\n                        </td>\n                    </tr>\n					\n					\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          \n                          <td width="181" height="5" colspan="3" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">CONTACT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_PHONE_NO][CUSTOMER_MOBILE_NO][CUSTOMER_EMAIL_PREFIX]<a href="mailto:[CUSTOMER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[CUSTOMER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                  \n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  \n                          <td width="181" height="5" colspan="3" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Job Number: [SC_JOB_NUMBER]&nbsp;&nbsp;&nbsp;&nbsp;Job Booked: [BOOKED_DATE] <br>Manufacturer: [MANUFACTURER_NAME]&nbsp;&nbsp;&nbsp;&nbsp;<br>Product: [UNIT_TYPE_NAME]&nbsp;&nbsp;&nbsp;&nbsp; <br>Model No: [MODEL_NO] <br>Service Type: [SERVICE_TYPE]&nbsp;&nbsp;&nbsp;&nbsp;Location: [PRODUCT_LOCATION]</p>\n                        </td>\n                    </tr>\n			    </tbody></table>\n                                        \n           \n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                </tbody></table>\n                                        \n                <!-- #TEXT FOOTER\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="100">\n                    \n\n						<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:\'Calibri\'">If you are unclear on this instruction please contact the [MANUFACTURER_NAME] warranty authorisation manager [WA_MANAGER_NAME] <a style="font-family:\'Calibri\'" href="mailto:[WA_MANAGER_EMAIL]" >[WA_MANAGER_EMAIL]</a>  \n						<br><br></p>\n						<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'"></p>\n\n						<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'"></p>\n                    \n					</td>\n                    </tr>\n                </tbody></table>\n                                                                                \n                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY\n                ================================================== -->\n               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td valign="top">\n                          \n                        </td>\n                       \n                    </tr>\n                </tbody></table>-->\n                                        \n                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE\n                ================================================== -->\n                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td width="180" valign="top">\n                          \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                       \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                            \n                        </td>\n                    </tr>\n                </tbody></table>-->\n            </td>\n            <td width="30"></td>\n        </tr>\n    </tbody></table>\n</td>\n                </tr>\n                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->\n                <!-- #FOOTER\n   				 ================================================== -->\n                <tr>\n                <td width="640">\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">\n <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'">If you have any queries contact us on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[WA_MANAGER_EMAIL]">[WA_MANAGER_EMAIL]</a></p>\n            </td>\n            </tr>\n\n    </tbody></table>\n</td>\n                </tr>\n               \n            </tbody></table>\n        </td>\n	</tr>\n</tbody></table>\n</body>\n</html>\n', '2013-04-04 14:29:27', 1),
	(15, 'query_raised_against_authorisation', 'Query Raised Against Authorisation <Job No>', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n<title>[PAGE_TITLE]</title>\n</head>\n\n<body>\n<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">\n	<tbody><tr>\n		<td bgcolor="#ffffff" align="center">\n        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">\n            	<tbody>\n                \n                <!-- #HEADER\n        		================================================== -->\n            	<tr>\n                	<td width="640"><!-- End top bar table -->\n                    </td>\n                </tr>\n                <tr>\n                <td width="640" bgcolor="#52aae0" align="center">\n    \n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        \n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="3" valign="bottom">QUERY RAISED AGAINST AUTHORISATION</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'">Email us: <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[WA_MANAGER_EMAIL]" >[WA_MANAGER_EMAIL]</a></p></td>\n        </tr>\n        \n    </tbody></table>\n    \n    <!-- End header -->\n</td>\n                </tr>\n                <!-- #CONTENT\n        		================================================== -->\n                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>\n                <tr>\n                <td width="640" bgcolor="#ffffff">\n    <table width="640" cellspacing="6" cellpadding="6" border="0">\n        <tbody><tr>\n            <td width="30"></td>\n            <td width="580">\n                <!-- #TEXT ONLY\n                ================================================== -->\n                <table width="580" cellspacing="6" cellpadding="6" border="0">\n                    <tbody><tr>\n                        <td width="405">\n                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>\n                            \n							\n				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">[MANUFACTURER_NAME] is unable to authorise your request for a repair under the manufacturer\'s warranty scheme because they require further information.</p>\n                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Please check the booking system to confirm if any further action is required.</p>\n                                \n\n                        </td>\n                        <td width="175">\n						                        \n                          <img width="[MANUFACTURER_LOGO_WIDTH]" height="[MANUFACTURER_LOGO_HEIGHT]" alt="[MANUFACTURER_NAME]"  title="[MANUFACTURER_NAME]" src="[MANUFACTURER_LOGO]" />\n                       \n					    </td>\n                    </tr>\n                   \n                </tbody></table>\n                <table>\n                <tbody>\n                 <tr><td width="580" height="5"></td></tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                    <tr><td width="580" height="5"></td></tr>\n                    </tbody>\n                    </table>\n                                        \n                <!-- #TEXT WITH ADDRESS\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="2" border="0">\n                    <tbody>\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;"  >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="181" colspan="3" valign="bottom">ADDRESS DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;"  >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_TITLE_FIRST_LAST_NAME], <br>[CUSTOMER_ADDRESS]</p>\n                        </td>\n                    </tr>\n					\n					\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          \n                          <td width="181" height="5" colspan="3" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">CONTACT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_PHONE_NO][CUSTOMER_MOBILE_NO][CUSTOMER_EMAIL_PREFIX]<a href="mailto:[CUSTOMER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[CUSTOMER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                  \n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  \n                          <td width="181" height="5" colspan="3" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Job Number: [SC_JOB_NUMBER]&nbsp;&nbsp;&nbsp;&nbsp;Job Booked: [BOOKED_DATE] <br>Manufacturer: [MANUFACTURER_NAME]&nbsp;&nbsp;&nbsp;&nbsp;<br>Product: [UNIT_TYPE_NAME]&nbsp;&nbsp;&nbsp;&nbsp; <br>Model No: [MODEL_NO] <br>Service Type: [SERVICE_TYPE]&nbsp;&nbsp;&nbsp;&nbsp;Location: [PRODUCT_LOCATION]</p>\n                        </td>\n                    </tr>\n			    </tbody></table>\n                                        \n           \n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                </tbody></table>\n                                        \n                <!-- #TEXT FOOTER\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="100">\n                    \n\n						<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:\'Calibri\'">If you are unclear on this instruction please contact the [MANUFACTURER_NAME] warranty authorisation manager [WA_MANAGER_NAME] <a style="font-family:\'Calibri\'" href="mailto:[WA_MANAGER_EMAIL]" >[WA_MANAGER_EMAIL]</a>  \n						<br><br></p>\n						<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'"></p>\n\n						<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'"></p>\n                    \n					</td>\n                    </tr>\n                </tbody></table>\n                                                                                \n                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY\n                ================================================== -->\n               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td valign="top">\n                          \n                        </td>\n                       \n                    </tr>\n                </tbody></table>-->\n                                        \n                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE\n                ================================================== -->\n                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td width="180" valign="top">\n                          \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                       \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                            \n                        </td>\n                    </tr>\n                </tbody></table>-->\n            </td>\n            <td width="30"></td>\n        </tr>\n    </tbody></table>\n</td>\n                </tr>\n                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->\n                <!-- #FOOTER\n   				 ================================================== -->\n                <tr>\n                <td width="640">\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">\n <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'">If you have any queries contact us on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[WA_MANAGER_EMAIL]">[WA_MANAGER_EMAIL]</a></p>\n            </td>\n            </tr>\n\n    </tbody></table>\n</td>\n                </tr>\n               \n            </tbody></table>\n        </td>\n	</tr>\n</tbody></table>\n</body>\n</html>\n', '2013-04-04 14:25:28', 1),
	(16, 'job_complete', 'Job Complete <Job No>', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\r\n<html xmlns="http://www.w3.org/1999/xhtml">\r\n<head>\r\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\r\n<title>[PAGE_TITLE]</title>\r\n</head>\r\n\r\n<body>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">\r\n	<tbody><tr>\r\n		<td bgcolor="#ffffff" align="center">\r\n        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">\r\n            	<tbody>\r\n                \r\n                <!-- #HEADER\r\n        		================================================== -->\r\n            	<tr>\r\n                	<td width="640"><!-- End top bar table -->\r\n                    </td>\r\n                </tr>\r\n                <tr>\r\n                <td width="640" bgcolor="#52aae0" align="center">\r\n    \r\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\r\n        <tbody><tr>\r\n        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>\r\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="2" valign="bottom">CUSTOMER RECEIPT</td>\r\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'">Email us: <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[NETWORK_EMAIL]" >[NETWORK_EMAIL]</a></p></td>\r\n        </tr>\r\n        \r\n    </tbody></table>\r\n    \r\n    <!-- End header -->\r\n</td>\r\n                </tr>\r\n                <!-- #CONTENT\r\n        		================================================== -->\r\n                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>\r\n                <tr>\r\n                <td width="640" bgcolor="#ffffff">\r\n    <table width="640" cellspacing="6" cellpadding="6" border="0">\r\n        <tbody><tr>\r\n            <td width="30"></td>\r\n            <td width="580">\r\n                <!-- #TEXT ONLY\r\n                ================================================== -->\r\n                <table width="580" cellspacing="6" cellpadding="6" border="0">\r\n                    <tbody><tr>\r\n                        <td width="405">\r\n                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>\r\n                            \r\n							\r\n				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">Following the recent service we supplied we sincerely hope that we satisfied your requirement and that the standard of service exceeded your expectations.  Should you experience any issues with this product or any other similar products, please feel free to contact us to arrange a new service request.</p>\r\n                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">If you feel that the service you supplied met with your expectations please complete the following questionnaire.</p>\r\n                                \r\n\r\n                        </td>\r\n                        <td width="175">\r\n						                        \r\n                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />\r\n                       \r\n					    </td>\r\n                    </tr>\r\n                   \r\n                </tbody></table>\r\n                <table>\r\n                <tbody>\r\n                \r\n                    <tr><td width="580" height="2" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\r\n                   \r\n                    </tbody>\r\n                    </table>\r\n                                        \r\n                <!-- #TEXT WITH ADDRESS\r\n                ================================================== -->\r\n                <table width="580" cellspacing="0" cellpadding="2" border="0">\r\n                    <tbody>\r\n					\r\n                    <tr>\r\n                        <td colspan="2" style="padding-bottom:25px;text-align:center;"  ><br>\r\n                          <a href="[SERVICE_QUESTIONNAIRE]" style="color:#3576bc;font-size:16px;font-weight:bold;" >Complete our Service Questionnaire</a>\r\n                       </td>\r\n                        \r\n                    </tr>\r\n					\r\n					\r\n			    </tbody></table>\r\n                                        \r\n           \r\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\r\n                    <tbody>\r\n                    <tr><td width="580" height="3" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\r\n                </tbody></table>\r\n                                        \r\n                <!-- #TEXT FOOTER\r\n                ================================================== -->\r\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\r\n                    <tbody><tr>\r\n                        \r\n                    </tr>\r\n                    <tr><td width="580" height="100">\r\n                    \r\n\r\n<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:\'Calibri\'"></p>\r\n<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'">Assuring you of our best intention at all times.</p>\r\n\r\n<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'">Yours Sincerely, [BRAND_NAME] Customer Service Team</p>\r\n                    </td>\r\n                    </tr>\r\n                </tbody></table>\r\n                                                                                \r\n                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY\r\n                ================================================== -->\r\n               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">\r\n                    <tbody><tr>\r\n                        <td valign="top">\r\n                          \r\n                        </td>\r\n                       \r\n                    </tr>\r\n                </tbody></table>-->\r\n                                        \r\n                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE\r\n                ================================================== -->\r\n                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">\r\n                    <tbody><tr>\r\n                        <td width="180" valign="top">\r\n                          \r\n                        </td>\r\n                        <td width="20"></td>\r\n                        <td width="180" valign="top">\r\n                       \r\n                        </td>\r\n                        <td width="20"></td>\r\n                        <td width="180" valign="top">\r\n                            \r\n                        </td>\r\n                    </tr>\r\n                </tbody></table>-->\r\n            </td>\r\n            <td width="30"></td>\r\n        </tr>\r\n    </tbody></table>\r\n</td>\r\n                </tr>\r\n                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->\r\n                <!-- #FOOTER\r\n   				 ================================================== -->\r\n                <tr>\r\n                <td width="640">\r\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\r\n        <tbody><tr>\r\n        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">\r\n <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'">If you have any queries contact us on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[NETWORK_EMAIL]">[NETWORK_EMAIL]</a></p>\r\n            </td>\r\n            </tr>\r\n\r\n    </tbody></table>\r\n</td>\r\n                </tr>\r\n               \r\n            </tbody></table>\r\n        </td>\r\n	</tr>\r\n</tbody></table>\r\n</body>\r\n</html>\r\n', '2013-04-04 08:39:07', 1),
	(17, 'appointment_date_confirmed', 'Appointment - Date Confirmed <Job No>', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n<title>[PAGE_TITLE]</title>\n</head>\n\n<body>\n<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">\n	<tbody><tr>\n		<td bgcolor="#ffffff" align="center">\n        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">\n            	<tbody>\n                \n                <!-- #HEADER\n        		================================================== -->\n            	<tr>\n                	<td width="640"><!-- End top bar table -->\n                    </td>\n                </tr>\n                <tr>\n                <td width="640" bgcolor="#52aae0" align="center">\n    \n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="2" valign="bottom">APPOINTMENT</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'">Email us: <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p></td>\n        </tr>\n        \n    </tbody></table>\n    \n    <!-- End header -->\n</td>\n                </tr>\n                <!-- #CONTENT\n        		================================================== -->\n                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>\n                <tr>\n                <td width="640" bgcolor="#ffffff">\n    <table width="640" cellspacing="6" cellpadding="6" border="0">\n        <tbody><tr>\n            <td width="30"></td>\n            <td width="580">\n                <!-- #TEXT ONLY\n                ================================================== -->\n                <table width="580" cellspacing="6" cellpadding="6" border="0">\n                    <tbody><tr>\n                        <td width="405">\n                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>\n                            \n							\n				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">\n								Following your recent contact with our call centre we are pleased to arrange  a site visit on behalf of [BRAND_NAME]. We are their authorised service provider and one of our agents will be with you on [APPOINTMENT_DATE].  We understand your preferred appointment time slot is [TIME_SLOT].  We will do all we can to accommodate this request; however, if there is a problem we will of course contact you.  In any event, we will email you the day before the appointment to confirm these arrangements.\n								</p>\n                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'"></p>\n                                \n\n                        </td>\n                        <td width="175">\n						                        \n                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />\n                       \n					    </td>\n                    </tr>\n                   \n                </tbody></table>\n                <table>\n                <tbody>\n                 <tr><td width="580" height="5"></td></tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                    <tr><td width="580" height="5"></td></tr>\n                    </tbody>\n                    </table>\n                                        \n                <!-- #TEXT WITH ADDRESS\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="2" border="0">\n                    <tbody>\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;"  >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="150" colspan="2" valign="bottom">ADDRESS DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;"  >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_TITLE_FIRST_LAST_NAME], <br>[CUSTOMER_ADDRESS]</p>\n                        </td>\n                    </tr>\n					\n					\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">CONTACT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_PHONE_NO][CUSTOMER_MOBILE_NO][CUSTOMER_EMAIL_PREFIX]<a href="mailto:[CUSTOMER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[CUSTOMER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;font-size:13px;font-family:\'Calibri\'">SERVICE CENTRE</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[SERVICE_CENTRE_ADDRESS].<br>[SERVICE_PROVIDER_TELNO_HTML][SERVICE_CENTRE_EMAIL_PREFIX]<a href="mailto:[SERVICE_PROVIDER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[SERVICE_PROVIDER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Job Number: [SC_JOB_NUMBER]&nbsp;&nbsp;&nbsp;&nbsp;Job Booked: [BOOKED_DATE] <br>Manufacturer: [MANUFACTURER_NAME]&nbsp;&nbsp;&nbsp;&nbsp;<br>Product: [UNIT_TYPE_NAME]&nbsp;&nbsp;&nbsp;&nbsp; <br>Model No: [MODEL_NO] <br>Service Type: [SERVICE_TYPE]&nbsp;&nbsp;&nbsp;&nbsp;Location: [PRODUCT_LOCATION]</p>\n                        </td>\n                    </tr>\n			    </tbody></table>\n                                        \n           \n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                </tbody></table>\n                                        \n                <!-- #TEXT FOOTER\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="100">\n                    \n\n<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:\'Calibri\'">Please email <a style="font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a> immediately if there are any discrepancies within the information. Alternatively, if you prefer to talk to a person please call [SERVICE_PROVIDER_NAME] \non [SERVICE_PROVIDER_TELNO] and quote [SC_JOB_NUMBER]    \n<br><br></p>\n<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'">Assuring you of our best intention at all times.</p>\n\n<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'">Yours Sincerely, [BRAND_NAME] Customer Service Team</p>\n                    </td>\n                    </tr>\n                </tbody></table>\n                                                                                \n                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY\n                ================================================== -->\n               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td valign="top">\n                          \n                        </td>\n                       \n                    </tr>\n                </tbody></table>-->\n                                        \n                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE\n                ================================================== -->\n                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td width="180" valign="top">\n                          \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                       \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                            \n                        </td>\n                    </tr>\n                </tbody></table>-->\n            </td>\n            <td width="30"></td>\n        </tr>\n    </tbody></table>\n</td>\n                </tr>\n                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->\n                <!-- #FOOTER\n   				 ================================================== -->\n                <tr>\n                <td width="640">\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">\n <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'">If you have any queries contact us on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p>\n            </td>\n            </tr>\n\n    </tbody></table>\n</td>\n                </tr>\n               \n            </tbody></table>\n        </td>\n	</tr>\n</tbody></table>\n</body>\n</html>\n', '2013-04-04 12:53:26', 1),
	(18, 'appointment_date_confirmed_crm', 'Appointment - Date Confirmed <Job No>', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n<title>[PAGE_TITLE]</title>\n</head>\n\n<body>\n<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">\n	<tbody><tr>\n		<td bgcolor="#ffffff" align="center">\n        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">\n            	<tbody>\n                \n                <!-- #HEADER\n        		================================================== -->\n            	<tr>\n                	<td width="640"><!-- End top bar table -->\n                    </td>\n                </tr>\n                <tr>\n                <td width="640" bgcolor="#52aae0" align="center">\n    \n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="2" valign="bottom">APPOINTMENT</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'">Email us: <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p></td>\n        </tr>\n        \n    </tbody></table>\n    \n    <!-- End header -->\n</td>\n                </tr>\n                <!-- #CONTENT\n        		================================================== -->\n                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>\n                <tr>\n                <td width="640" bgcolor="#ffffff">\n    <table width="640" cellspacing="6" cellpadding="6" border="0">\n        <tbody><tr>\n            <td width="30"></td>\n            <td width="580">\n                <!-- #TEXT ONLY\n                ================================================== -->\n                <table width="580" cellspacing="6" cellpadding="6" border="0">\n                    <tbody><tr>\n                        <td width="405">\n                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>\n                            \n							\n				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">\n								Following your recent contact with our call centre we are pleased to arrange  a site visit on behalf of [BRAND_NAME]. We are their authorised service provider and one of our agents will be with you on [APPOINTMENT_DATE].  We understand your preferred appointment time slot is [TIME_SLOT].  We will do all we can to accommodate this request; however, if there is a problem we will of course contact you.  In any event, we will email you the day before the appointment to confirm these arrangements.\n								\n								</p>\n                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">\n								\n								Please be aware that you can access <a style="font-family:\'Calibri\'" href="[CUSTOMER_WEBSITE_URL]" target="_blank" >[CUSTOMER_WEBSITE_NAME]</a> to view a web page specifically created to allow you to manage your service life cycle on-line and receive real time progress reports.  You will be able to monitor your engineers progress when he is on route to your location and you can also login to <a style="font-family:\'Calibri\'" href="[CUSTOMER_WEBSITE_URL]" target="_blank" >[CUSTOMER_WEBSITE_NAME]</a> if you wish to cancel or re-schedule your appointment.\n								</p>\n                                \n\n                        </td>\n                        <td width="175">\n						                        \n                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />\n                       \n					    </td>\n                    </tr>\n                   \n                </tbody></table>\n                <table>\n                <tbody>\n                 <tr><td width="580" height="5"></td></tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                    <tr><td width="580" height="5"></td></tr>\n                    </tbody>\n                    </table>\n                                        \n                <!-- #TEXT WITH ADDRESS\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="2" border="0">\n                    <tbody>\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;"  >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="150" colspan="2" valign="bottom">ADDRESS DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;"  >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_TITLE_FIRST_LAST_NAME], <br>[CUSTOMER_ADDRESS]</p>\n                        </td>\n                    </tr>\n					\n					\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">CONTACT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_PHONE_NO][CUSTOMER_MOBILE_NO][CUSTOMER_EMAIL_PREFIX]<a href="mailto:[CUSTOMER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[CUSTOMER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;font-size:13px;font-family:\'Calibri\'">SERVICE CENTRE</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[SERVICE_CENTRE_ADDRESS].<br>[SERVICE_PROVIDER_TELNO_HTML][SERVICE_CENTRE_EMAIL_PREFIX]<a href="mailto:[SERVICE_PROVIDER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[SERVICE_PROVIDER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Job Number: [SC_JOB_NUMBER]&nbsp;&nbsp;&nbsp;&nbsp;Job Booked: [BOOKED_DATE] <br>Manufacturer: [MANUFACTURER_NAME]&nbsp;&nbsp;&nbsp;&nbsp;<br>Product: [UNIT_TYPE_NAME]&nbsp;&nbsp;&nbsp;&nbsp; <br>Model No: [MODEL_NO] <br>Service Type: [SERVICE_TYPE]&nbsp;&nbsp;&nbsp;&nbsp;Location: [PRODUCT_LOCATION]</p>\n                        </td>\n                    </tr>\n			    </tbody></table>\n                                        \n           \n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                </tbody></table>\n                                        \n                <!-- #TEXT FOOTER\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="100">\n                    \n\n<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:\'Calibri\'">Please email <a style="font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a> immediately if there are any discrepancies within the information. Alternatively, if you prefer to talk to a person please call [SERVICE_PROVIDER_NAME] \non [SERVICE_PROVIDER_TELNO] and quote [SC_JOB_NUMBER]    \n<br><br></p>\n<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'">Assuring you of our best intention at all times.</p>\n\n<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'">Yours Sincerely, [BRAND_NAME] Customer Service Team</p>\n                    </td>\n                    </tr>\n                </tbody></table>\n                                                                                \n                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY\n                ================================================== -->\n               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td valign="top">\n                          \n                        </td>\n                       \n                    </tr>\n                </tbody></table>-->\n                                        \n                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE\n                ================================================== -->\n                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td width="180" valign="top">\n                          \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                       \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                            \n                        </td>\n                    </tr>\n                </tbody></table>-->\n            </td>\n            <td width="30"></td>\n        </tr>\n    </tbody></table>\n</td>\n                </tr>\n                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->\n                <!-- #FOOTER\n   				 ================================================== -->\n                <tr>\n                <td width="640">\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">\n <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'">If you have any queries contact us on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p>\n            </td>\n            </tr>\n\n    </tbody></table>\n</td>\n                </tr>\n               \n            </tbody></table>\n        </td>\n	</tr>\n</tbody></table>\n</body>\n</html>\n', '2013-04-04 13:00:37', 1),
	(19, 'appointment_date_confirmed_smart_service_tracker', 'Appointment - Date Confirmed <Job No>', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n<title>[PAGE_TITLE]</title>\n</head>\n\n<body>\n<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">\n	<tbody><tr>\n		<td bgcolor="#ffffff" align="center">\n        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">\n            	<tbody>\n                \n                <!-- #HEADER\n        		================================================== -->\n            	<tr>\n                	<td width="640"><!-- End top bar table -->\n                    </td>\n                </tr>\n                <tr>\n                <td width="640" bgcolor="#52aae0" align="center">\n    \n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="2" valign="bottom">APPOINTMENT</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'">Email us: <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p></td>\n        </tr>\n        \n    </tbody></table>\n    \n    <!-- End header -->\n</td>\n                </tr>\n                <!-- #CONTENT\n        		================================================== -->\n                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>\n                <tr>\n                <td width="640" bgcolor="#ffffff">\n    <table width="640" cellspacing="6" cellpadding="6" border="0">\n        <tbody><tr>\n            <td width="30"></td>\n            <td width="580">\n                <!-- #TEXT ONLY\n                ================================================== -->\n                <table width="580" cellspacing="6" cellpadding="6" border="0">\n                    <tbody><tr>\n                        <td width="405">\n                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>\n                            \n							\n				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">\n								\n								Following your recent contact with our call centre we are pleased to arrange  a site visit on behalf of [BRAND_NAME]. We are their authorised service provider and one of our agents will be with you on [APPOINTMENT_DATE].  We understand your preferred appointment time slot is [TIME_SLOT].  We will do all we can to accommodate this request; however, if there is a problem we will of course contact you.  In any event, we will email you the day before the appointment to confirm these arrangements.\n								\n								</p>\n                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">\n								\n								Although, we will communicate regularly confirming each stage of your service lifecycle you can check progress anytime at <a style="font-family:\'Calibri\'" href="[CUSTOMER_WEBSITE_URL]" target="_blank" >[CUSTOMER_WEBSITE_NAME]</a>\n								\n								</p>\n                                \n\n                        </td>\n                        <td width="175">\n						                        \n                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />\n                       \n					    </td>\n                    </tr>\n                   \n                </tbody></table>\n                <table>\n                <tbody>\n                 <tr><td width="580" height="5"></td></tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                    <tr><td width="580" height="5"></td></tr>\n                    </tbody>\n                    </table>\n                                        \n                <!-- #TEXT WITH ADDRESS\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="2" border="0">\n                    <tbody>\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;"  >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="150" colspan="2" valign="bottom">ADDRESS DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;"  >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_TITLE_FIRST_LAST_NAME], <br>[CUSTOMER_ADDRESS]</p>\n                        </td>\n                    </tr>\n					\n					\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">CONTACT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_PHONE_NO][CUSTOMER_MOBILE_NO][CUSTOMER_EMAIL_PREFIX]<a href="mailto:[CUSTOMER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[CUSTOMER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;font-size:13px;font-family:\'Calibri\'">SERVICE CENTRE</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[SERVICE_CENTRE_ADDRESS].<br>[SERVICE_PROVIDER_TELNO_HTML][SERVICE_CENTRE_EMAIL_PREFIX]<a href="mailto:[SERVICE_PROVIDER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[SERVICE_PROVIDER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Job Number: [SC_JOB_NUMBER]&nbsp;&nbsp;&nbsp;&nbsp;Job Booked: [BOOKED_DATE] <br>Manufacturer: [MANUFACTURER_NAME]&nbsp;&nbsp;&nbsp;&nbsp;<br>Product: [UNIT_TYPE_NAME]&nbsp;&nbsp;&nbsp;&nbsp; <br>Model No: [MODEL_NO] <br>Service Type: [SERVICE_TYPE]&nbsp;&nbsp;&nbsp;&nbsp;Location: [PRODUCT_LOCATION]</p>\n                        </td>\n                    </tr>\n			    </tbody></table>\n                                        \n           \n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                </tbody></table>\n                                        \n                <!-- #TEXT FOOTER\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="100">\n                    \n\n<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:\'Calibri\'">Please email <a style="font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a> immediately if there are any discrepancies within the information. Alternatively, if you prefer to talk to a person please call [SERVICE_PROVIDER_NAME] \non [SERVICE_PROVIDER_TELNO] and quote [SC_JOB_NUMBER]    \n<br><br></p>\n<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'">Assuring you of our best intention at all times.</p>\n\n<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'">Yours Sincerely, [BRAND_NAME] Customer Service Team</p>\n                    </td>\n                    </tr>\n                </tbody></table>\n                                                                                \n                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY\n                ================================================== -->\n               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td valign="top">\n                          \n                        </td>\n                       \n                    </tr>\n                </tbody></table>-->\n                                        \n                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE\n                ================================================== -->\n                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td width="180" valign="top">\n                          \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                       \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                            \n                        </td>\n                    </tr>\n                </tbody></table>-->\n            </td>\n            <td width="30"></td>\n        </tr>\n    </tbody></table>\n</td>\n                </tr>\n                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->\n                <!-- #FOOTER\n   				 ================================================== -->\n                <tr>\n                <td width="640">\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">\n <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'">If you have any queries contact us on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p>\n            </td>\n            </tr>\n\n    </tbody></table>\n</td>\n                </tr>\n               \n            </tbody></table>\n        </td>\n	</tr>\n</tbody></table>\n</body>\n</html>\n', '2013-04-04 13:06:58', 1),
	(20, 'appraisal_request', 'Appraisal Request <Job No>', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n<title>[PAGE_TITLE]</title>\n</head>\n\n<body>\n<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">\n	<tbody><tr>\n		<td bgcolor="#ffffff" align="center">\n        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">\n            	<tbody>\n                \n                <!-- #HEADER\n        		================================================== -->\n            	<tr>\n                	<td width="640"><!-- End top bar table -->\n                    </td>\n                </tr>\n                <tr>\n                <td width="640" bgcolor="#52aae0" align="center">\n    \n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="2" valign="bottom">APPRAISAL REQUEST</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'">Email us: <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[BRANCH_EMAIL]" >[BRANCH_EMAIL]</a></p></td>\n        </tr>\n        \n    </tbody></table>\n    \n    <!-- End header -->\n</td>\n                </tr>\n                <!-- #CONTENT\n        		================================================== -->\n                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>\n                <tr>\n                <td width="640" bgcolor="#ffffff">\n    <table width="640" cellspacing="6" cellpadding="6" border="0">\n        <tbody><tr>\n            <td width="30"></td>\n            <td width="580">\n                <!-- #TEXT ONLY\n                ================================================== -->\n                <table width="580" cellspacing="6" cellpadding="6" border="0">\n                    <tbody><tr>\n                        <td width="405">\n                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>\n                            \n							\n				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">Thank you for registering your service request with us today. We will arrange for your product to be appraised by our senior technician who will decide upon the most suitable course of action to satisfy your particular requirement and who will notify you accordingly.</p>\n                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'"></p>\n                                \n\n                        </td>\n                        <td width="175">\n						                        \n                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />\n                       \n					    </td>\n                    </tr>\n                   \n                </tbody></table>\n                <table>\n                <tbody>\n                 <tr><td width="580" height="5"></td></tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                    <tr><td width="580" height="5"></td></tr>\n                    </tbody>\n                    </table>\n                                        \n                <!-- #TEXT WITH ADDRESS\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="2" border="0">\n                    <tbody>\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;"  >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="150" colspan="2" valign="bottom">ADDRESS DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;"  >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_TITLE_FIRST_LAST_NAME], <br>[CUSTOMER_ADDRESS]</p>\n                        </td>\n                    </tr>\n					\n					\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">CONTACT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_PHONE_NO][CUSTOMER_MOBILE_NO][CUSTOMER_EMAIL_PREFIX]<a href="mailto:[CUSTOMER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[CUSTOMER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Job Number: [SC_JOB_NUMBER]&nbsp;&nbsp;&nbsp;&nbsp;Job Booked: [BOOKED_DATE] <br>Manufacturer: [MANUFACTURER_NAME]&nbsp;&nbsp;&nbsp;&nbsp;<br>Product: [UNIT_TYPE_NAME]&nbsp;&nbsp;&nbsp;&nbsp; <br>Model No: [MODEL_NO] <br>Service Type: [SERVICE_TYPE]&nbsp;&nbsp;&nbsp;&nbsp;Location: [PRODUCT_LOCATION]</p>\n                        </td>\n                    </tr>\n			    </tbody></table>\n                                        \n           \n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                </tbody></table>\n                                        \n                <!-- #TEXT FOOTER\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="100">\n                    \n\n<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:\'Calibri\'" >Please email <a style="font-family:\'Calibri\'" href="mailto:[BRANCH_EMAIL]" >[BRANCH_EMAIL]</a> immediately if there are any discrepancies within the information \n<br><br></p>\n<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'" >Assuring you of our best intention at all times.</p>\n\n<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'" >Yours Sincerely, [BRANCH_NAME]</p>\n                    </td>\n                    </tr>\n                </tbody></table>\n                                                                                \n                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY\n                ================================================== -->\n               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td valign="top">\n                          \n                        </td>\n                       \n                    </tr>\n                </tbody></table>-->\n                                        \n                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE\n                ================================================== -->\n                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td width="180" valign="top">\n                          \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                       \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                            \n                        </td>\n                    </tr>\n                </tbody></table>-->\n            </td>\n            <td width="30"></td>\n        </tr>\n    </tbody></table>\n</td>\n                </tr>\n                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->\n                <!-- #FOOTER\n   				 ================================================== -->\n                <tr>\n                <td width="640">\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">\n <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'">If you have any queries contact us on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[BRANCH_EMAIL]">[BRANCH_EMAIL]</a></p>\n            </td>\n            </tr>\n\n    </tbody></table>\n</td>\n                </tr>\n               \n            </tbody></table>\n        </td>\n	</tr>\n</tbody></table>\n</body>\n</html>\n', '2013-04-04 13:12:59', 1),
	(21, 'appraisal_request_crm', 'Appraisal Request <Job No>', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n<title>[PAGE_TITLE]</title>\n</head>\n\n<body>\n<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">\n	<tbody><tr>\n		<td bgcolor="#ffffff" align="center">\n        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">\n            	<tbody>\n                \n                <!-- #HEADER\n        		================================================== -->\n            	<tr>\n                	<td width="640"><!-- End top bar table -->\n                    </td>\n                </tr>\n                <tr>\n                <td width="640" bgcolor="#52aae0" align="center">\n    \n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="2" valign="bottom">APPRAISAL REQUEST</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'">Email us: <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[BRANCH_EMAIL]" >[BRANCH_EMAIL]</a></p></td>\n        </tr>\n        \n    </tbody></table>\n    \n    <!-- End header -->\n</td>\n                </tr>\n                <!-- #CONTENT\n        		================================================== -->\n                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>\n                <tr>\n                <td width="640" bgcolor="#ffffff">\n    <table width="640" cellspacing="6" cellpadding="6" border="0">\n        <tbody><tr>\n            <td width="30"></td>\n            <td width="580">\n                <!-- #TEXT ONLY\n                ================================================== -->\n                <table width="580" cellspacing="6" cellpadding="6" border="0">\n                    <tbody><tr>\n                        <td width="405">\n                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>\n                            \n							\n				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">Thank you for registering your service request with us today. We will arrange for your product to be appraised by our senior technician who will decide upon the most suitable course of action to satisfy your particular requirement and who will notify you accordingly.</p>\n                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'" >Please be aware that you can access <a style="font-family:\'Calibri\'" href="[CUSTOMER_WEBSITE_URL]" target="_blank" >[CUSTOMER_WEBSITE_NAME]</a> to view a web page specifically created to allow you to manage your service life cycle on-line and receive real time progress reports.  If you require a site visit you will be able to monitor your engineers progress when he is on route to your location and you can also login to <a style="font-family:\'Calibri\'" href="[CUSTOMER_WEBSITE_URL]" target="_blank" >[CUSTOMER_WEBSITE_NAME]</a> if you wish to cancel or re-schedule your appointment.</p>\n                                \n\n                        </td>\n                        <td width="175">\n						                        \n                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />\n                       \n					    </td>\n                    </tr>\n                   \n                </tbody></table>\n                <table>\n                <tbody>\n                 <tr><td width="580" height="5"></td></tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                    <tr><td width="580" height="5"></td></tr>\n                    </tbody>\n                    </table>\n                                        \n                <!-- #TEXT WITH ADDRESS\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="2" border="0">\n                    <tbody>\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;"  >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="150" colspan="2" valign="bottom">ADDRESS DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;"  >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_TITLE_FIRST_LAST_NAME], <br>[CUSTOMER_ADDRESS]</p>\n                        </td>\n                    </tr>\n					\n					\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">CONTACT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_PHONE_NO][CUSTOMER_MOBILE_NO][CUSTOMER_EMAIL_PREFIX]<a href="mailto:[CUSTOMER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[CUSTOMER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Job Number: [SC_JOB_NUMBER]&nbsp;&nbsp;&nbsp;&nbsp;Job Booked: [BOOKED_DATE] <br>Manufacturer: [MANUFACTURER_NAME]&nbsp;&nbsp;&nbsp;&nbsp;<br>Product: [UNIT_TYPE_NAME]&nbsp;&nbsp;&nbsp;&nbsp; <br>Model No: [MODEL_NO] <br>Service Type: [SERVICE_TYPE]&nbsp;&nbsp;&nbsp;&nbsp;Location: [PRODUCT_LOCATION]</p>\n                        </td>\n                    </tr>\n			    </tbody></table>\n                                        \n           \n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                </tbody></table>\n                                        \n                <!-- #TEXT FOOTER\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="100">\n                    \n\n<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:\'Calibri\'" >Please email <a style="font-family:\'Calibri\'" href="mailto:[BRANCH_EMAIL]" >[BRANCH_EMAIL]</a> immediately if there are any discrepancies within the information \n<br><br></p>\n<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'" >Assuring you of our best intention at all times.</p>\n\n<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'" >Yours Sincerely, [BRANCH_NAME]</p>\n                    </td>\n                    </tr>\n                </tbody></table>\n                                                                                \n                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY\n                ================================================== -->\n               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td valign="top">\n                          \n                        </td>\n                       \n                    </tr>\n                </tbody></table>-->\n                                        \n                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE\n                ================================================== -->\n                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td width="180" valign="top">\n                          \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                       \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                            \n                        </td>\n                    </tr>\n                </tbody></table>-->\n            </td>\n            <td width="30"></td>\n        </tr>\n    </tbody></table>\n</td>\n                </tr>\n                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->\n                <!-- #FOOTER\n   				 ================================================== -->\n                <tr>\n                <td width="640">\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">\n <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'">If you have any queries contact us on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[BRANCH_EMAIL]">[BRANCH_EMAIL]</a></p>\n            </td>\n            </tr>\n\n    </tbody></table>\n</td>\n                </tr>\n               \n            </tbody></table>\n        </td>\n	</tr>\n</tbody></table>\n</body>\n</html>\n', '2013-04-04 13:17:59', 1),
	(22, 'appraisal_request_smart_service_tracker', 'Appraisal Request <Job No>', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n<title>[PAGE_TITLE]</title>\n</head>\n\n<body>\n<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">\n	<tbody><tr>\n		<td bgcolor="#ffffff" align="center">\n        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">\n            	<tbody>\n                \n                <!-- #HEADER\n        		================================================== -->\n            	<tr>\n                	<td width="640"><!-- End top bar table -->\n                    </td>\n                </tr>\n                <tr>\n                <td width="640" bgcolor="#52aae0" align="center">\n    \n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="2" valign="bottom">APPRAISAL REQUEST</td>\n        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'">Email us: <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[BRANCH_EMAIL]" >[BRANCH_EMAIL]</a></p></td>\n        </tr>\n        \n    </tbody></table>\n    \n    <!-- End header -->\n</td>\n                </tr>\n                <!-- #CONTENT\n        		================================================== -->\n                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>\n                <tr>\n                <td width="640" bgcolor="#ffffff">\n    <table width="640" cellspacing="6" cellpadding="6" border="0">\n        <tbody><tr>\n            <td width="30"></td>\n            <td width="580">\n                <!-- #TEXT ONLY\n                ================================================== -->\n                <table width="580" cellspacing="6" cellpadding="6" border="0">\n                    <tbody><tr>\n                        <td width="405">\n                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>\n                            \n							\n				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">Thank you for registering your service request with us today. We will arrange for your product to be appraised by our senior technician who will decide upon the most suitable course of action to satisfy your particular requirement and who will notify you accordingly.</p>\n                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'" >Although, we will communicate regularly confirming each stage of your service lifecycle you can check progress anytime at <a style="font-family:\'Calibri\'" href="[CUSTOMER_WEBSITE_URL]" target="_blank" >[CUSTOMER_WEBSITE_NAME]</a></p>\n                                \n\n                        </td>\n                        <td width="175">\n						                        \n                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />\n                       \n					    </td>\n                    </tr>\n                   \n                </tbody></table>\n                <table>\n                <tbody>\n                 <tr><td width="580" height="5"></td></tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                    <tr><td width="580" height="5"></td></tr>\n                    </tbody>\n                    </table>\n                                        \n                <!-- #TEXT WITH ADDRESS\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="2" border="0">\n                    <tbody>\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;"  >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="150" colspan="2" valign="bottom">ADDRESS DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;"  >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_TITLE_FIRST_LAST_NAME], <br>[CUSTOMER_ADDRESS]</p>\n                        </td>\n                    </tr>\n					\n					\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                           <table cellspacing="0" cellpadding="5" border="0">\n                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">CONTACT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_PHONE_NO][CUSTOMER_MOBILE_NO][CUSTOMER_EMAIL_PREFIX]<a href="mailto:[CUSTOMER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[CUSTOMER_EMAIL]</a></p>\n                        </td>\n                    </tr>\n					\n                    <tr>\n                        <td width="186" style="padding-bottom:25px;" >\n                          <table cellspacing="0" cellpadding="5" border="0">\n						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>\n                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>\n                          </table>\n                       </td>\n                        <td width="394" style="padding-bottom:25px;" >\n                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Job Number: [SC_JOB_NUMBER]&nbsp;&nbsp;&nbsp;&nbsp;Job Booked: [BOOKED_DATE] <br>Manufacturer: [MANUFACTURER_NAME]&nbsp;&nbsp;&nbsp;&nbsp;<br>Product: [UNIT_TYPE_NAME]&nbsp;&nbsp;&nbsp;&nbsp; <br>Model No: [MODEL_NO] <br>Service Type: [SERVICE_TYPE]&nbsp;&nbsp;&nbsp;&nbsp;Location: [PRODUCT_LOCATION]</p>\n                        </td>\n                    </tr>\n			    </tbody></table>\n                                        \n           \n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>\n                </tbody></table>\n                                        \n                <!-- #TEXT FOOTER\n                ================================================== -->\n                <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        \n                    </tr>\n                    <tr><td width="580" height="100">\n                    \n\n<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:\'Calibri\'" >Please email <a style="font-family:\'Calibri\'" href="mailto:[BRANCH_EMAIL]" >[BRANCH_EMAIL]</a> immediately if there are any discrepancies within the information \n<br><br></p>\n<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'" >Assuring you of our best intention at all times.</p>\n\n<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'" >Yours Sincerely, [BRANCH_NAME]</p>\n                    </td>\n                    </tr>\n                </tbody></table>\n                                                                                \n                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY\n                ================================================== -->\n               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td valign="top">\n                          \n                        </td>\n                       \n                    </tr>\n                </tbody></table>-->\n                                        \n                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE\n                ================================================== -->\n                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">\n                    <tbody><tr>\n                        <td width="180" valign="top">\n                          \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                       \n                        </td>\n                        <td width="20"></td>\n                        <td width="180" valign="top">\n                            \n                        </td>\n                    </tr>\n                </tbody></table>-->\n            </td>\n            <td width="30"></td>\n        </tr>\n    </tbody></table>\n</td>\n                </tr>\n                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->\n                <!-- #FOOTER\n   				 ================================================== -->\n                <tr>\n                <td width="640">\n    <table width="640" cellspacing="0" cellpadding="0" border="0">\n        <tbody><tr>\n        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">\n <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'">If you have any queries contact us on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[BRANCH_EMAIL]">[BRANCH_EMAIL]</a></p>\n            </td>\n            </tr>\n\n    </tbody></table>\n</td>\n                </tr>\n               \n            </tbody></table>\n        </td>\n	</tr>\n</tbody></table>\n</body>\n</html>\n', '2013-04-04 13:22:26', 1);
/*!40000 ALTER TABLE `email` ENABLE KEYS */;


-- Dumping structure for table create_new.email_job
CREATE TABLE IF NOT EXISTS `email_job` (
  `EmailJobID` int(11) NOT NULL AUTO_INCREMENT,
  `MailBody` text,
  `MailAccessCode` varchar(32) DEFAULT NULL,
  `JobID` int(11) DEFAULT NULL,
  `EmailID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ModifiedUserID` int(11) DEFAULT NULL,
  PRIMARY KEY (`EmailJobID`),
  KEY `IDX_email_job_JobID_FK` (`JobID`),
  KEY `IDX_email_job_EmailID_FK` (`EmailID`),
  KEY `IDX_email_job_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `email_TO_email_job` FOREIGN KEY (`EmailID`) REFERENCES `email` (`EmailID`),
  CONSTRAINT `job_TO_email_job` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`),
  CONSTRAINT `user_TO_email_job` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.email_job: ~0 rows (approximately)
/*!40000 ALTER TABLE `email_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_job` ENABLE KEYS */;


-- Dumping structure for table create_new.extended_warrantor
CREATE TABLE IF NOT EXISTS `extended_warrantor` (
  `ExtendedWarrantorID` int(11) NOT NULL AUTO_INCREMENT,
  `ExtendedWarrantorName` varchar(64) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ExtendedWarrantorID`),
  KEY `IDX_extended_warrantor_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `user_TO_extended_warrantor` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.extended_warrantor: ~0 rows (approximately)
/*!40000 ALTER TABLE `extended_warrantor` DISABLE KEYS */;
/*!40000 ALTER TABLE `extended_warrantor` ENABLE KEYS */;


-- Dumping structure for table create_new.general_default
CREATE TABLE IF NOT EXISTS `general_default` (
  `GeneralDefaultID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `IDNo` int(11) NOT NULL,
  `Category` varchar(20) NOT NULL,
  `DefaultName` varchar(40) NOT NULL,
  `Default` varchar(20) NOT NULL,
  `Description` text NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`GeneralDefaultID`),
  KEY `IDX_general_default_BrandID_FK` (`BrandID`),
  KEY `IDX_general_default_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `brand_TO_general_default` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `user_TO_general_default` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.general_default: ~0 rows (approximately)
/*!40000 ALTER TABLE `general_default` DISABLE KEYS */;
/*!40000 ALTER TABLE `general_default` ENABLE KEYS */;


-- Dumping structure for table create_new.help_text
CREATE TABLE IF NOT EXISTS `help_text` (
  `HelpTextID` int(11) NOT NULL AUTO_INCREMENT,
  `HelpTextCode` varchar(64) NOT NULL,
  `HelpTextTitle` varchar(255) DEFAULT NULL,
  `HelpText` text,
  `CreatedDate` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`HelpTextID`),
  UNIQUE KEY `HelpTextCode` (`HelpTextCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table contains help text for each element.';

-- Dumping data for table create_new.help_text: ~0 rows (approximately)
/*!40000 ALTER TABLE `help_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `help_text` ENABLE KEYS */;


-- Dumping structure for table create_new.job
CREATE TABLE IF NOT EXISTS `job` (
  `JobID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  `BrandID` int(11) DEFAULT NULL,
  `BranchID` int(11) DEFAULT NULL,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `ProductID` int(11) DEFAULT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `ServiceTypeID` int(11) DEFAULT NULL,
  `JobTypeID` int(11) NOT NULL,
  `StatusID` int(11) DEFAULT NULL,
  `OpenJobStatus` enum('in_store','with_supplier','awaiting_collection','customer_notified','closed') DEFAULT NULL,
  `NetworkRefNo` varchar(20) DEFAULT NULL,
  `AgentRefNo` varchar(20) DEFAULT NULL,
  `ServiceCentreJobNo` int(11) DEFAULT NULL,
  `JobSite` enum('field call','workshop') DEFAULT NULL,
  `ServiceBaseManufacturer` varchar(30) DEFAULT NULL,
  `ServiceBaseModel` varchar(24) DEFAULT NULL,
  `ServiceBaseUnitType` varchar(30) DEFAULT NULL,
  `SerialNo` varchar(24) DEFAULT NULL,
  `VestelModelCode` varchar(30) DEFAULT NULL,
  `VestelManufactureDate` char(4) DEFAULT NULL,
  `DateOfPurchase` date DEFAULT NULL,
  `GuaranteeCode` char(4) DEFAULT NULL,
  `CustomerType` char(1) DEFAULT NULL,
  `ConditionCode` char(4) DEFAULT NULL,
  `SymptomCode` char(4) DEFAULT NULL,
  `DefectType` char(4) DEFAULT NULL,
  `ReportedFault` varchar(2000) DEFAULT NULL,
  `RepairType` enum('customer','installation','stock') DEFAULT NULL,
  `Notes` text,
  `FaultOccurredDate` date DEFAULT NULL,
  `FaultOccurredTime` time DEFAULT NULL,
  `RepairCompleteDate` date DEFAULT NULL,
  `DateReturnedToCustomer` timestamp NULL DEFAULT NULL,
  `ServiceProviderDespatchDate` timestamp NULL DEFAULT NULL,
  `DownloadedToSC` tinyint(1) DEFAULT NULL,
  `ItemLocation` varchar(40) DEFAULT NULL,
  `RepairDescription` text,
  `CompletionStatus` varchar(40) DEFAULT NULL,
  `OriginalRetailer` varchar(75) DEFAULT NULL,
  `RetailerLocation` varchar(100) DEFAULT NULL,
  `DateOfManufacture` date DEFAULT NULL,
  `PolicyNo` varchar(20) DEFAULT NULL,
  `ETDDate` date DEFAULT NULL,
  `ETDTime` time DEFAULT NULL,
  `Accessories` varchar(200) DEFAULT NULL,
  `UnitCondition` varchar(150) DEFAULT NULL,
  `Insurer` varchar(30) DEFAULT NULL,
  `RepairAuthStatus` enum('required','not_required','confirmed','rejected') DEFAULT NULL,
  `AuthorisationNo` varchar(20) DEFAULT NULL,
  `DateBooked` date DEFAULT NULL,
  `TimeBooked` time DEFAULT NULL,
  `DateUnitReceived` date DEFAULT NULL,
  `ClosedDate` date DEFAULT NULL,
  `Status` varchar(40) DEFAULT NULL,
  `CancelReason` text,
  `EngineerCode` varchar(20) DEFAULT NULL,
  `EngineerName` varchar(50) DEFAULT NULL,
  `ClaimNumber` varchar(20) DEFAULT NULL,
  `ClaimTransmitStatus` int(11) DEFAULT NULL,
  `eInvoiceStatus` int(11) DEFAULT NULL,
  `ChargeableLabourCost` decimal(10,2) DEFAULT '0.00',
  `ChargeableLabourVATCost` decimal(10,2) DEFAULT '0.00',
  `ChargeablePartsCost` decimal(10,2) DEFAULT '0.00',
  `ChargeableDeliveryCost` decimal(10,2) DEFAULT '0.00',
  `ChargeableDeliveryVATCost` decimal(10,2) DEFAULT '0.00',
  `ChargeableVATCost` decimal(10,2) DEFAULT '0.00',
  `ChargeableSubTotal` decimal(10,2) DEFAULT '0.00',
  `ChargeableTotalCost` decimal(10,2) DEFAULT '0.00',
  `AgentChargeableInvoiceNo` int(11) DEFAULT '0',
  `AgentChargeableInvoiceDate` date DEFAULT NULL,
  `ProductLocation` enum('Customer','Branch','Service Provider','Supplier','other') DEFAULT NULL,
  `ModelID` int(11) DEFAULT NULL,
  `BookedBy` int(11) NOT NULL,
  `ModifiedUserID` int(11) NOT NULL,
  `ModifiedDate` datetime NOT NULL,
  `ContractTracking` varchar(4) DEFAULT NULL,
  `SamsungWarrantyParts` varchar(1) DEFAULT NULL,
  `SamsungWarrantyLabour` varchar(1) DEFAULT NULL,
  `RMANumber` int(11) DEFAULT NULL,
  `AgentStatus` varchar(30) DEFAULT NULL,
  `StockCode` varchar(64) DEFAULT NULL,
  `EngineerAssignedDate` date DEFAULT NULL,
  `EngineerAssignedTime` date DEFAULT NULL,
  `WarrantyNotes` varchar(50) DEFAULT NULL,
  `NewFirmwareVersion` varchar(20) DEFAULT NULL,
  `ClaimBillNumber` varchar(15) DEFAULT NULL,
  `EstimateStatus` enum('Awaiting Confiirmation','Accepted','Rejected') DEFAULT NULL,
  `ReceiptNo` varchar(40) DEFAULT NULL,
  `ColAddCompanyName` varchar(100) DEFAULT NULL,
  `ColAddBuildingNameNumber` varchar(40) DEFAULT NULL,
  `ColAddStreet` varchar(40) DEFAULT NULL,
  `ColAddLocalArea` varchar(40) DEFAULT NULL,
  `ColAddTownCity` varchar(60) DEFAULT NULL,
  `ColAddCountyID` int(11) DEFAULT NULL,
  `ColAddCountryID` int(11) DEFAULT NULL,
  `ColAddPostcode` varchar(10) DEFAULT NULL,
  `ColAddEmail` varchar(40) DEFAULT NULL,
  `ColAddPhone` varchar(40) DEFAULT NULL,
  `ColAddPhoneExt` varchar(10) DEFAULT NULL,
  `ExtendedWarrantyNo` varchar(40) DEFAULT NULL,
  `JobSourceID` int(11) DEFAULT NULL,
  `JobRating` int(2) DEFAULT NULL,
  `batchID` int(11) DEFAULT NULL,
  `RAType` enum('repair','return','estimate','invoice','on hold') DEFAULT NULL,
  `RAStatusID` int(11) DEFAULT NULL,
  `CollectionDate` date DEFAULT NULL,
  `CourierID` int(11) DEFAULT NULL,
  `SamsungSubServiceType` varchar(2) DEFAULT NULL,
  `SamsungRefNo` varchar(15) DEFAULT NULL,
  `ConsumerType` varchar(30) DEFAULT NULL,
  `ServiceAction` varchar(250) DEFAULT NULL,
  `ImeiNo` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`JobID`),
  KEY `IDX_job_1` (`GuaranteeCode`),
  KEY `IDX_job_2` (`DateBooked`),
  KEY `IDX_job_3` (`ClosedDate`),
  KEY `IDX_job_4` (`JobSourceID`),
  KEY `IDX_job_5` (`NetworkRefNo`),
  KEY `IDX_job_6` (`ServiceCentreJobNo`),
  KEY `IDX_job_7` (`OpenJobStatus`),
  KEY `IDX_job_NetworkID_FK` (`NetworkID`),
  KEY `IDX_job_BranchID_FK` (`BranchID`),
  KEY `IDX_job_ClientID_FK` (`ClientID`),
  KEY `IDX_job_ServiceProviderID_FK` (`ServiceProviderID`),
  KEY `IDX_job_CustomerID_FK` (`CustomerID`),
  KEY `IDX_job_ProductID_FK` (`ProductID`),
  KEY `IDX_job_ServiceTypeID_FK` (`ServiceTypeID`),
  KEY `IDX_job_ManufacturerID_FK` (`ManufacturerID`),
  KEY `IDX_job_JobTypeID_FK` (`JobTypeID`),
  KEY `IDX_job_StatusID_FK` (`StatusID`),
  KEY `IDX_job_ModelID_FK` (`ModelID`),
  KEY `IDX_job_BookedBy_FK` (`BookedBy`),
  KEY `IDX_job_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_job_ColAddCountyID_FK` (`ColAddCountyID`),
  KEY `IDX_job_ColAddCountryID_FK` (`ColAddCountryID`),
  KEY `IDX_job_RAStatusID_FK` (`RAStatusID`),
  KEY `IDX_job_SerialNo` (`SerialNo`),
  KEY `BrandID` (`BrandID`),
  KEY `IDX_ServiceBaseUnitType` (`ServiceBaseUnitType`),
  CONSTRAINT `branch_TO_job` FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`),
  CONSTRAINT `client_TO_job` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `country_TO_job` FOREIGN KEY (`ColAddCountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `county_TO_job` FOREIGN KEY (`ColAddCountyID`) REFERENCES `county` (`CountyID`),
  CONSTRAINT `customer_TO_job` FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`),
  CONSTRAINT `job_type_TO_job` FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`),
  CONSTRAINT `manufacturer_TO_job` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `model_TO_job` FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`),
  CONSTRAINT `network_TO_job` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `product_TO_job` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`),
  CONSTRAINT `ra_status_TO_job` FOREIGN KEY (`RAStatusID`) REFERENCES `ra_status` (`RAStatusID`),
  CONSTRAINT `service_provider_TO_job` FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`),
  CONSTRAINT `service_type_TO_job` FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`),
  CONSTRAINT `status_TO_job` FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`),
  CONSTRAINT `user_TO_job` FOREIGN KEY (`BookedBy`) REFERENCES `user` (`UserID`),
  CONSTRAINT `user_TO_job_ModifiedUser` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.job: ~0 rows (approximately)
/*!40000 ALTER TABLE `job` DISABLE KEYS */;
/*!40000 ALTER TABLE `job` ENABLE KEYS */;


-- Dumping structure for table create_new.job_accessory
CREATE TABLE IF NOT EXISTS `job_accessory` (
  `JobID` int(11) NOT NULL,
  `AccessoryID` int(11) NOT NULL,
  UNIQUE KEY `JobID` (`JobID`,`AccessoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.job_accessory: ~0 rows (approximately)
/*!40000 ALTER TABLE `job_accessory` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_accessory` ENABLE KEYS */;


-- Dumping structure for table create_new.job_import_batch_data
CREATE TABLE IF NOT EXISTS `job_import_batch_data` (
  `batchID` int(11) NOT NULL AUTO_INCREMENT,
  `batchNumber` int(11) NOT NULL,
  `clientID` int(11) NOT NULL,
  `batchRowCount` int(11) NOT NULL,
  `batchDescription` varchar(250) NOT NULL,
  `batchState` enum('created','importing','finished') NOT NULL,
  `batchHash` varchar(40) NOT NULL,
  `batchTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`batchID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.job_import_batch_data: ~0 rows (approximately)
/*!40000 ALTER TABLE `job_import_batch_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_import_batch_data` ENABLE KEYS */;


-- Dumping structure for table create_new.job_source
CREATE TABLE IF NOT EXISTS `job_source` (
  `JobSourceID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`JobSourceID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.job_source: ~0 rows (approximately)
/*!40000 ALTER TABLE `job_source` DISABLE KEYS */;
INSERT IGNORE INTO `job_source` (`JobSourceID`, `Description`) VALUES
	(1, 'Skyline'),
	(2, 'Micro Sites'),
	(3, 'API'),
	(4, 'Unknown'),
	(5, 'Import'),
	(6, 'ServiceBase'),
	(7, 'RMA Tracker'),
	(8, 'Fixzone'),
	(9, 'Samsung'),
	(10, 'Panasonic');
/*!40000 ALTER TABLE `job_source` ENABLE KEYS */;


-- Dumping structure for table create_new.job_type
CREATE TABLE IF NOT EXISTS `job_type` (
  `JobTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `Type` varchar(30) NOT NULL,
  `TypeCode` int(11) NOT NULL,
  `Priority` int(11) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`JobTypeID`),
  KEY `IDX_job_type_1` (`Type`),
  KEY `IDX_job_type_BrandID_FK` (`BrandID`),
  KEY `IDX_job_type_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `brand_TO_job_type` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `user_TO_job_type` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table contains Job Type details.';

-- Dumping data for table create_new.job_type: ~0 rows (approximately)
/*!40000 ALTER TABLE `job_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_type` ENABLE KEYS */;


-- Dumping structure for table create_new.manufacturer
CREATE TABLE IF NOT EXISTS `manufacturer` (
  `ManufacturerID` int(11) NOT NULL AUTO_INCREMENT,
  `ManufacturerName` varchar(30) DEFAULT NULL,
  `Acronym` varchar(30) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `BuildingNameNumber` varchar(50) DEFAULT NULL,
  `Street` varchar(100) DEFAULT NULL,
  `LocalArea` varchar(100) DEFAULT NULL,
  `TownCity` varchar(100) DEFAULT NULL,
  `CountryID` int(3) DEFAULT NULL,
  `AuthorisationRequired` enum('Yes','No') DEFAULT 'No',
  `AuthReqChangedDate` timestamp NULL DEFAULT NULL,
  `AuthorisationManager` varchar(100) DEFAULT NULL,
  `AuthorisationManagerEmail` varchar(100) DEFAULT NULL,
  `ManufacturerLogo` varchar(40) DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `AccountNo` varchar(20) DEFAULT NULL,
  `AddressLine1` varchar(60) DEFAULT NULL,
  `AddressLine2` varchar(60) DEFAULT NULL,
  `AddressLine3` varchar(60) DEFAULT NULL,
  `Postcode` varchar(10) DEFAULT NULL,
  `TelNoSwitchboard` varchar(20) DEFAULT NULL,
  `TelNoSpares` varchar(20) DEFAULT NULL,
  `TelNoWarranty` varchar(20) DEFAULT NULL,
  `TelNoTechnical` varchar(20) DEFAULT NULL,
  `FaxNo` varchar(20) DEFAULT NULL,
  `EmailAddress` varchar(255) DEFAULT NULL,
  `WarrantyEmailAddress` varchar(255) DEFAULT NULL,
  `SparesEmailAddress` varchar(255) DEFAULT NULL,
  `ContactName` varchar(30) DEFAULT NULL,
  `PrimarySupplierID` int(11) DEFAULT NULL,
  `StartClaimNo` varchar(50) DEFAULT NULL,
  `VATNo` varchar(20) DEFAULT NULL,
  `IRISManufacturer` enum('Yes','No') DEFAULT NULL,
  `FaultCodesReq` enum('Yes','No') DEFAULT NULL,
  `ExtendedGuaranteeNoReq` enum('Yes','No') DEFAULT NULL,
  `PolicyNoReq` enum('Yes','No') DEFAULT NULL,
  `InvoiceNoReq` enum('Yes','No') DEFAULT NULL,
  `CircuitRefReq` enum('Yes','No') DEFAULT NULL,
  `PartNoReq` enum('Yes','No') DEFAULT NULL,
  `OriginalRetailerReq` enum('Yes','No') DEFAULT NULL,
  `ReferralNoReq` enum('Yes','No') DEFAULT NULL,
  `SerialNoReq` enum('Yes','No') DEFAULT NULL,
  `FirmwareReq` enum('Yes','No') DEFAULT NULL,
  `WarrantyLabourRateReqOn` enum('Yes','No') DEFAULT NULL,
  `SageAccountNo` varchar(20) DEFAULT NULL,
  `NoWarrantyInvoices` enum('Yes','No') DEFAULT NULL,
  `DisplaySectionCodes` enum('Yes','No') DEFAULT NULL,
  `DisplayTwoConditionCodes` enum('Yes','No') DEFAULT NULL,
  `DisplayJobPriceStructure` enum('Yes','No') DEFAULT NULL,
  `DisplayTelNos` enum('Yes','No') DEFAULT NULL,
  `DisplayPartCost` enum('Yes','No') DEFAULT NULL,
  `DisplayRepairTime` enum('Yes','No') DEFAULT NULL,
  `DisplayAuthNo` enum('Yes','No') DEFAULT NULL,
  `DisplayOtherCosts` enum('Yes','No') DEFAULT NULL,
  `DisplayAdditionalFreight` enum('Yes','No') DEFAULT NULL,
  `NoOfClaimPrints` int(11) DEFAULT NULL,
  `EDIClaimNo` int(11) DEFAULT NULL,
  `EDIClaimFileName` varchar(50) DEFAULT NULL,
  `UseProductCode` enum('Yes','No') DEFAULT NULL,
  `UseMSNNo` enum('Yes','No') DEFAULT NULL,
  `UseMSNFormat` enum('Yes','No') DEFAULT NULL,
  `MSNFormat` varchar(20) DEFAULT NULL,
  `WarrantyPeriodFromDOP` int(11) DEFAULT NULL,
  `ValidateDateCode` enum('Yes','No') DEFAULT NULL,
  `ForceStatusID` int(11) DEFAULT NULL,
  `POPPeriod` int(11) DEFAULT NULL,
  `ClaimPeriod` int(11) DEFAULT NULL,
  `UseQA` enum('Yes','No') DEFAULT NULL,
  `UsePreQA` enum('Yes','No') DEFAULT NULL,
  `QAExchangeUnits` enum('Yes','No') DEFAULT NULL,
  `QALoanUnits` enum('Yes','No') DEFAULT NULL,
  `CompleteOnQAPass` enum('Yes','No') DEFAULT NULL,
  `ValidateNetworkQA` enum('Yes','No') DEFAULT NULL,
  `ExcludeFromBouncerTable` enum('Yes','No') DEFAULT NULL,
  `BouncerPeriod` int(11) DEFAULT NULL,
  `BouncerPeriodBasedOn` enum('BookedDate','CompletedDate','DespatchedDate') DEFAULT NULL,
  `EDIExportFormatID` int(11) DEFAULT NULL,
  `CreateTechnicalReport` enum('Yes','No') DEFAULT NULL,
  `CreateServiceHistoryReport` enum('Yes','No') DEFAULT NULL,
  `IncludeOutOfWarrantyJobs` enum('Yes','No') DEFAULT NULL,
  `WarrantyAccountNo` varchar(20) DEFAULT NULL,
  `TechnicalReportIncludeAdj` enum('Yes','No') DEFAULT NULL,
  `PartNoReqForWarrantyAdj` enum('Yes','No') DEFAULT NULL,
  `ForceAdjOnNoParts` enum('Yes','No') DEFAULT NULL,
  `ForceKeyRepairPart` enum('Yes','No') DEFAULT NULL,
  `ForceOutOfWarrantyFaultCodes` enum('Yes','No') DEFAULT NULL,
  `WarrantyExchangeFree` decimal(10,2) DEFAULT NULL,
  `UseFaultCodesForOBFJobs` enum('Yes','No') DEFAULT NULL,
  `AutoFinalRejectClaimsNotResubmitted` enum('Yes','No') DEFAULT NULL,
  `DaysToResubmit` int(11) DEFAULT NULL,
  `LimitResubmissions` enum('Yes','No') DEFAULT NULL,
  `LimitResubmissionsTo` int(11) DEFAULT NULL,
  `CopyDOPFromBouncerJob` enum('Yes','No') DEFAULT NULL,
  `JobFaultCodeID` int(11) DEFAULT NULL,
  `PartFaultCodeID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ManufacturerID`),
  UNIQUE KEY `IDX_manufacturer_1` (`ManufacturerName`),
  KEY `IDX_manufacturer_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `user_TO_manufacturer` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.manufacturer: ~0 rows (approximately)
/*!40000 ALTER TABLE `manufacturer` DISABLE KEYS */;
/*!40000 ALTER TABLE `manufacturer` ENABLE KEYS */;


-- Dumping structure for table create_new.manufacturer_service_provider
CREATE TABLE IF NOT EXISTS `manufacturer_service_provider` (
  `ManufacturerID` int(10) NOT NULL,
  `ServiceProviderID` int(10) NOT NULL,
  `Authorised` enum('Yes','No') DEFAULT 'Yes',
  `DataChecked` enum('Yes','No') DEFAULT 'No',
  PRIMARY KEY (`ManufacturerID`,`ServiceProviderID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.manufacturer_service_provider: ~0 rows (approximately)
/*!40000 ALTER TABLE `manufacturer_service_provider` DISABLE KEYS */;
/*!40000 ALTER TABLE `manufacturer_service_provider` ENABLE KEYS */;


-- Dumping structure for table create_new.model
CREATE TABLE IF NOT EXISTS `model` (
  `ModelID` int(11) NOT NULL AUTO_INCREMENT,
  `ManufacturerID` int(11) DEFAULT NULL,
  `UnitTypeID` int(11) DEFAULT NULL,
  `ModelNumber` varchar(30) DEFAULT NULL,
  `ModelName` varchar(24) DEFAULT NULL,
  `ModelDescription` text,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Features` varchar(50) DEFAULT NULL,
  `IMEILengthFrom` int(11) DEFAULT NULL,
  `IMEILengthTo` int(11) DEFAULT NULL,
  `MSNLengthFrom` int(11) DEFAULT NULL,
  `MSNLengthTo` int(11) DEFAULT NULL,
  `WarrantyRepairLimit` decimal(10,4) DEFAULT NULL,
  `ExcludeFromRRCRepair` enum('Yes','No') DEFAULT 'No',
  `AllowIMEIAlphaChar` enum('Yes','No') DEFAULT 'No',
  `UseReplenishmentProcess` enum('Yes','No') DEFAULT 'No',
  `HandsetWarranty1Year` enum('Yes','No') DEFAULT 'No',
  `ReplacmentValue` decimal(10,4) DEFAULT NULL,
  `ExchangeSellingPrice` decimal(10,4) DEFAULT NULL,
  `LoanSellingPrice` decimal(10,4) DEFAULT NULL,
  `RRCOrderCap` int(11) DEFAULT NULL,
  PRIMARY KEY (`ModelID`),
  UNIQUE KEY `TUC_model_1` (`ModelNumber`,`ManufacturerID`),
  KEY `IDX_model_ManufacturerID_FK` (`ManufacturerID`),
  KEY `IDX_model_UnitTypeID_FK` (`UnitTypeID`),
  KEY `IDX_model_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `manufacturer_TO_model` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `unit_type_TO_model` FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`),
  CONSTRAINT `user_TO_model` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.model: ~0 rows (approximately)
/*!40000 ALTER TABLE `model` DISABLE KEYS */;
/*!40000 ALTER TABLE `model` ENABLE KEYS */;


-- Dumping structure for table create_new.national_bank_holiday
CREATE TABLE IF NOT EXISTS `national_bank_holiday` (
  `NationalBankHolidayID` int(10) NOT NULL AUTO_INCREMENT,
  `HolidayDate` date NOT NULL,
  `HolidayName` varchar(50) NOT NULL,
  `CountryID` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`NationalBankHolidayID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.national_bank_holiday: ~8 rows (approximately)
/*!40000 ALTER TABLE `national_bank_holiday` DISABLE KEYS */;
INSERT IGNORE INTO `national_bank_holiday` (`NationalBankHolidayID`, `HolidayDate`, `HolidayName`, `CountryID`) VALUES
	(1, '2013-01-01', 'New Year\'s Day', 1),
	(2, '2013-03-29', 'Good Friday', 1),
	(3, '2013-04-01', 'Easter Monday', 1),
	(4, '2013-05-06', 'Early May bank holiday', 1),
	(5, '2013-05-27', 'Spring bank holiday', 1),
	(6, '2013-08-26', 'Summer Bank Holliday', 1),
	(7, '2013-12-25', 'Christmas Day', 1),
	(8, '2013-12-26', 'Boxing Day', 1);
/*!40000 ALTER TABLE `national_bank_holiday` ENABLE KEYS */;


-- Dumping structure for table create_new.network
CREATE TABLE IF NOT EXISTS `network` (
  `NetworkID` int(11) NOT NULL AUTO_INCREMENT,
  `DefaultClientID` int(11) DEFAULT NULL,
  `CreditPrice` decimal(10,2) DEFAULT '0.00',
  `CreditLevel` int(11) DEFAULT '0',
  `ReorderLevel` int(11) DEFAULT '0',
  `CreditLevelMailSentOn` date DEFAULT NULL,
  `InvoiceCompanyName` varchar(30) DEFAULT NULL,
  `VATRateID` int(11) DEFAULT NULL,
  `VATNumber` varchar(15) DEFAULT NULL,
  `SageReferralNominalCode` int(11) DEFAULT NULL,
  `SageLabourNominalCode` int(11) DEFAULT NULL,
  `SagePartsNominalCode` int(11) DEFAULT NULL,
  `SageCarriageNominalCode` int(11) DEFAULT NULL,
  `CompanyName` varchar(50) DEFAULT NULL,
  `PostalCode` varchar(10) DEFAULT NULL,
  `BuildingNameNumber` varchar(40) DEFAULT NULL,
  `Street` varchar(40) DEFAULT NULL,
  `LocalArea` varchar(40) DEFAULT NULL,
  `TownCity` varchar(60) DEFAULT NULL,
  `ContactPhone` varchar(40) DEFAULT NULL,
  `ContactPhoneExt` varchar(10) DEFAULT NULL,
  `CountyID` int(11) DEFAULT NULL,
  `CountryID` int(11) DEFAULT NULL,
  `ContactEmail` varchar(40) DEFAULT NULL,
  `Terms` text,
  `CustomerAssurancePolicy` text,
  `TermsConditions` text,
  `Skin` varchar(20) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ServiceManagerForename` varchar(50) DEFAULT NULL,
  `ServiceManagerSurname` varchar(50) DEFAULT NULL,
  `ServiceManagerEmail` varchar(100) DEFAULT NULL,
  `AdminSupervisorForename` varchar(50) DEFAULT NULL,
  `AdminSupervisorSurname` varchar(50) DEFAULT NULL,
  `AdminSupervisorEmail` varchar(100) DEFAULT NULL,
  `NetworkManagerForename` varchar(50) DEFAULT NULL,
  `NetworkManagerSurname` varchar(50) DEFAULT NULL,
  `NetworkManagerEmail` varchar(100) DEFAULT NULL,
  `SendASCReport` enum('Yes','No') DEFAULT NULL,
  `SendNetworkReport` enum('Yes','No') DEFAULT NULL,
  PRIMARY KEY (`NetworkID`),
  KEY `IDX_network_1` (`CompanyName`),
  KEY `IDX_network_CountryID_FK` (`CountryID`),
  KEY `IDX_network_CountyID_FK` (`CountyID`),
  KEY `IDX_network_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_network_DefaultClientID_FK` (`DefaultClientID`),
  CONSTRAINT `client_TO_network` FOREIGN KEY (`DefaultClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `country_TO_network` FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `county_TO_network` FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`),
  CONSTRAINT `user_TO_network` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.network: ~0 rows (approximately)
/*!40000 ALTER TABLE `network` DISABLE KEYS */;
/*!40000 ALTER TABLE `network` ENABLE KEYS */;


-- Dumping structure for table create_new.network_client
CREATE TABLE IF NOT EXISTS `network_client` (
  `NetworkClientID` int(11) NOT NULL AUTO_INCREMENT,
  `ClientID` int(11) NOT NULL,
  `NetworkID` int(11) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`NetworkClientID`),
  KEY `IDX_network_client_NetworkID_FK` (`NetworkID`),
  KEY `IDX_network_client_ClientID_FK` (`ClientID`),
  KEY `IDX_network_client_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `client_TO_network_client` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `network_TO_network_client` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `user_TO_network_client` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.network_client: ~0 rows (approximately)
/*!40000 ALTER TABLE `network_client` DISABLE KEYS */;
/*!40000 ALTER TABLE `network_client` ENABLE KEYS */;


-- Dumping structure for table create_new.network_manufacturer
CREATE TABLE IF NOT EXISTS `network_manufacturer` (
  `NetworkManufacturerID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) NOT NULL,
  `ManufacturerID` int(11) NOT NULL,
  `CostCodeAccountNumber` varchar(40) DEFAULT NULL,
  `AccountNumber` varchar(40) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`NetworkManufacturerID`),
  UNIQUE KEY `TUC_network_manufacturer_1` (`NetworkID`,`ManufacturerID`),
  KEY `IDX_network_manufacturer_ManufacturerID_FK` (`ManufacturerID`),
  KEY `IDX_network_manufacturer_NetworkID_FK` (`NetworkID`),
  KEY `IDX_network_manufacturer_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `manufacturer_TO_network_manufacturer` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `network_TO_network_manufacturer` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `user_TO_network_manufacturer` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.network_manufacturer: ~0 rows (approximately)
/*!40000 ALTER TABLE `network_manufacturer` DISABLE KEYS */;
/*!40000 ALTER TABLE `network_manufacturer` ENABLE KEYS */;


-- Dumping structure for table create_new.network_service_provider
CREATE TABLE IF NOT EXISTS `network_service_provider` (
  `NetworkServiceProviderID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) NOT NULL,
  `ServiceProviderID` int(11) NOT NULL,
  `AccountNo` varchar(20) DEFAULT NULL,
  `SageSalesAccountNo` varchar(40) DEFAULT NULL,
  `SagePurchaseAccountNo` varchar(40) DEFAULT NULL,
  `TrackingUsername` varchar(40) DEFAULT NULL,
  `TrackingPassword` varchar(40) DEFAULT NULL,
  `WarrantyUsername` varchar(40) DEFAULT NULL,
  `WarrantyPassword` varchar(40) DEFAULT NULL,
  `WarrantyAccountNumber` varchar(40) DEFAULT NULL,
  `Area` varchar(40) DEFAULT NULL,
  `WalkinJobsUpload` tinyint(1) DEFAULT NULL,
  `CompanyCode` varchar(255) DEFAULT NULL,
  `SamsungDownloadEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`NetworkServiceProviderID`),
  UNIQUE KEY `TUC_network_service_provider_1` (`NetworkID`,`ServiceProviderID`),
  KEY `IDX_network_service_provider_NetworkID_FK` (`NetworkID`),
  KEY `IDX_network_service_provider_ServiceProviderID_FK` (`ServiceProviderID`),
  KEY `IDX_network_service_provider_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `network_TO_network_service_provider` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `service_provider_TO_network_service_provider` FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`),
  CONSTRAINT `user_TO_network_service_provider` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.network_service_provider: ~0 rows (approximately)
/*!40000 ALTER TABLE `network_service_provider` DISABLE KEYS */;
/*!40000 ALTER TABLE `network_service_provider` ENABLE KEYS */;


-- Dumping structure for table create_new.non_skyline_job
CREATE TABLE IF NOT EXISTS `non_skyline_job` (
  `NonSkylineJobID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) DEFAULT NULL,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `AppointmentID` int(11) DEFAULT NULL,
  `ServiceProviderJobNo` int(11) DEFAULT NULL,
  `NetworkRefNo` varchar(22) DEFAULT NULL,
  `CustHomeTelNo` varchar(50) DEFAULT NULL,
  `CustWorkTelNo` varchar(50) DEFAULT NULL,
  `CustMobileNo` varchar(50) DEFAULT NULL,
  `CollContactNumber` varchar(50) DEFAULT NULL,
  `CustomerSurname` varchar(40) DEFAULT NULL,
  `CustomerTitle` varchar(10) DEFAULT NULL,
  `Postcode` varchar(8) DEFAULT NULL,
  `JobType` varchar(50) DEFAULT NULL,
  `JobSourceID` int(11) DEFAULT NULL,
  `RepairSkillID` int(11) DEFAULT NULL,
  `JobTypeID` int(11) DEFAULT NULL,
  `ServiceType` varchar(50) DEFAULT NULL,
  `CustomerAddress1` varchar(50) DEFAULT NULL,
  `CustomerAddress2` varchar(50) DEFAULT NULL,
  `CustomerAddress3` varchar(50) DEFAULT NULL,
  `CustomerAddress4` varchar(50) DEFAULT NULL,
  `ProductType` varchar(50) DEFAULT NULL,
  `Manufacturer` varchar(50) DEFAULT NULL,
  `ModelNumber` varchar(50) DEFAULT NULL,
  `Client` varchar(50) DEFAULT NULL,
  `ReportedFault` varchar(2000) DEFAULT NULL,
  `CustomerContactNumber` varchar(50) DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`NonSkylineJobID`),
  KEY `IDX_non_skyline_job_1` (`ServiceProviderID`),
  KEY `IDX_non_skyline_job_2` (`AppointmentID`),
  KEY `IDX_non_skyline_job_3` (`ServiceProviderJobNo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.non_skyline_job: ~0 rows (approximately)
/*!40000 ALTER TABLE `non_skyline_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `non_skyline_job` ENABLE KEYS */;


-- Dumping structure for table create_new.part
CREATE TABLE IF NOT EXISTS `part` (
  `PartID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) DEFAULT NULL,
  `PartNo` varchar(25) DEFAULT NULL,
  `PartDescription` varchar(30) DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `Supplier` varchar(100) DEFAULT NULL,
  `SectionCode` char(4) DEFAULT NULL,
  `DefectCode` char(4) DEFAULT NULL,
  `RepairCode` char(4) DEFAULT NULL,
  `IsMajorFault` tinyint(1) DEFAULT NULL,
  `CircuitReference` varchar(10) DEFAULT NULL,
  `PartSerialNo` varchar(50) DEFAULT NULL,
  `OrderNo` int(11) DEFAULT NULL,
  `OrderStatus` varchar(30) DEFAULT NULL,
  `OrderDate` date DEFAULT NULL,
  `ReceivedDate` date DEFAULT NULL,
  `InvoiceNo` varchar(30) DEFAULT NULL,
  `UnitCost` decimal(10,2) DEFAULT '0.00',
  `SaleCost` decimal(10,2) DEFAULT '0.00',
  `VATRate` decimal(4,2) DEFAULT NULL,
  `SBPartID` int(11) DEFAULT NULL,
  `PartStatus` varchar(30) DEFAULT NULL,
  `OldPartSerialNo` varchar(20) DEFAULT NULL,
  `IsOtherCost` char(1) DEFAULT NULL,
  `IsAdjustment` char(1) DEFAULT NULL,
  `DueDate` date DEFAULT NULL,
  `SupplierOrderNo` varchar(10) DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  PRIMARY KEY (`PartID`),
  KEY `IDX_part_JobID_FK` (`JobID`),
  CONSTRAINT `job_TO_part` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.part: ~0 rows (approximately)
/*!40000 ALTER TABLE `part` DISABLE KEYS */;
/*!40000 ALTER TABLE `part` ENABLE KEYS */;


-- Dumping structure for table create_new.payment_type
CREATE TABLE IF NOT EXISTS `payment_type` (
  `PaymentTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `Type` varchar(255) NOT NULL,
  `TypeCode` int(11) DEFAULT NULL,
  `SystemCode` varchar(255) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PaymentTypeID`),
  KEY `IDX_payment_type_BrandID_FK` (`BrandID`),
  KEY `IDX_payment_type_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `brand_TO_payment_type` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `user_TO_payment_type` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table contains Payment Type details.';

-- Dumping data for table create_new.payment_type: ~0 rows (approximately)
/*!40000 ALTER TABLE `payment_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_type` ENABLE KEYS */;


-- Dumping structure for table create_new.permission
CREATE TABLE IF NOT EXISTS `permission` (
  `PermissionID` int(11) NOT NULL,
  `Name` varchar(40) DEFAULT NULL,
  `Description` text,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `URLSegment` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PermissionID`),
  KEY `IDX_permission_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `user_TO_permission` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.permission: ~1 rows (approximately)
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT IGNORE INTO `permission` (`PermissionID`, `Name`, `Description`, `CreatedDate`, `EndDate`, `Status`, `ModifiedUserID`, `ModifiedDate`, `URLSegment`) VALUES
	(7000, 'tes', 'tes', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-02-06 09:51:30', NULL),
	(7001, 'Network Level Access Permison', 'Allow access to the network features', '2012-09-14 13:48:41', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:13', ''),
	(7002, 'Client Level Access Permission.', 'Allow the user to access client features', '2012-09-14 13:48:41', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:13', ''),
	(7003, 'Display Job Search Box', 'Show the job search box on home page.', '2012-09-14 13:48:41', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:13', ''),
	(7004, 'Display Open and Overdue Jobs on Home Pa', 'Show the job search box on home page.', '2012-09-14 13:48:41', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:13', ''),
	(7005, 'Job Allocation Admin Section', 'Job Allocation Admin Section', '2012-09-14 13:48:41', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:13', ''),
	(7006, 'Service Provider Change Link', 'Service Provider Change/Default Link on Job Booking process.', '2012-09-14 13:48:41', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:13', ''),
	(7007, 'Insert Branch when booking job', 'Insert Branch when booking job', '2012-09-14 13:48:41', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:13', ''),
	(7008, 'Display Menu Item - Open Jobs', 'Display Menu Item - Open Jobs', '2012-09-18 10:04:01', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:13', 'openJobs'),
	(7009, 'Display Menu Item - Overdue Jobs', 'Display Menu Item - Overdue Jobs', '2012-09-18 10:04:01', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:14', 'overdueJobs'),
	(7010, 'Display Menu Item - Performance', 'Display Menu Item - Performance', '2012-09-18 10:04:01', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:14', 'performance'),
	(7011, 'Display Menu Item - Site Map', 'Display Menu Item - Site Map', '2012-09-18 10:04:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:14', 'siteMap'),
	(7012, 'Display Menu Item - Add Contact Note', 'Display Menu Item - Add Contact Note', '2012-09-18 10:04:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:14', ''),
	(7013, 'Display Menu Item - Send Email', 'Display Menu Item - Send Email', '2012-09-18 10:04:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:14', 'sendemail'),
	(7014, 'Display Menu Item - Send SMS', 'Display Menu Item - Send SMS', '2012-09-18 10:04:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:14', 'sendsms'),
	(7015, 'Display Menu Item - Cancel Jobs', 'Display Menu Item - Cancel Jobs', '2012-09-18 10:04:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:15', 'canceljob'),
	(7016, 'Display Menu Item - Other Actions', 'Display Menu Item - Other Actions', '2012-09-18 10:04:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:15', 'otheractions'),
	(7017, 'Display Control Item - Wild Card Search', 'Display Control Item - Wild Card Search', '2012-09-18 10:04:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:15', ''),
	(7018, 'Display Control Item - Advanced Job Sear', 'Display Control Item - Advanced Job Search', '2012-09-18 10:04:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:15', ''),
	(7019, 'Display Control Item - Advanced Catalogu', 'Display Control Item - Advanced Catalogue', '2012-09-18 10:04:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:15', 'productAdvancedSearch'),
	(7020, 'Display Language Control - English', 'Display Language Control - English', '2012-09-18 10:04:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:15', ''),
	(7021, 'Display Language Control - German', 'Display Language Control - German', '2012-09-18 10:04:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:15', ''),
	(7022, 'Display CSV Import button', 'Display CSV Import button', '2012-09-18 10:04:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:16', ''),
	(7023, 'Display Appointments', 'Display Appointments', '2012-09-18 10:04:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:16', ''),
	(7024, 'Display Invoice Costs', 'Display Invoice Costs', '2012-09-18 10:04:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:16', ''),
	(7025, 'Display Service Report', 'Display Service Report', '2012-09-18 10:04:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:16', ''),
	(7026, 'Display Parts Use', 'Display Parts Use', '2012-09-18 10:04:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:16', ''),
	(7027, 'Display Contact History', 'Display Contact History', '2012-09-18 10:04:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:16', ''),
	(7028, 'Display Status Changes', 'Display Status Changes', '2012-09-18 10:04:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:17', ''),
	(7029, 'RA Jobs Display Return', 'RA Jobs Display Return', '2012-10-29 14:25:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:17', ''),
	(7030, 'RA Jobs Display Repair', 'RA Jobs Display Repair', '2012-10-29 14:25:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:17', ''),
	(7031, 'RA Jobs Display On Hold', 'RA Jobs Display On Hold', '2012-10-29 14:25:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:17', ''),
	(7032, 'RA Jobs Display Estimate', 'RA Jobs Display Estimate', '2012-10-29 14:25:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:17', ''),
	(7033, 'RA Jobs Display Invoice', 'RA Jobs Display Invoice', '2012-10-29 14:25:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:17', ''),
	(7034, 'Display RA Jobs Menu Item', 'Display RA Jobs Menu Item', '2012-10-29 14:25:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:17', 'raJobs'),
	(7035, 'Display Job Update RA Drop Down', 'Display Job Update RA Drop Down', '2012-10-29 14:25:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:18', ''),
	(7036, 'Insert Repair Authorisation Number', 'Insert Repair Authorisation Number', '2012-10-29 14:25:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:18', ''),
	(7037, 'Display Policy Search Panel', 'Show the Policy Search panel on the Home Page', '2012-10-29 14:25:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:18', ''),
	(7038, 'Free Text Job Booking', 'Show Free Text Job Booking Button', '2013-01-04 14:25:02', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:18', ''),
	(7039, 'Display Menu Item - Help & Legal', 'Display Menu Item - Help & Legal', '2012-09-18 10:04:01', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:18', 'help'),
	(7040, 'Allow model update on booking', 'Allow model update on booking', '2013-01-10 12:55:01', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:18', ''),
	(7041, 'Display Booking Panel on Home Page', 'Display Booking Panel on Home Page', '2013-02-05 05:18:01', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:19', ''),
	(7042, 'Display Menu Item - Graphical Analysis', 'Display Menu Item - Graphical Analysis', '2013-02-05 05:18:01', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:19', 'graphicalAnalysis'),
	(7043, 'Site Map - Reports', 'Site Map - Reports', '2013-02-05 05:18:01', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:19', ''),
	(7044, 'Site Map - Reports - Samsung', 'Site Map - Reports - Samsung', '2013-02-05 05:18:01', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:19', 'astrReport'),
	(7045, 'Site Map - Request Authorisation', 'Site Map - Request Authorisation', '2013-02-05 05:18:01', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:19', 'raJobs'),
	(7046, 'Insert Part - Job Update Page', 'Insert Part - Job Update Page', '2013-02-05 05:18:01', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:19', ''),
	(10000, 'Arrange Collection at Booking', 'Arrange Collection at Booking', '2013-02-05 05:18:01', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:19', ''),
	(11000, 'Display Booking Panel on Home Page', 'Display Booking Panel on Home Page', '2013-02-05 05:18:01', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:19', ''),
	(11001, 'Display Menu Item - Graphical Analysis', 'Display Menu Item - Graphical Analysis', '2013-02-05 05:18:01', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:20', 'graphicalAnalysis'),
	(11002, 'Site Map - Reports', 'Site Map - Reports', '2013-02-05 05:18:01', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:20', ''),
	(11003, 'Site Map - Reports - Samsung', 'Site Map - Reports - Samsung', '2013-02-05 05:18:01', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:20', 'astrReport'),
	(11004, 'Site Map - Request Authorisation', 'Site Map - Request Authorisation', '2013-02-05 05:18:01', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:19:20', 'raJobs'),
	(11005, 'Job update appointment CRUD buttons', 'Job update appointment CRUD buttons', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:21:26', NULL),
	(11006, 'Site Map - Appointment Diary', 'Site Map - Appointment Diary', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:21:26', NULL),
	(11007, 'Site Map - Booking Performance Wallboard', 'Site Map - Booking Performance Wallboard', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:21:26', NULL),
	(11008, 'Site Map - Diary Wall Board', 'Site Map - Diary Wall Board', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:21:26', NULL),
	(12002, 'Branch Service Appraisal', 'Branch Service Appraisal', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-04-15 16:21:26', NULL);
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;


-- Dumping structure for table create_new.postcode
CREATE TABLE IF NOT EXISTS `postcode` (
  `Postcode` varchar(4) NOT NULL,
  `CountyCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`Postcode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.postcode: ~0 rows (approximately)
/*!40000 ALTER TABLE `postcode` DISABLE KEYS */;
/*!40000 ALTER TABLE `postcode` ENABLE KEYS */;


-- Dumping structure for table create_new.postcode_allocation
CREATE TABLE IF NOT EXISTS `postcode_allocation` (
  `PostcodeAllocationID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) NOT NULL,
  `ManufacturerID` int(11) NOT NULL,
  `RepairSkillID` int(11) DEFAULT NULL,
  `JobTypeID` int(11) DEFAULT NULL,
  `ServiceTypeID` int(11) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  `CountryID` int(11) DEFAULT NULL,
  `ServiceProviderID` int(11) NOT NULL,
  `OutOfArea` enum('No','Yes') NOT NULL DEFAULT 'No',
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PostcodeAllocationID`),
  KEY `IDX_postcode_allocation_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_postcode_allocation_NetworkID_FK` (`NetworkID`),
  KEY `IDX_postcode_allocation_ManufacturerID_FK` (`ManufacturerID`),
  KEY `IDX_postcode_allocation_RepairSkillID_FK` (`RepairSkillID`),
  KEY `IDX_postcode_allocation_JobTypeID_FK` (`JobTypeID`),
  KEY `IDX_postcode_allocation_ServiceTypeID_FK` (`ServiceTypeID`),
  KEY `IDX_postcode_allocation_ClientID_FK` (`ClientID`),
  KEY `IDX_postcode_allocation_CountryID_FK` (`CountryID`),
  KEY `IDX_postcode_allocation_ServiceProviderID_FK` (`ServiceProviderID`),
  CONSTRAINT `client_TO_postcode_allocation` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `country_TO_postcode_allocation` FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `job_type_TO_postcode_allocation` FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`),
  CONSTRAINT `manufacturer_TO_postcode_allocation` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `network_TO_postcode_allocation` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `repair_skill_TO_postcode_allocation` FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`),
  CONSTRAINT `service_provider_TO_postcode_allocation` FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`),
  CONSTRAINT `service_type_TO_postcode_allocation` FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`),
  CONSTRAINT `user_TO_postcode_allocation` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.postcode_allocation: ~0 rows (approximately)
/*!40000 ALTER TABLE `postcode_allocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `postcode_allocation` ENABLE KEYS */;


-- Dumping structure for table create_new.postcode_allocation_data
CREATE TABLE IF NOT EXISTS `postcode_allocation_data` (
  `PostcodeAllocationID` int(11) NOT NULL,
  `PostcodeArea` varchar(15) NOT NULL,
  KEY `IDX_postcode_allocation_data_1` (`PostcodeArea`),
  KEY `IDX_postcode_allocation_data_PostcodeAllocationID_FK` (`PostcodeAllocationID`),
  CONSTRAINT `postcode_allocation_TO_postcode_allocation_data` FOREIGN KEY (`PostcodeAllocationID`) REFERENCES `postcode_allocation` (`PostcodeAllocationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.postcode_allocation_data: ~0 rows (approximately)
/*!40000 ALTER TABLE `postcode_allocation_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `postcode_allocation_data` ENABLE KEYS */;


-- Dumping structure for table create_new.postcode_lat_long
CREATE TABLE IF NOT EXISTS `postcode_lat_long` (
  `PostCode` varchar(10) NOT NULL,
  `Latitude` float DEFAULT NULL,
  `Longitude` float DEFAULT NULL,
  PRIMARY KEY (`PostCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.postcode_lat_long: ~0 rows (approximately)
/*!40000 ALTER TABLE `postcode_lat_long` DISABLE KEYS */;
/*!40000 ALTER TABLE `postcode_lat_long` ENABLE KEYS */;


-- Dumping structure for table create_new.preferential_clients_manufacturers
CREATE TABLE IF NOT EXISTS `preferential_clients_manufacturers` (
  `UserID` int(11) NOT NULL,
  `PreferentialID` int(11) NOT NULL,
  `PageName` varchar(40) NOT NULL,
  `PreferentialType` enum('Manufacturer','Brand','ServiceProvider') NOT NULL,
  `Priority` int(11) DEFAULT NULL,
  UNIQUE KEY `UserID_2` (`UserID`,`PreferentialID`,`PageName`,`PreferentialType`),
  KEY `UserID` (`UserID`,`PageName`,`PreferentialType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.preferential_clients_manufacturers: ~0 rows (approximately)
/*!40000 ALTER TABLE `preferential_clients_manufacturers` DISABLE KEYS */;
/*!40000 ALTER TABLE `preferential_clients_manufacturers` ENABLE KEYS */;


-- Dumping structure for table create_new.primary_fields
CREATE TABLE IF NOT EXISTS `primary_fields` (
  `primaryFieldID` int(11) NOT NULL AUTO_INCREMENT,
  `primaryFieldName` varchar(250) NOT NULL,
  PRIMARY KEY (`primaryFieldID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.primary_fields: ~1 rows (approximately)
/*!40000 ALTER TABLE `primary_fields` DISABLE KEYS */;
INSERT IGNORE INTO `primary_fields` (`primaryFieldID`, `primaryFieldName`) VALUES
	(1, 'Model No'),
	(2, 'Authorisation No.'),
	(3, 'Referral Number'),
	(4, 'Catalogue No'),
	(5, 'Return'),
	(6, 'Repair'),
	(7, 'On Hold'),
	(8, 'Estimate'),
	(9, 'Invoice'),
	(10, 'Courier'),
	(11, 'Consignment No'),
	(12, 'Consignment Date'),
	(13, 'Model No');
/*!40000 ALTER TABLE `primary_fields` ENABLE KEYS */;


-- Dumping structure for table create_new.product
CREATE TABLE IF NOT EXISTS `product` (
  `ProductID` int(11) NOT NULL AUTO_INCREMENT,
  `ModelID` int(11) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  `UnitTypeID` int(11) DEFAULT NULL,
  `NetworkID` int(11) DEFAULT NULL,
  `ProductNo` varchar(10) DEFAULT NULL,
  `ActualSellingPrice` decimal(10,2) DEFAULT '0.00',
  `AuthorityLimit` decimal(10,2) DEFAULT '0.00',
  `PaymentRoute` enum('Claim Back','Invoice') DEFAULT NULL,
  `Supplier` varchar(30) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ProductID`),
  KEY `IDX_product_ModelID_FK` (`ModelID`),
  KEY `IDX_product_ClientID_FK` (`ClientID`),
  KEY `IDX_product_UnitTypeID_FK` (`UnitTypeID`),
  KEY `IDX_product_NetworkID_FK` (`NetworkID`),
  KEY `IDX_product_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `client_TO_product` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `model_TO_product` FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`),
  CONSTRAINT `network_TO_product` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `unit_type_TO_product` FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`),
  CONSTRAINT `user_TO_product` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.product: ~0 rows (approximately)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
/*!40000 ALTER TABLE `product` ENABLE KEYS */;


-- Dumping structure for table create_new.product_code
CREATE TABLE IF NOT EXISTS `product_code` (
  `ProductCodeID` int(10) NOT NULL AUTO_INCREMENT,
  `ProductCodeName` varchar(50) NOT NULL DEFAULT '0',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`ProductCodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.product_code: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_code` ENABLE KEYS */;


-- Dumping structure for table create_new.product_code_model
CREATE TABLE IF NOT EXISTS `product_code_model` (
  `ProductCodeModelID` int(10) NOT NULL AUTO_INCREMENT,
  `ModelID` int(10) DEFAULT NULL,
  `ProductCodeID` int(10) DEFAULT NULL,
  PRIMARY KEY (`ProductCodeModelID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.product_code_model: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_code_model` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_code_model` ENABLE KEYS */;


-- Dumping structure for table create_new.ra_history
CREATE TABLE IF NOT EXISTS `ra_history` (
  `RAHistoryID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) NOT NULL,
  `RAStatusID` int(11) NOT NULL,
  `Notes` varchar(1000) DEFAULT NULL,
  `CreatedDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ModifiedUserID` int(11) NOT NULL,
  `RAType` enum('repair','return','estimate','invoice','on hold') DEFAULT NULL,
  PRIMARY KEY (`RAHistoryID`),
  KEY `IDX_ra_history_JobID_FK` (`JobID`),
  KEY `IDX_ra_history_RAStatusID_FK` (`RAStatusID`),
  KEY `IDX_ra_history_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `job_TO_ra_history` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`),
  CONSTRAINT `ra_status_TO_ra_history_2` FOREIGN KEY (`RAStatusID`) REFERENCES `ra_status` (`RAStatusID`),
  CONSTRAINT `user_TO_ra_history` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.ra_history: ~0 rows (approximately)
/*!40000 ALTER TABLE `ra_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `ra_history` ENABLE KEYS */;


-- Dumping structure for table create_new.ra_status
CREATE TABLE IF NOT EXISTS `ra_status` (
  `RAStatusID` int(11) NOT NULL AUTO_INCREMENT,
  `ListOrder` int(11) NOT NULL,
  `BrandID` int(11) NOT NULL,
  `Status` enum('Active','In-active') DEFAULT NULL,
  `CreatedDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ModifiedUserID` int(11) NOT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `RAStatusName` varchar(40) DEFAULT NULL,
  `RAStatusCode` int(11) NOT NULL,
  `Colour` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`RAStatusID`),
  KEY `IDX_ra_status_BrandID_FK` (`BrandID`),
  KEY `IDX_ra_status_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `brand_TO_ra_status` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `user_TO_ra_status` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.ra_status: ~0 rows (approximately)
/*!40000 ALTER TABLE `ra_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `ra_status` ENABLE KEYS */;


-- Dumping structure for table create_new.ra_status_subset
CREATE TABLE IF NOT EXISTS `ra_status_subset` (
  `ParentRAStatusID` int(11) NOT NULL,
  `ChildRAStatusID` int(11) NOT NULL,
  UNIQUE KEY `IDX_ra_status_subset_1` (`ParentRAStatusID`,`ChildRAStatusID`),
  CONSTRAINT `ra_status_TO_ra_status_subset` FOREIGN KEY (`ParentRAStatusID`) REFERENCES `ra_status` (`RAStatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.ra_status_subset: ~0 rows (approximately)
/*!40000 ALTER TABLE `ra_status_subset` DISABLE KEYS */;
/*!40000 ALTER TABLE `ra_status_subset` ENABLE KEYS */;


-- Dumping structure for table create_new.repair_skill
CREATE TABLE IF NOT EXISTS `repair_skill` (
  `RepairSkillID` int(11) NOT NULL AUTO_INCREMENT,
  `RepairSkillName` varchar(40) DEFAULT NULL,
  `RepairSkillCode` enum('ConsumerElectronics','DomesticAppliance') DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`RepairSkillID`),
  KEY `IDX_repair_skill_1` (`RepairSkillName`),
  KEY `IDX_repair_skill_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `user_TO_repair_skill` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.repair_skill: ~0 rows (approximately)
/*!40000 ALTER TABLE `repair_skill` DISABLE KEYS */;
INSERT IGNORE INTO `repair_skill` (`RepairSkillID`, `RepairSkillName`, `RepairSkillCode`, `CreatedDate`, `EndDate`, `Status`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 'AERIAL', 'ConsumerElectronics', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-09 11:15:12'),
	(2, 'BROWN GOODS', 'ConsumerElectronics', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-09 11:15:14'),
	(3, 'COMPUTER', 'ConsumerElectronics', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-09 11:15:16'),
	(4, 'WHITE GOODS', 'DomesticAppliance', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-09 11:15:18'),
	(5, 'CAMERA', 'ConsumerElectronics', '2012-10-25 13:30:19', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-09 11:15:20'),
	(6, 'GAS', 'DomesticAppliance', '2012-10-25 13:30:19', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-09 11:15:21'),
	(7, 'REFRIDGERATION', 'DomesticAppliance', '2012-10-25 13:30:19', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-09 11:15:26'),
	(8, 'BANG & OLUFSEN', 'ConsumerElectronics', '2012-11-01 14:16:59', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-09 11:15:28'),
	(9, 'OTHER', 'ConsumerElectronics', '2012-11-26 14:51:55', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-09 11:15:30');
/*!40000 ALTER TABLE `repair_skill` ENABLE KEYS */;


-- Dumping structure for table create_new.report_progress
CREATE TABLE IF NOT EXISTS `report_progress` (
  `Hash` varchar(40) DEFAULT NULL,
  `Status` enum('running','finished','abort') DEFAULT NULL,
  `TotalRows` int(11) DEFAULT NULL,
  `CurrentRow` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.report_progress: ~0 rows (approximately)
/*!40000 ALTER TABLE `report_progress` DISABLE KEYS */;
/*!40000 ALTER TABLE `report_progress` ENABLE KEYS */;


-- Dumping structure for table create_new.retailer
CREATE TABLE IF NOT EXISTS `retailer` (
  `RetailerID` int(11) NOT NULL AUTO_INCREMENT,
  `RetailerName` varchar(100) NOT NULL,
  `BranchID` int(11) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`RetailerID`),
  KEY `BranchID` (`BranchID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.retailer: ~0 rows (approximately)
/*!40000 ALTER TABLE `retailer` DISABLE KEYS */;
/*!40000 ALTER TABLE `retailer` ENABLE KEYS */;


-- Dumping structure for table create_new.role
CREATE TABLE IF NOT EXISTS `role` (
  `RoleID` int(11) NOT NULL AUTO_INCREMENT,
  `Parent` int(11) DEFAULT '0',
  `Name` varchar(40) NOT NULL,
  `UserType` enum('Service Network','Client','Branch','Service Provider','Manufacturer','Extended Warrantor') NOT NULL,
  `Comment` text,
  `InactivityTimeout` int(11) NOT NULL DEFAULT '24' COMMENT 'Inactivity Timeout for logged in users in minutes',
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`RoleID`),
  KEY `IDX_role_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `user_TO_role` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.role: ~0 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


-- Dumping structure for table create_new.security_question
CREATE TABLE IF NOT EXISTS `security_question` (
  `SecurityQuestionID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `Question` varchar(255) NOT NULL,
  `QuestionCode` int(11) NOT NULL,
  `SystemCode` varchar(255) NOT NULL,
  `Status` enum('Active','In-active') NOT NULL,
  PRIMARY KEY (`SecurityQuestionID`),
  KEY `IDX_security_question_BrandID_FK` (`BrandID`),
  CONSTRAINT `brand_TO_security_question` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table contains Security Question details.';

-- Dumping data for table create_new.security_question: ~0 rows (approximately)
/*!40000 ALTER TABLE `security_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `security_question` ENABLE KEYS */;


-- Dumping structure for table create_new.service_provider
CREATE TABLE IF NOT EXISTS `service_provider` (
  `ServiceProviderID` int(11) NOT NULL AUTO_INCREMENT,
  `CompanyName` varchar(50) DEFAULT NULL,
  `Acronym` varchar(50) DEFAULT NULL,
  `BuildingNameNumber` varchar(40) DEFAULT NULL,
  `PostalCode` varchar(10) DEFAULT NULL,
  `Street` varchar(40) DEFAULT NULL,
  `LocalArea` varchar(40) DEFAULT NULL,
  `TownCity` varchar(60) DEFAULT NULL,
  `CountyID` int(11) DEFAULT NULL,
  `CountryID` int(11) DEFAULT NULL,
  `WeekdaysOpenTime` time DEFAULT NULL,
  `WeekdaysCloseTime` time DEFAULT NULL,
  `SaturdayOpenTime` time DEFAULT NULL,
  `SaturdayCloseTime` time DEFAULT NULL,
  `SundayOpenTime` time DEFAULT NULL,
  `SundayCloseTime` time DEFAULT NULL,
  `ServiceProvided` text,
  `SynopsisOfBusiness` text,
  `ContactPhoneExt` varchar(10) DEFAULT NULL,
  `ContactPhone` varchar(40) DEFAULT NULL,
  `ContactEmail` varchar(40) DEFAULT NULL,
  `AdminSupervisor` varchar(40) DEFAULT NULL,
  `ServiceManager` varchar(40) DEFAULT NULL,
  `Accounts` varchar(40) DEFAULT NULL,
  `IPAddress` varchar(40) DEFAULT NULL,
  `Port` int(11) DEFAULT NULL,
  `InvoiceToCompanyName` varchar(40) DEFAULT NULL,
  `UseAddressProgram` enum('Yes','No') DEFAULT NULL,
  `ServiceBaseVersion` varchar(255) DEFAULT NULL,
  `ServiceBaseLicence` varchar(255) DEFAULT NULL,
  `VATRateID` varchar(40) DEFAULT NULL,
  `VATNumber` varchar(40) DEFAULT NULL,
  `Platform` enum('ServiceBase','API','Skyline','RMA Tracker') DEFAULT NULL,
  `Skin` varchar(20) DEFAULT NULL,
  `ReplyEmail` varchar(64) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ViamenteKey` varchar(50) DEFAULT NULL,
  `DiaryStatus` enum('Active','In-Active') NOT NULL DEFAULT 'In-Active',
  `OnlineDiary` enum('Active','In-Active') DEFAULT 'In-Active',
  `DefaultTravelTime` int(11) NOT NULL DEFAULT '120',
  `DefaultTravelSpeed` int(11) NOT NULL DEFAULT '120',
  `AutoChangeTimeSlotLabel` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `CourierStatus` enum('Active','In-active') NOT NULL DEFAULT 'In-active',
  `RunViamenteToday` enum('Yes','No') DEFAULT 'No',
  `DiaryAdministratorEmail` varchar(255) DEFAULT NULL,
  `SetupUniqueTimeSlotPostcodes` enum('No','Yes') NOT NULL DEFAULT 'No',
  `UnlockingPassword` varchar(64) DEFAULT NULL,
  `PasswordProtectNextDayBookings` enum('Yes','No') NOT NULL DEFAULT 'No',
  `PasswordProtectNextDayBookingsTime` time DEFAULT NULL,
  `AutoSelectDay` enum('','Today','NextDay') DEFAULT '',
  `AutoDisplayTable` enum('','Summary','Appointment') DEFAULT '',
  `AutoSpecifyEngineerByPostcode` enum('Yes','No') DEFAULT 'No',
  `ServiceManagerForename` varchar(50) DEFAULT NULL,
  `ServiceManagerSurname` varchar(50) DEFAULT NULL,
  `ServiceManagerEmail` varchar(100) DEFAULT NULL,
  `AdminSupervisorForename` varchar(50) DEFAULT NULL,
  `AdminSupervisorSurname` varchar(50) DEFAULT NULL,
  `AdminSupervisorEmail` varchar(100) DEFAULT NULL,
  `ViamenteRunType` enum('AllEngineers','CurrentEngineer') NOT NULL DEFAULT 'AllEngineers',
  `SendASCReport` enum('Yes','No') DEFAULT NULL,
  `EngineerDefaultStartTime` time DEFAULT NULL,
  `EngineerDefaultEndTime` time DEFAULT NULL,
  `NumberOfVehicles` int(11) DEFAULT NULL,
  `NumberOfWaypoints` int(11) DEFAULT NULL,
  `DiaryAllocationType` enum('Postcode','GridMapping') NOT NULL DEFAULT 'Postcode',
  `SendRouteMapsEmail` time DEFAULT NULL,
  `Multimaps` int(2) DEFAULT '6',
  `DiaryType` enum('FullViamente','NoViamente','AllocationOnly') NOT NULL DEFAULT 'FullViamente',
  `DiaryShowSlotNumbers` enum('Yes','No') NOT NULL DEFAULT 'No',
  `EmailAppBooking` enum('Yes','No') NOT NULL DEFAULT 'No',
  `EmailServiceInstruction` enum('Active','In-Active') DEFAULT 'In-Active',
  `ViamenteKey2` varchar(50) DEFAULT NULL,
  `ViamenteKey2MaxEng` int(11) DEFAULT '0',
  `ViamenteKey2MaxWayPoints` int(11) DEFAULT '0',
  `DpdFtpServer` varchar(50) DEFAULT NULL,
  `DpdFtpUser` varchar(30) DEFAULT NULL,
  `DpdFtpPassword` varchar(30) DEFAULT NULL,
  `DpdFilePrefix` varchar(30) DEFAULT NULL,
  `DpdFileCount` int(11) NOT NULL DEFAULT '0',
  `PublicWebsiteAddress` varchar(100) DEFAULT NULL,
  `GeneralManagerForename` varchar(50) DEFAULT NULL,
  `GeneralManagerSurname` varchar(50) DEFAULT NULL,
  `GeneralManagerEmail` varchar(100) DEFAULT NULL,
  `LockAppWindow` enum('Yes','No') NOT NULL DEFAULT 'No',
  `LockAppWindowTime` int(11) NOT NULL DEFAULT '60',
  PRIMARY KEY (`ServiceProviderID`),
  KEY `IDX_service_provider_1` (`CompanyName`),
  KEY `IDX_service_provider_2` (`DiaryStatus`),
  KEY `IDX_service_provider_CountyID_FK` (`CountyID`),
  KEY `IDX_service_provider_CountryID_FK` (`CountryID`),
  KEY `IDX_service_provider_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `country_TO_service_provider` FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `county_TO_service_provider` FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`),
  CONSTRAINT `user_TO_service_provider` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.service_provider: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider` DISABLE KEYS */;
INSERT IGNORE INTO `service_provider` (`ServiceProviderID`, `CompanyName`, `Acronym`, `BuildingNameNumber`, `PostalCode`, `Street`, `LocalArea`, `TownCity`, `CountyID`, `CountryID`, `WeekdaysOpenTime`, `WeekdaysCloseTime`, `SaturdayOpenTime`, `SaturdayCloseTime`, `SundayOpenTime`, `SundayCloseTime`, `ServiceProvided`, `SynopsisOfBusiness`, `ContactPhoneExt`, `ContactPhone`, `ContactEmail`, `AdminSupervisor`, `ServiceManager`, `Accounts`, `IPAddress`, `Port`, `InvoiceToCompanyName`, `UseAddressProgram`, `ServiceBaseVersion`, `ServiceBaseLicence`, `VATRateID`, `VATNumber`, `Platform`, `Skin`, `ReplyEmail`, `CreatedDate`, `EndDate`, `Status`, `ModifiedUserID`, `ModifiedDate`, `ViamenteKey`, `DiaryStatus`, `OnlineDiary`, `DefaultTravelTime`, `DefaultTravelSpeed`, `AutoChangeTimeSlotLabel`, `CourierStatus`, `RunViamenteToday`, `DiaryAdministratorEmail`, `SetupUniqueTimeSlotPostcodes`, `UnlockingPassword`, `PasswordProtectNextDayBookings`, `PasswordProtectNextDayBookingsTime`, `AutoSelectDay`, `AutoDisplayTable`, `AutoSpecifyEngineerByPostcode`, `ServiceManagerForename`, `ServiceManagerSurname`, `ServiceManagerEmail`, `AdminSupervisorForename`, `AdminSupervisorSurname`, `AdminSupervisorEmail`, `ViamenteRunType`, `SendASCReport`, `EngineerDefaultStartTime`, `EngineerDefaultEndTime`, `NumberOfVehicles`, `NumberOfWaypoints`, `DiaryAllocationType`, `SendRouteMapsEmail`, `Multimaps`, `DiaryType`, `DiaryShowSlotNumbers`, `EmailAppBooking`, `EmailServiceInstruction`, `ViamenteKey2`, `ViamenteKey2MaxEng`, `ViamenteKey2MaxWayPoints`, `DpdFtpServer`, `DpdFtpUser`, `DpdFtpPassword`, `DpdFilePrefix`, `DpdFileCount`, `PublicWebsiteAddress`, `GeneralManagerForename`, `GeneralManagerSurname`, `GeneralManagerEmail`, `LockAppWindow`, `LockAppWindowTime`) VALUES
	(2, 'GENERIC SERVICE CENTRE', NULL, ' ', 'nn1 5ex', ' 66 Palmerston Road', '', 'Northampton', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '.', 'generic.service.centre@skylinecms.co.uk', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, '', 'Skyline', NULL, '', '2013-02-21 12:24:05', '0000-00-00 00:00:00', 'Active', NULL, '2013-02-21 12:24:42', NULL, 'In-Active', 'In-Active', 120, 120, 'No', 'In-active', 'No', NULL, 'No', NULL, 'No', NULL, '', '', 'No', '', '', '', '', '', '', 'AllEngineers', 'No', NULL, NULL, NULL, NULL, 'Postcode', NULL, 6, 'FullViamente', 'No', 'No', 'In-Active', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'No', 60);
/*!40000 ALTER TABLE `service_provider` ENABLE KEYS */;


-- Dumping structure for table create_new.service_provider_bank_holidays
CREATE TABLE IF NOT EXISTS `service_provider_bank_holidays` (
  `ServiceProviderBankHolidaysID` int(10) NOT NULL AUTO_INCREMENT,
  `NationalBankHolidayID` int(10) NOT NULL DEFAULT '0',
  `ServiceProviderID` int(10) NOT NULL DEFAULT '0',
  `Status` enum('Open','Closed') NOT NULL DEFAULT 'Closed',
  PRIMARY KEY (`ServiceProviderBankHolidaysID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.service_provider_bank_holidays: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_bank_holidays` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_bank_holidays` ENABLE KEYS */;


-- Dumping structure for table create_new.service_provider_contacts
CREATE TABLE IF NOT EXISTS `service_provider_contacts` (
  `ServiceProviderID` int(11) NOT NULL,
  `ClientID` int(11) NOT NULL,
  `ContactPhone` varchar(40) DEFAULT NULL,
  `ContactEmail` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`ServiceProviderID`,`ClientID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.service_provider_contacts: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_contacts` ENABLE KEYS */;


-- Dumping structure for table create_new.service_provider_engineer
CREATE TABLE IF NOT EXISTS `service_provider_engineer` (
  `ServiceProviderEngineerID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `EngineerFirstName` varchar(40) DEFAULT NULL,
  `EngineerLastName` varchar(40) DEFAULT NULL,
  `ServiceBaseUserCode` varchar(4) DEFAULT NULL,
  `EmailAddress` varchar(255) DEFAULT NULL,
  `MobileNumber` varchar(20) DEFAULT NULL,
  `RouteColour` varchar(20) DEFAULT NULL,
  `StartShift` time DEFAULT NULL,
  `EndShift` time DEFAULT NULL,
  `LunchBreakDuration` tinyint(3) DEFAULT NULL,
  `LunchPeriodFrom` time NOT NULL DEFAULT '12:00:00',
  `LunchPeriodTo` time NOT NULL DEFAULT '14:00:00',
  `Status` enum('Active','In-active') DEFAULT 'Active',
  `StartPostcode` varchar(8) DEFAULT NULL,
  `EndPostcode` varchar(8) DEFAULT NULL,
  `StartLocation` enum('Home','Office','Other') DEFAULT NULL,
  `EndLocation` enum('Home','Office','Other') DEFAULT NULL,
  `StartHomePostcode` varchar(100) DEFAULT NULL COMMENT 'Used as main start pc',
  `StartOfficePostcode` varchar(10) DEFAULT NULL,
  `StartOtherPostcode` varchar(10) DEFAULT NULL,
  `EndHomePostcode` varchar(100) DEFAULT NULL COMMENT 'Used as main end pc',
  `EndOfficePostcode` varchar(10) DEFAULT NULL,
  `EndOtherPostcode` varchar(10) DEFAULT NULL,
  `MondayActive` enum('yes','no') DEFAULT 'no',
  `MondayStartShift` time DEFAULT NULL,
  `MondayEndShift` time DEFAULT NULL,
  `MondayStartPostcode` varchar(8) DEFAULT NULL,
  `MondayEndPostcode` varchar(8) DEFAULT NULL,
  `TuesdayActive` enum('yes','no') DEFAULT 'no',
  `TuesdayStartShift` time DEFAULT NULL,
  `TuesdayEndShift` time DEFAULT NULL,
  `TuesdayStartPostcode` varchar(8) DEFAULT NULL,
  `TuesdayEndPostcode` varchar(8) DEFAULT NULL,
  `WednesdayActive` enum('yes','no') DEFAULT 'no',
  `WednesdayStartShift` time DEFAULT NULL,
  `WednesdayEndShift` time DEFAULT NULL,
  `WednesdayStartPostcode` varchar(8) DEFAULT NULL,
  `WednesdayEndPostcode` varchar(8) DEFAULT NULL,
  `ThursdayActive` enum('yes','no') DEFAULT 'no',
  `ThursdayStartShift` time DEFAULT NULL,
  `ThursdayEndShift` time DEFAULT NULL,
  `ThursdayStartPostcode` varchar(8) DEFAULT NULL,
  `ThursdayEndPostcode` varchar(8) DEFAULT NULL,
  `FridayActive` enum('yes','no') DEFAULT 'no',
  `FridayStartShift` time DEFAULT NULL,
  `FridayEndShift` time DEFAULT NULL,
  `FridayStartPostcode` varchar(8) DEFAULT NULL,
  `FridayEndPostcode` varchar(8) DEFAULT NULL,
  `SaturdayActive` enum('yes','no') DEFAULT 'no',
  `SaturdayStartShift` time DEFAULT NULL,
  `SaturdayEndShift` time DEFAULT NULL,
  `SaturdayStartPostcode` varchar(8) DEFAULT NULL,
  `SaturdayEndPostcode` varchar(8) DEFAULT NULL,
  `SundayActive` enum('yes','no') DEFAULT 'no',
  `SundayStartShift` time DEFAULT NULL,
  `SundayEndShift` time DEFAULT NULL,
  `SundayStartPostcode` varchar(8) DEFAULT NULL,
  `SundayEndPostcode` varchar(8) DEFAULT NULL,
  `SentToViamente` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `Deleted` enum('No','Yes') NOT NULL DEFAULT 'No',
  `SpeedFactor` int(11) NOT NULL DEFAULT '100',
  `EngineerImportance` int(11) NOT NULL DEFAULT '50',
  `ViamenteExclude` enum('Yes','No') NOT NULL DEFAULT 'No',
  `SetupType` enum('Unique','Replicate') DEFAULT 'Unique',
  `ReplicateType` enum('FourWeeksOnly','WeeklyCycle','MonthlyCycle') DEFAULT 'FourWeeksOnly',
  `ReplicateStatusFlag` enum('No','Yes') DEFAULT 'No',
  `ReplicatePostcodeAllocation` enum('Yes','No') DEFAULT 'Yes',
  `ExcludeFromAppTable` enum('Yes','No') NOT NULL DEFAULT 'No',
  `ViamenteStartType` enum('Fixed','Flexible') NOT NULL DEFAULT 'Flexible',
  `AppsBeforeOptimise` tinyint(4) NOT NULL DEFAULT '0',
  `PrimarySkill` enum('Brown Goods','White Goods') DEFAULT NULL,
  PRIMARY KEY (`ServiceProviderEngineerID`),
  KEY `IDX_service_provider_engineer_1` (`ServiceProviderID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.service_provider_engineer: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_engineer` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_engineer` ENABLE KEYS */;


-- Dumping structure for table create_new.service_provider_engineer_details
CREATE TABLE IF NOT EXISTS `service_provider_engineer_details` (
  `ServiceProviderEngineerDetailsID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderEngineerID` int(11) NOT NULL,
  `WorkDate` date NOT NULL,
  `StartShift` time NOT NULL,
  `EndShift` time NOT NULL,
  `StartPostcode` varchar(100) NOT NULL,
  `EndPostcode` varchar(100) NOT NULL,
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`ServiceProviderEngineerDetailsID`),
  KEY `ServiceProviderEngineerID` (`ServiceProviderEngineerID`,`WorkDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.service_provider_engineer_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_engineer_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_engineer_details` ENABLE KEYS */;


-- Dumping structure for table create_new.service_provider_engineer_skillset
CREATE TABLE IF NOT EXISTS `service_provider_engineer_skillset` (
  `ServiceProviderEngineerSkillsetID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderSkillsetID` int(11) DEFAULT NULL,
  `ServiceProviderEngineerID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ServiceProviderEngineerSkillsetID`),
  KEY `IDX_service_provider_engineer_skillset_1` (`ServiceProviderSkillsetID`),
  KEY `IDX_service_provider_engineer_skillset_2` (`ServiceProviderEngineerID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.service_provider_engineer_skillset: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_engineer_skillset` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_engineer_skillset` ENABLE KEYS */;


-- Dumping structure for table create_new.service_provider_engineer_skillset_day
CREATE TABLE IF NOT EXISTS `service_provider_engineer_skillset_day` (
  `ServiceProviderEngineerSkillsetDayID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderEngineerDetailsID` int(11) NOT NULL,
  `ServiceProviderSkillsetID` int(11) NOT NULL,
  PRIMARY KEY (`ServiceProviderEngineerSkillsetDayID`),
  UNIQUE KEY `IDX_service_provider_engineer_skillset_day_1` (`ServiceProviderEngineerDetailsID`,`ServiceProviderSkillsetID`),
  KEY `ServiceProviderEngineerDetailsID` (`ServiceProviderEngineerDetailsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.service_provider_engineer_skillset_day: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_engineer_skillset_day` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_engineer_skillset_day` ENABLE KEYS */;


-- Dumping structure for table create_new.service_provider_engineer_workload
CREATE TABLE IF NOT EXISTS `service_provider_engineer_workload` (
  `ServiceProviderEngineerWorkloadID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(11) NOT NULL,
  `ServiceProviderEngineerID` int(11) NOT NULL,
  `WorkingDay` date NOT NULL,
  `TotalWorkTimeSec` int(11) NOT NULL,
  `TotalIdleTimeSec` int(11) NOT NULL,
  `TotalServiceTimeSec` int(11) NOT NULL,
  `TotalDriveTimeSec` int(11) NOT NULL,
  `TotalSteps` int(11) NOT NULL,
  PRIMARY KEY (`ServiceProviderEngineerWorkloadID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.service_provider_engineer_workload: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_engineer_workload` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_engineer_workload` ENABLE KEYS */;


-- Dumping structure for table create_new.service_provider_geotags
CREATE TABLE IF NOT EXISTS `service_provider_geotags` (
  `ServiceProviderGeoTagsID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(11) NOT NULL,
  `Postcode` varchar(8) DEFAULT NULL,
  `Latitude` float(6,4) NOT NULL,
  `Longitude` float(7,4) NOT NULL,
  PRIMARY KEY (`ServiceProviderGeoTagsID`),
  KEY `ServiceProviderID` (`ServiceProviderID`),
  KEY `Postcode` (`Postcode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.service_provider_geotags: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_geotags` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_geotags` ENABLE KEYS */;


-- Dumping structure for table create_new.service_provider_skillset
CREATE TABLE IF NOT EXISTS `service_provider_skillset` (
  `ServiceProviderSkillsetID` int(11) NOT NULL AUTO_INCREMENT,
  `SkillsetID` int(11) DEFAULT NULL,
  `ServiceDuration` smallint(4) DEFAULT NULL,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `EngineersRequired` tinyint(2) DEFAULT '1',
  `Status` enum('Active','In-active') DEFAULT 'Active',
  PRIMARY KEY (`ServiceProviderSkillsetID`),
  KEY `IDX_service_provider_skillset_1` (`ServiceProviderID`),
  KEY `IDX_service_provider_skillset_2` (`SkillsetID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.service_provider_skillset: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_skillset` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_skillset` ENABLE KEYS */;


-- Dumping structure for table create_new.service_provider_trade_account
CREATE TABLE IF NOT EXISTS `service_provider_trade_account` (
  `ServiceProviderTradeAccountID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `TradeAccount` varchar(40) DEFAULT NULL,
  `Postcode` varchar(10) DEFAULT NULL,
  `Monday` enum('Yes','No') DEFAULT 'No',
  `Tuesday` enum('Yes','No') DEFAULT 'No',
  `Wednesday` enum('Yes','No') DEFAULT 'No',
  `Thursday` enum('Yes','No') DEFAULT 'No',
  `Friday` enum('Yes','No') DEFAULT 'No',
  `Saturday` enum('Yes','No') DEFAULT 'No',
  `Sunday` enum('Yes','No') DEFAULT 'No',
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ServiceProviderTradeAccountID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.service_provider_trade_account: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_trade_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_trade_account` ENABLE KEYS */;


-- Dumping structure for table create_new.service_type
CREATE TABLE IF NOT EXISTS `service_type` (
  `ServiceTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) DEFAULT NULL,
  `JobTypeID` int(11) DEFAULT NULL,
  `ServiceTypeName` varchar(40) DEFAULT NULL,
  `ServiceTypeCode` int(11) DEFAULT NULL,
  `Code` varchar(64) DEFAULT NULL,
  `Priority` int(11) DEFAULT NULL,
  `Chargeable` enum('Y','N') NOT NULL DEFAULT 'N',
  `Warranty` enum('Y','N') NOT NULL DEFAULT 'N',
  `DateOfPurchaseForced` enum('N','B','C') NOT NULL DEFAULT 'N',
  `OverrideTradeSettingsPolicy` enum('Y','N') NOT NULL DEFAULT 'N',
  `ForcePolicyNoAtBooking` enum('Y','N') NOT NULL DEFAULT 'N',
  `OverrideTradeSettingsModel` enum('Y','N') NOT NULL DEFAULT 'N',
  `ForceModelNumber` enum('Y','N') NOT NULL DEFAULT 'N',
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ServiceTypeID`),
  KEY `IDX_service_type_1` (`ServiceTypeName`),
  KEY `IDX_service_type_BrandID_FK` (`BrandID`),
  KEY `IDX_service_type_JobTypeID_FK` (`JobTypeID`),
  KEY `IDX_service_type_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_Warranty` (`Warranty`),
  CONSTRAINT `brand_TO_service_type` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `job_type_TO_service_type` FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`),
  CONSTRAINT `user_TO_service_type` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.service_type: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_type` ENABLE KEYS */;


-- Dumping structure for table create_new.service_type_alias
CREATE TABLE IF NOT EXISTS `service_type_alias` (
  `ServiceTypeAliasID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) DEFAULT NULL,
  `ClientID` int(11) NOT NULL,
  `ServiceTypeID` int(11) NOT NULL,
  `ServiceTypeMask` int(11) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ServiceTypeAliasID`),
  UNIQUE KEY `TUC_service_type_alias_1` (`ClientID`,`NetworkID`,`ServiceTypeID`),
  KEY `IDX_service_type_alias_ClientID_FK` (`ClientID`),
  KEY `IDX_service_type_alias_ServiceTypeID_FK` (`ServiceTypeID`),
  KEY `IDX_service_type_alias_NetworkID_FK` (`NetworkID`),
  KEY `IDX_service_type_alias_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `client_TO_service_type_alias` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `network_TO_service_type_alias` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `service_type_TO_service_type_alias` FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`),
  CONSTRAINT `user_TO_service_type_alias` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.service_type_alias: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_type_alias` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_type_alias` ENABLE KEYS */;


-- Dumping structure for table create_new.shipping
CREATE TABLE IF NOT EXISTS `shipping` (
  `ShippingID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) NOT NULL,
  `CourierID` int(11) DEFAULT NULL,
  `ConsignmentNo` varchar(50) DEFAULT NULL,
  `ConsignmentDate` timestamp NULL DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ShippingID`),
  KEY `job_TO_shipping` (`JobID`),
  KEY `courier_TO_shipping` (`CourierID`),
  CONSTRAINT `courier_TO_shipping` FOREIGN KEY (`CourierID`) REFERENCES `courier` (`CourierID`),
  CONSTRAINT `job_TO_shipping` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.shipping: ~0 rows (approximately)
/*!40000 ALTER TABLE `shipping` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipping` ENABLE KEYS */;


-- Dumping structure for table create_new.skillset
CREATE TABLE IF NOT EXISTS `skillset` (
  `SkillsetID` int(11) NOT NULL AUTO_INCREMENT,
  `SkillsetName` varchar(40) DEFAULT NULL,
  `ServiceDuration` smallint(4) DEFAULT NULL,
  `EngineersRequired` tinyint(2) DEFAULT '1',
  `RepairSkillID` int(11) DEFAULT NULL,
  `ServiceTypeID` int(11) DEFAULT NULL,
  `Status` enum('Active','In-active') DEFAULT 'Active',
  `AppointmentTypeID` int(11) DEFAULT NULL,
  PRIMARY KEY (`SkillsetID`),
  KEY `IDX_skillset_1` (`AppointmentTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.skillset: ~0 rows (approximately)
/*!40000 ALTER TABLE `skillset` DISABLE KEYS */;
INSERT IGNORE INTO `skillset` (`SkillsetID`, `SkillsetName`, `ServiceDuration`, `EngineersRequired`, `RepairSkillID`, `ServiceTypeID`, `Status`, `AppointmentTypeID`) VALUES
	(1, 'Aerial Installation', 60, 1, 1, NULL, 'Active', 2),
	(2, 'Aerial Inspection', 120, 1, 1, NULL, 'Active', 3),
	(3, 'Aerial Repair', 60, 1, 1, NULL, 'Active', 5),
	(4, 'Brown Goods Delivery', 20, 1, 2, NULL, 'Active', 1),
	(5, 'Brown Goods Installation', 30, 1, 2, NULL, 'Active', 2),
	(6, 'Brown Goods Inspection', 60, 1, 2, NULL, 'Active', 3),
	(7, 'Brown Goods Install Wallmount', 60, 1, 2, NULL, 'Active', 7),
	(8, 'Brown Goods Collection', 30, 1, 2, NULL, 'Active', 4),
	(9, 'Brown Goods Repair', 40, 1, 2, NULL, 'Active', 5),
	(11, 'Camera Collection', 10, 1, 5, NULL, 'Active', 4),
	(12, 'Camera Send-in', 1, 1, 5, NULL, 'Active', 6),
	(13, 'Camera Repair', 60, 1, 5, NULL, 'Active', 5),
	(14, 'Computer Delivery', 15, 1, 3, NULL, 'Active', 1),
	(16, 'Computer Collection', 10, 1, 3, NULL, 'Active', 4),
	(17, 'Computer Send-in', 5, 1, 3, NULL, 'Active', 6),
	(18, 'Computer Repair', 60, 1, 3, NULL, 'Active', 5),
	(20, 'Camera Delivery', 15, 1, 5, NULL, 'Active', 1),
	(25, 'Computer Installation', 30, 1, 3, NULL, 'Active', 2),
	(29, 'Gas Delivery', 45, 1, 6, NULL, 'Active', 1),
	(30, 'Gas Installation', 120, 1, 6, NULL, 'Active', 2),
	(31, 'Gas Inspection', 240, 1, 6, NULL, 'Active', 3),
	(32, 'Gas Collection', 45, 1, 6, NULL, 'Active', 4),
	(33, 'Gas Repair', 180, 1, 6, NULL, 'Active', 5),
	(34, 'Refridgeration Delivery', 30, 1, 7, NULL, 'Active', 1),
	(35, 'Refrigeration Installation', 45, 1, 7, NULL, 'Active', 2),
	(36, 'Refrigeration Inspection', 90, 1, 7, NULL, 'Active', 3),
	(37, 'Refrigeration Collection', 15, 1, 7, NULL, 'Active', 4),
	(38, 'Refrigeration Repair', 120, 1, 7, NULL, 'Active', 5),
	(39, 'White Goods Delivery', 10, 1, 4, NULL, 'Active', 1),
	(40, 'White Goods Installation', 25, 1, 4, NULL, 'Active', 2),
	(42, 'White Goods Collection', 50, 1, 4, NULL, 'Active', 4),
	(43, 'White Goods Repair', 50, 1, 4, NULL, 'Active', 5),
	(44, 'Brown Goods Collection (2 Person)', 30, 2, 2, NULL, 'Active', 9),
	(45, 'Brown Goods Delivery (2 Person)', 20, 2, 2, NULL, 'Active', 8),
	(46, 'Brown Goods Wall Mount (2 Person)', 60, 2, 2, NULL, 'Active', 12),
	(47, 'Brown Goods Repair (2 Person)', 40, 2, 2, NULL, 'Active', 13),
	(48, 'White Goods Collection (2 Person)', 50, 2, 4, NULL, 'Active', 9),
	(49, 'White Goods Delivery (2 Person)', 10, 2, 4, NULL, 'Active', 8),
	(50, 'White Goods Installation (2 Person)', 25, 2, 4, NULL, 'Active', 11),
	(51, 'White Goods Repair (2 Person)', 50, 2, 4, NULL, 'Active', 13),
	(52, 'Aerial Inspection (2 Person)', 120, 2, 1, NULL, 'Active', 10),
	(53, 'Brown Goods Inspection (2 Person)', 60, 2, 2, NULL, 'Active', 10),
	(54, 'BANG & OLUFSEN Delivery', 15, 1, 8, NULL, 'Active', 1),
	(55, 'BANG & OLUFSEN Delivery (2 Person)', 15, 2, 8, NULL, 'Active', 8),
	(56, 'BANG & OLUFSEN Collection', 15, 1, 8, NULL, 'Active', 4),
	(57, 'BANG & OLUFSEN Collection (2 Person)', 30, 2, 8, NULL, 'Active', 9),
	(58, 'BANG & OLUFSEN Install Wallmount (2 Pers', 60, 2, 8, NULL, 'Active', 12),
	(59, 'BANG & OLUFSEN Repair', 60, 1, 8, NULL, 'Active', 5),
	(60, 'BANG & OLUFSEN Repair (2 Person)', 60, 2, 8, NULL, 'Active', 13),
	(61, 'White Goods Inspection', 30, 1, 4, NULL, 'Active', 3),
	(62, 'Brown Goods Return', 30, 1, 2, NULL, 'Active', 14),
	(63, 'Brown Goods Fit Parts', 30, 1, 2, NULL, 'Active', 15),
	(64, 'Brown Goods Fit Parts (2 Person)', 30, 2, 2, NULL, 'Active', 16);
/*!40000 ALTER TABLE `skillset` ENABLE KEYS */;


-- Dumping structure for table create_new.sp_engineer_day_location
CREATE TABLE IF NOT EXISTS `sp_engineer_day_location` (
  `SpEngineerDayLocation` int(10) NOT NULL AUTO_INCREMENT,
  `ServiceProviderEngineerID` int(10) NOT NULL DEFAULT '0',
  `Langitude` decimal(17,4) NOT NULL DEFAULT '0.0000',
  `Longitude` decimal(17,4) NOT NULL DEFAULT '0.0000',
  `DateModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LunchTaken` enum('Yes','No') NOT NULL DEFAULT 'No',
  PRIMARY KEY (`SpEngineerDayLocation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.sp_engineer_day_location: ~0 rows (approximately)
/*!40000 ALTER TABLE `sp_engineer_day_location` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_engineer_day_location` ENABLE KEYS */;


-- Dumping structure for table create_new.sp_geo_cells
CREATE TABLE IF NOT EXISTS `sp_geo_cells` (
  `SpGeoCellsID` int(10) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(10) NOT NULL DEFAULT '0',
  `DiaryAllocationID` int(10) NOT NULL DEFAULT '0',
  `Lat1` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Lng1` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Lat2` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Lng2` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`SpGeoCellsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='stores service provider geocell information';

-- Dumping data for table create_new.sp_geo_cells: ~0 rows (approximately)
/*!40000 ALTER TABLE `sp_geo_cells` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_geo_cells` ENABLE KEYS */;


-- Dumping structure for table create_new.sp_grid_defaults
CREATE TABLE IF NOT EXISTS `sp_grid_defaults` (
  `SpGridDefaultsID` int(10) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(10) DEFAULT NULL,
  `Lat1` decimal(10,2) DEFAULT NULL,
  `Lng1` decimal(10,2) DEFAULT NULL,
  `Lat2` decimal(10,2) DEFAULT NULL,
  `Lng2` decimal(10,2) DEFAULT NULL,
  `CellSize` decimal(10,2) NOT NULL DEFAULT '1.00',
  `UpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UserUpdated` varchar(50) NOT NULL,
  PRIMARY KEY (`SpGridDefaultsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.sp_grid_defaults: ~0 rows (approximately)
/*!40000 ALTER TABLE `sp_grid_defaults` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_grid_defaults` ENABLE KEYS */;


-- Dumping structure for table create_new.status
CREATE TABLE IF NOT EXISTS `status` (
  `StatusID` int(11) NOT NULL AUTO_INCREMENT,
  `StatusName` varchar(40) DEFAULT NULL,
  `TimelineStatus` enum('booked','viewed','in_progress','parts_ordered','parts_received','site_visit','revisit','closed') DEFAULT NULL,
  `Colour` varchar(10) DEFAULT NULL,
  `Category` enum('Cancelled','Client','Complete','Field Call','In Progress','Invoiced','Parts','System','Waiting For Info') DEFAULT NULL,
  `Branch` tinyint(1) DEFAULT NULL,
  `ServiceProvider` tinyint(1) DEFAULT NULL,
  `FieldCall` tinyint(1) DEFAULT NULL,
  `Support` tinyint(1) DEFAULT NULL,
  `PartsOrders` tinyint(1) DEFAULT NULL,
  `RelatedTable` varchar(40) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `StatusTAT` int(11) DEFAULT '0',
  `StatusTATType` enum('Minutes','Hours','Days') DEFAULT 'Minutes',
  PRIMARY KEY (`StatusID`),
  KEY `IDX_status_1` (`StatusName`),
  KEY `IDX_status_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `user_TO_status` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.status: ~0 rows (approximately)
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT IGNORE INTO `status` (`StatusID`, `StatusName`, `TimelineStatus`, `Colour`, `Category`, `Branch`, `ServiceProvider`, `FieldCall`, `Support`, `PartsOrders`, `RelatedTable`, `CreatedDate`, `EndDate`, `Status`, `ModifiedUserID`, `ModifiedDate`, `StatusTAT`, `StatusTATType`) VALUES
	(1, '01 UNALLOCATED JOB', 'booked', 'ef1a1a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:55', 0, 'Minutes'),
	(2, '02 ALLOCATED TO AGENT', 'booked', 'ffff56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:55', 0, 'Minutes'),
	(3, '03 TRANSMITTED TO AGENT', 'booked', '7fff00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:55', 0, 'Minutes'),
	(4, '04 JOB VIEWED BY AGENT', 'viewed', '007f7f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:55', 0, 'Minutes'),
	(5, '34 PACKAGING SENT', 'viewed', '9bdb5c', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:55', 0, 'Minutes'),
	(6, '30 OUT CARD LEFT', 'site_visit', 'ff00ff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:55', 0, 'Minutes'),
	(7, '39 CX NOT AVAIL - MESSAGE LEFT', 'viewed', 'aaffff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:55', 0, 'Minutes'),
	(8, '40 CX NOT AVAIL - NO RESPONSE', 'viewed', 'ff7f00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:55', 0, 'Minutes'),
	(9, '41 CX NOT AVAIL - WRONG NUM', 'viewed', 'ff00ff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:56', 0, 'Minutes'),
	(10, '05 FIELD CALL REQUIRED', 'viewed', 'ff5656', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:56', 0, 'Minutes'),
	(11, '06 FIELD CALL ARRANGED', 'site_visit', 'ffff56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:56', 0, 'Minutes'),
	(12, '07 UNIT IN REPAIR', 'in_progress', '03bc03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:56', 0, 'Minutes'),
	(13, '31 WAITING FOR INFO', 'in_progress', 'f27f13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:56', 0, 'Minutes'),
	(14, '32 INFO RECEIVED', 'in_progress', '00bf00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:56', 0, 'Minutes'),
	(15, '08 ESTIMATE SENT', 'in_progress', '00fc0c', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:56', 0, 'Minutes'),
	(16, '09 ESTIMATE ACCEPTED', 'in_progress', '56ff9a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:56', 0, 'Minutes'),
	(17, '10 ESTIMATE REFUSED', 'in_progress', '4c4c4c', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:56', 0, 'Minutes'),
	(18, '11 PARTS REQUIRED', 'in_progress', 'bf5f00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:56', 0, 'Minutes'),
	(19, '12 PARTS ORDERED', 'parts_ordered', '00007f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:56', 0, 'Minutes'),
	(20, '13 PARTS RECEIVED', 'parts_received', '10eaea', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:56', 0, 'Minutes'),
	(21, '14 2ND FIELD CALL REQ.', 'parts_received', 'ff007f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:56', 0, 'Minutes'),
	(22, '15 2ND FIELD CALL BKD.', 'revisit', 'ff0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:56', 0, 'Minutes'),
	(23, '16 AUTHORITY REQUIRED', 'viewed', '7f0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:57', 0, 'Minutes'),
	(24, '17 AUTHORITY ISSUED', 'viewed', '02ef02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:57', 0, 'Minutes'),
	(25, '18 REPAIR COMPLETED', 'closed', '00007f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:57', 0, 'Minutes'),
	(26, '18 FIELD CALL COMPLETE', 'in_progress', '56ffff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:57', 0, 'Minutes'),
	(27, '19 DELIVER BACK REQ.', 'parts_received', 'ff7f00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:57', 0, 'Minutes'),
	(28, '20 DELIVER BACK ARR.', 'revisit', '74c607', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:57', 0, 'Minutes'),
	(29, '21C INVOICED', 'closed', 'bfbf00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:57', 0, 'Minutes'),
	(30, '21W CLAIMED', 'closed', 'ffff00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:57', 0, 'Minutes'),
	(31, '25 INVOICE PAID', 'closed', 'ffaad4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:57', 0, 'Minutes'),
	(32, '26 INVOICE RECONCILED', 'closed', '7f7f7f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:57', 0, 'Minutes'),
	(33, '29 JOB CANCELLED', 'closed', '000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:57', 0, 'Minutes'),
	(34, '33 PART NO LONGER AVAILABLE', 'parts_ordered', 'cccccc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:57', 0, 'Minutes'),
	(35, '35 EINVOICE PENDING', 'closed', '56ffff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:57', 0, 'Minutes'),
	(36, '36 EINVOICE REJECTED', 'closed', 'ff0004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:58', 0, 'Minutes'),
	(37, '37 EINVOICE RESUBMITTED', 'closed', 'ffaaaa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:58', 0, 'Minutes'),
	(38, '38 EINVOICE APPROVED', 'closed', '56ff56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:58', 0, 'Minutes'),
	(39, '00 REPAIR AUTHORISATION', 'booked', 'aabbcc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:18:58', 0, 'Minutes'),
	(40, '18A ALTERNATIVE RESOLUTION', 'closed', 'ffff56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:22:34', 0, 'Minutes'),
	(41, '29A JOB CANCELLED - NO CONTACT', 'closed', '000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:22:13', 0, 'Minutes'),
	(42, '07 AWAITING REPAIR', 'in_progress', '03bc03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-01-24 13:34:48', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:21:58', 0, 'Minutes'),
	(43, '07 REPAIR INVESTIGATION', 'in_progress', '03bc03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-01-24 13:49:21', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-30 15:22:02', 0, 'Minutes'),
	(44, '00 APPRAISAL REQUIRED', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-02-11 11:44:28', 0, 'Minutes'),
	(45, '00 APPRAISAL COMPLETE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-02-11 11:44:48', 0, 'Minutes'),
	(46, '28 RESUBMIT CLAIM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-02-26 12:12:12', 0, 'Minutes'),
	(47, '99 Service Order Closed', 'closed', '03bc03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-02-26 12:12:12', 0, 'Minutes');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;


-- Dumping structure for function create_new.statustat
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `statustat`(jobID INT) RETURNS int(11)
    DETERMINISTIC
BEGIN

	DECLARE result INT;
	DECLARE serviceProviderID INT;
	DECLARE statusDate DATE;
	DECLARE statusTime TIME;
	DECLARE diff TIME;
	
	SET serviceProviderID = (SELECT job.ServiceProviderID FROM job WHERE job.JobID = jobID);
	SET statusDate = 	(
							SELECT 		CAST(DATE_FORMAT(status_history.Date, "%Y-%m-%d") AS DATE)
							FROM		job 
							LEFT JOIN	status_history ON job.JobID = status_history.JobID
							WHERE 		job.JobID = jobID AND status_history.StatusID = job.StatusID
							LIMIT		1
						);
	SET statusTime =	(
							SELECT 		CAST(DATE_FORMAT(status_history.Date, "%H:%i:%s") AS TIME)
							FROM		job 
							LEFT JOIN	status_history ON job.JobID = status_history.JobID
							WHERE 		job.JobID = jobID AND status_history.StatusID = job.StatusID
							LIMIT		1
						);

	SET @normalOpen := CAST((SELECT sp.WeekdaysOpenTime FROM service_provider AS sp WHERE sp.ServiceProviderID = serviceProviderID LIMIT 1) AS TIME);
	SET @normalClose := CAST((SELECT sp.WeekdaysCloseTime FROM service_provider AS sp WHERE sp.ServiceProviderID = serviceProviderID LIMIT 1) AS TIME);

	#normal working day in seconds
	SET @normalDay := TIME_TO_SEC(TIMEDIFF(@normalClose, @normalOpen));

	#SET @satOpen := CAST((SELECT sp.SaturdayOpenTime FROM service_provider AS sp WHERE sp.ServiceProviderID = serviceProviderID LIMIT 1) AS TIME);
	#SET @satClose := CAST((SELECT sp.SaturdayCloseTime FROM service_provider AS sp WHERE sp.ServiceProviderID = serviceProviderID LIMIT 1) AS TIME);

	#SET @sunOpen := CAST((SELECT sp.SundayOpenTime FROM service_provider AS sp WHERE sp.ServiceProviderID = serviceProviderID LIMIT 1) AS TIME);
	#SET @sunClose := CAST((SELECT sp.SundayCloseTime FROM service_provider AS sp WHERE sp.ServiceProviderID = serviceProviderID LIMIT 1) AS TIME);
	
	
	SET @tatType :=	(
						SELECT	status.StatusTATType 
						FROM 	status 
						WHERE 	status.StatusID = (SELECT job.StatusID FROM job WHERE job.JobID = jobID)
						LIMIT	1
					);
	
	SET @tatTemp :=	(
						SELECT	status.StatusTAT 
						FROM 	status 
						WHERE 	status.StatusID = (SELECT job.StatusID FROM job WHERE job.JobID = jobID)
						LIMIT	1
					);
	
	IF (@tatType = "Minutes") THEN SET @tatTime := @tatTemp * 60;
	ELSEIF (@tatType = "Hours") THEN SET @tatTime := @tatTemp * 3600;
	ELSEIF (@tatType = "Days") THEN SET @tatTime := @tatTemp * @normalDay;
	END IF;
	
	
	IF DATEDIFF(CURDATE(), statusDate) = 0
	
	THEN 
	
		SET result := 0;
		
	ELSE
	
		BEGIN

			IF ((SELECT COUNT(*) FROM national_bank_holiday AS nh WHERE nh.HolidayDate = statusDate) > 0)
			THEN 
				#bank holiday
				SET @firstDay := 0;
			/*	
			ELSEIF (DATE_FORMAT(statusDate, "%w") = 0)
			THEN
				BEGIN
					#sunday
					SET @firstDay := TIME_TO_SEC(TIMEDIFF(@sunClose, statusTime));
					#account for time difference if status was changed outside working hours
					IF (TIME_TO_SEC(TIMEDIFF(@sunOpen, statusTime)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(@sunOpen, statusTime))); END IF;
					IF (TIME_TO_SEC(TIMEDIFF(statusTime, @sunClose)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(statusTime, @sunClose))); END IF;
				END;
			ELSEIF (DATE_FORMAT(statusDate, "%w") = 6)
			THEN
				BEGIN
					#saturday 
					SET @firstDay := TIME_TO_SEC(TIMEDIFF(@satClose, statusTime));
					#account for time difference if status was changed outside working hours
					IF(TIME_TO_SEC(TIMEDIFF(@satOpen, statusTime)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(@satOpen, statusTime))); END IF;
					IF(TIME_TO_SEC(TIMEDIFF(statusTime, @satClose)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(statusTime, @satClose))); END IF;
				END;
			*/
			ELSE 
				BEGIN
					#working day
					SET @firstDay := TIME_TO_SEC(TIMEDIFF(@normalClose, statusTime));
					#account for time difference if status was changed outside working hours
					IF(TIME_TO_SEC(TIMEDIFF(@normalOpen, statusTime)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(@normalOpen, statusTime))); END IF;
					IF(TIME_TO_SEC(TIMEDIFF(statusTime, @normalClose)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(statusTime, @normalClose))); END IF;
				END;
			END IF;			
			
			IF ((SELECT COUNT(*) FROM national_bank_holiday AS nh WHERE nh.HolidayDate = CURDATE()) > 0)
			THEN 
				#bank holiday
				SET @lastDay := 0;
			/*	
			ELSEIF (DATE_FORMAT(CURDATE(), "%w") = 0)
			THEN
				BEGIN
					#sunday
					SET @lastDay := TIME_TO_SEC(TIMEDIFF(CURTIME(), @sunOpen));
					#account for time difference if status was changed outside working hours
					IF(TIME_TO_SEC(TIMEDIFF(@sunOpen, CURTIME())) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(@sunOpen, CURTIME()))); END IF;
					IF(TIME_TO_SEC(TIMEDIFF(CURTIME(), @sunClose)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(CURTIME(), @sunClose))); END IF;
				END;
			ELSEIF (DATE_FORMAT(CURDATE(), "%w") = 6)
			THEN 
				BEGIN
					#saturday
					SET @lastDay := TIME_TO_SEC(TIMEDIFF(CURTIME(), @satOpen));
					#account for time difference if status was changed outside working hours
					IF(TIME_TO_SEC(TIMEDIFF(@satOpen, CURTIME())) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(@satOpen, CURTIME()))); END IF;
					IF(TIME_TO_SEC(TIMEDIFF(CURTIME(), @satClose)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(CURTIME(), @satClose))); END IF;
				END;
			*/
			ELSE 
				BEGIN
					#working day
					SET @lastDay := TIME_TO_SEC(TIMEDIFF(CURTIME(), @normalOpen));
					#account for time difference if status was changed outside working hours
					IF(TIME_TO_SEC(TIMEDIFF(@normalOpen, CURTIME())) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(@normalOpen, CURTIME()))); END IF;
					IF(TIME_TO_SEC(TIMEDIFF(CURTIME(), @normalClose)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(CURTIME(), @normalClose))); END IF;
				END;
			END IF;			
						
			
			SET @diffSeconds := @firstDay + @lastDay;

						
			SET @daysDiff := DATEDIFF(CURDATE(), statusDate);
			
			IF @daysDiff > 1
			THEN
				BEGIN
					
					SET @curDay := 1;
					
					SET @seconds := 0;
					
					WHILE @curDay < @daysDiff DO
						
						SET @curDayDate := INTERVAL @curDay DAY + statusDate;
						
						IF ((SELECT COUNT(*) FROM national_bank_holiday AS nh WHERE nh.HolidayDate = @curDayDate) > 0)
						THEN 
							#bank holiday
							SET @diffSeconds := @diffSeconds;
						/*	
						ELSEIF (DATE_FORMAT(@curDayDate, "%w") = 0)
						THEN
							#sunday
							SET @diffSeconds := @diffSeconds + TIME_TO_SEC(TIMEDIFF(@sunClose, @sunOpen));
						ELSEIF (DATE_FORMAT(@curDayDate, "%w") = 6)
						THEN 
							#saturday
							SET @diffSeconds := @diffSeconds + TIME_TO_SEC(TIMEDIFF(@satClose, @satOpen));
						*/
						ELSE 
							#working day
							SET @diffSeconds := @diffSeconds + TIME_TO_SEC(TIMEDIFF(@normalClose, @normalOpen));
						END IF;			
						
						SET @curDay := @curDay + 1;
						
					END WHILE;
					
				END;
			END IF;
			
			
			IF (@diffSeconds > @tatTime) 
			THEN SET result = @daysDiff;
			ELSE SET result = -1;
			END IF;
			
		END;
		
	END IF;

	
	RETURN result;

END//
DELIMITER ;


-- Dumping structure for table create_new.status_history
CREATE TABLE IF NOT EXISTS `status_history` (
  `StatusHistoryID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) DEFAULT NULL,
  `StatusID` int(11) NOT NULL,
  `UserID` int(11) DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  `AdditionalInformation` varchar(40) DEFAULT NULL,
  `UserCode` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`StatusHistoryID`),
  KEY `IDX_status_history_StatusID_FK` (`StatusID`),
  KEY `IDX_status_history_JobID_FK` (`JobID`),
  KEY `IDX_status_history_UserID_FK` (`UserID`),
  CONSTRAINT `job_TO_status_history` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`),
  CONSTRAINT `status_TO_status_history` FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`),
  CONSTRAINT `user_TO_status_history` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.status_history: ~0 rows (approximately)
/*!40000 ALTER TABLE `status_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_history` ENABLE KEYS */;


-- Dumping structure for table create_new.status_permission
CREATE TABLE IF NOT EXISTS `status_permission` (
  `StatusPermissionID` int(11) NOT NULL AUTO_INCREMENT,
  `PermissionType` enum('user','role','network','client','brand','branch') NOT NULL,
  `EntityID` int(11) NOT NULL,
  `StatusID` int(11) NOT NULL,
  PRIMARY KEY (`StatusPermissionID`),
  KEY `status_permission_TO_status` (`StatusID`),
  CONSTRAINT `status_permission_TO_status` FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.status_permission: ~0 rows (approximately)
/*!40000 ALTER TABLE `status_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_permission` ENABLE KEYS */;


-- Dumping structure for table create_new.status_preference
CREATE TABLE IF NOT EXISTS `status_preference` (
  `UserID` int(11) NOT NULL,
  `StatusID` int(11) NOT NULL,
  `Page` varchar(40) NOT NULL,
  `Type` varchar(2) NOT NULL,
  UNIQUE KEY `IDX_status_preference_1` (`Page`,`UserID`,`Type`,`StatusID`),
  KEY `user_TO_status_preference` (`UserID`),
  CONSTRAINT `user_TO_status_preference` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.status_preference: ~0 rows (approximately)
/*!40000 ALTER TABLE `status_preference` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_preference` ENABLE KEYS */;


-- Dumping structure for table create_new.supplier
CREATE TABLE IF NOT EXISTS `supplier` (
  `SupplierID` int(10) NOT NULL AUTO_INCREMENT,
  `CompanyName` varchar(50) NOT NULL,
  `AccountNumber` varchar(50) DEFAULT NULL,
  `TelephoneNo` varchar(20) DEFAULT NULL,
  `PostCode` varchar(10) DEFAULT NULL,
  `FaxNo` varchar(13) DEFAULT NULL,
  `EmailAddress` varchar(255) DEFAULT NULL,
  `Website` varchar(255) DEFAULT NULL,
  `ContactName` varchar(50) DEFAULT NULL,
  `DirectOrderTemplateID` int(11) DEFAULT NULL,
  `CollectSupplierOrderNo` varchar(20) DEFAULT NULL,
  `PostageChargePrompt` enum('Yes','No') DEFAULT NULL,
  `DefaultPostageCharge` decimal(10,2) DEFAULT NULL,
  `DefaultCurrencyID` int(11) DEFAULT NULL,
  `NormalSupplyPeriod` int(11) DEFAULT NULL,
  `MinimumOrderValue` decimal(50,0) DEFAULT NULL,
  `UsageHistoryPeriod` int(50) DEFAULT NULL,
  `MultiplyByFactor` decimal(10,2) DEFAULT NULL,
  `TaxExempt` enum('Yes','No') DEFAULT NULL,
  `OrderPermittedVariancePercent` decimal(10,2) DEFAULT NULL,
  `Status` enum('Active','In-Active') NOT NULL DEFAULT 'Active',
  `BuildingNameNumber` varchar(50) DEFAULT NULL,
  `Street` varchar(100) DEFAULT NULL,
  `LocalArea` varchar(100) DEFAULT NULL,
  `TownCity` varchar(100) DEFAULT NULL,
  `CountryID` int(3) DEFAULT NULL,
  PRIMARY KEY (`SupplierID`),
  KEY `CompanyName` (`CompanyName`),
  KEY `AccountNumber` (`AccountNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.supplier: ~0 rows (approximately)
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;


-- Dumping structure for table create_new.table_state
CREATE TABLE IF NOT EXISTS `table_state` (
  `UserID` int(10) NOT NULL,
  `TableID` varchar(250) NOT NULL,
  `StateData` text NOT NULL,
  PRIMARY KEY (`UserID`),
  KEY `TableID` (`TableID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.table_state: ~0 rows (approximately)
/*!40000 ALTER TABLE `table_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `table_state` ENABLE KEYS */;


-- Dumping structure for table create_new.tat_defaults
CREATE TABLE IF NOT EXISTS `tat_defaults` (
  `TatID` int(11) NOT NULL AUTO_INCREMENT,
  `TatType` varchar(20) NOT NULL,
  `Button1` int(11) DEFAULT NULL,
  `Button2` int(11) DEFAULT NULL,
  `Button3` int(11) DEFAULT NULL,
  `Button4` int(11) DEFAULT NULL,
  `Button1Colour` varchar(10) DEFAULT NULL,
  `Button2Colour` varchar(10) DEFAULT NULL,
  `Button3Colour` varchar(10) DEFAULT NULL,
  `Button4Colour` varchar(10) DEFAULT NULL,
  `UserID` int(11) NOT NULL,
  PRIMARY KEY (`TatID`),
  UNIQUE KEY `TatType_2` (`TatType`,`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.tat_defaults: ~0 rows (approximately)
/*!40000 ALTER TABLE `tat_defaults` DISABLE KEYS */;
/*!40000 ALTER TABLE `tat_defaults` ENABLE KEYS */;


-- Dumping structure for table create_new.testimonials
CREATE TABLE IF NOT EXISTS `testimonials` (
  `CustomerID` int(11) NOT NULL,
  `ServiceProviderID` int(11) NOT NULL,
  `TestimonialText` text NOT NULL,
  `TestimonialUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.testimonials: ~0 rows (approximately)
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;


-- Dumping structure for table create_new.town_allocation
CREATE TABLE IF NOT EXISTS `town_allocation` (
  `TownAllocationID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) NOT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `RepairSkillID` int(11) DEFAULT NULL,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  `JobTypeID` int(11) DEFAULT NULL,
  `ServiceTypeID` int(11) DEFAULT NULL,
  `UnitTypeID` int(11) DEFAULT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `CountryID` int(11) DEFAULT NULL,
  `CountyID` int(11) NOT NULL,
  `Town` varchar(40) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`TownAllocationID`),
  KEY `IDX_town_allocation_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_town_allocation_NetworkID_FK` (`NetworkID`),
  KEY `IDX_town_allocation_ManufacturerID_FK` (`ManufacturerID`),
  KEY `IDX_town_allocation_RepairSkillID_FK` (`RepairSkillID`),
  KEY `IDX_town_allocation_ServiceProviderID_FK` (`ServiceProviderID`),
  KEY `IDX_town_allocation_ClientID_FK` (`ClientID`),
  KEY `IDX_town_allocation_JobTypeID_FK` (`JobTypeID`),
  KEY `IDX_town_allocation_ServiceTypeID_FK` (`ServiceTypeID`),
  KEY `IDX_town_allocation_UnitTypeID_FK` (`UnitTypeID`),
  KEY `IDX_town_allocation_CountryID_FK` (`CountryID`),
  KEY `IDX_town_allocation_CustomerID_FK` (`CustomerID`),
  KEY `IDX_town_allocation_CountyID_FK` (`CountyID`),
  CONSTRAINT `client_TO_town_allocation` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `country_TO_town_allocation` FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `county_TO_town_allocation` FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`),
  CONSTRAINT `customer_TO_town_allocation` FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`),
  CONSTRAINT `job_type_TO_town_allocation` FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`),
  CONSTRAINT `manufacturer_TO_town_allocation` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `network_TO_town_allocation` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `repair_skill_TO_town_allocation` FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`),
  CONSTRAINT `service_provider_TO_town_allocation` FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`),
  CONSTRAINT `service_type_TO_town_allocation` FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`),
  CONSTRAINT `unit_type_TO_town_allocation` FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`),
  CONSTRAINT `user_TO_town_allocation` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.town_allocation: ~0 rows (approximately)
/*!40000 ALTER TABLE `town_allocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `town_allocation` ENABLE KEYS */;


-- Dumping structure for function create_new.turnaround
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `turnaround`(jobID INT) RETURNS int(11)
    DETERMINISTIC
BEGIN

	DECLARE turnaround INT;

	SET turnaround = (
	
	    CASE

			WHEN 	(@unit :=	(
									SELECT	    uct.UnitTurnaroundTime
									FROM	    job AS j
									LEFT JOIN   product AS p ON j.ProductID = p.ProductID
									LEFT JOIN   model AS m ON j.ModelID = m.ModelID
									LEFT JOIN   unit_type AS ut ON p.UnitTypeID = ut.UnitTypeID OR 
										    	m.UnitTypeID = ut.UnitTypeID
									LEFT JOIN   unit_client_type AS uct 
										    	ON ut.UnitTypeID = uct.UnitTypeID AND 
										    	uct.ClientID = j.ClientID
									WHERE	    j.JobID = jobID
									LIMIT	    1
					    		)
					) IS NOT NULL

			THEN    @unit

			ELSE    CASE
			    		WHEN	(@clnt :=	(   
											    SELECT  DefaultTurnaroundTime AS dtt
											    FROM    client
											    WHERE   client.ClientID = (SELECT ClientID FROM job AS j WHERE j.JobID = jobID)
											)
				    			) IS NOT NULL

			    		THEN    @clnt

			    		ELSE    (
									SELECT  gen.Default
									FROM    general_default AS gen
									WHERE   gen.GeneralDefaultID = 1
							    )
					END

    	END
		    
	);

	RETURN turnaround;

END//
DELIMITER ;


-- Dumping structure for table create_new.unit_client_type
CREATE TABLE IF NOT EXISTS `unit_client_type` (
  `UnitClientTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `UnitTypeID` int(11) NOT NULL,
  `ClientID` int(11) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `UnitTurnaroundTime` int(2) DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UnitClientTypeID`),
  KEY `IDX_unit_client_type_ClientID_FK` (`ClientID`),
  KEY `IDX_unit_client_type_UnitTypeID_FK` (`UnitTypeID`),
  KEY `IDX_unit_client_type_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `client_TO_unit_client_type` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `unit_type_TO_unit_client_type` FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`),
  CONSTRAINT `user_TO_unit_client_type` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.unit_client_type: ~0 rows (approximately)
/*!40000 ALTER TABLE `unit_client_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `unit_client_type` ENABLE KEYS */;


-- Dumping structure for table create_new.unit_pricing_structure
CREATE TABLE IF NOT EXISTS `unit_pricing_structure` (
  `UnitPricingStructureID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  `UnitTypeID` int(11) DEFAULT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `CompletionStatusID` int(11) DEFAULT NULL,
  `JobSite` enum('field call','workshop') NOT NULL,
  `ServiceRate` decimal(10,2) DEFAULT '0.00',
  `ReferralFee` decimal(10,2) DEFAULT '0.00',
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UnitPricingStructureID`),
  KEY `IDX_unit_pricing_structure_NetworkID_FK` (`NetworkID`),
  KEY `IDX_unit_pricing_structure_ClientID_FK` (`ClientID`),
  KEY `IDX_unit_pricing_structure_UnitTypeID_FK` (`UnitTypeID`),
  KEY `IDX_unit_pricing_structure_ManufacturerID_FK` (`ManufacturerID`),
  KEY `IDX_unit_pricing_structure_CompletionStatusID_FK` (`CompletionStatusID`),
  KEY `IDX_unit_pricing_structure_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `client_TO_unit_pricing_structure` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `completion_status_TO_unit_pricing_structure` FOREIGN KEY (`CompletionStatusID`) REFERENCES `completion_status` (`CompletionStatusID`),
  CONSTRAINT `manufacturer_TO_unit_pricing_structure` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `network_TO_unit_pricing_structure` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `unit_type_TO_unit_pricing_structure` FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`),
  CONSTRAINT `user_TO_unit_pricing_structure` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.unit_pricing_structure: ~0 rows (approximately)
/*!40000 ALTER TABLE `unit_pricing_structure` DISABLE KEYS */;
/*!40000 ALTER TABLE `unit_pricing_structure` ENABLE KEYS */;


-- Dumping structure for table create_new.unit_type
CREATE TABLE IF NOT EXISTS `unit_type` (
  `UnitTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `RepairSkillID` int(11) DEFAULT NULL,
  `UnitTypeName` varchar(30) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ImeiRequired` enum('Yes','No') DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UnitTypeID`),
  UNIQUE KEY `IDX_unit_type_1` (`RepairSkillID`,`UnitTypeName`),
  KEY `IDX_unit_type_RepairSkillID_FK` (`RepairSkillID`),
  KEY `IDX_unit_type_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `repair_skill_TO_unit_type` FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`),
  CONSTRAINT `user_TO_unit_type` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.unit_type: ~0 rows (approximately)
/*!40000 ALTER TABLE `unit_type` DISABLE KEYS */;
INSERT IGNORE INTO `unit_type` (`UnitTypeID`, `RepairSkillID`, `UnitTypeName`, `CreatedDate`, `EndDate`, `Status`, `ImeiRequired`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, NULL, 'UNKNOWN', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(2, 1, 'TV AERIAL', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(3, 2, 'AMPLIFIER', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(4, 2, 'AUDIO', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(5, 2, 'AUDIO / DOCKING STATION', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(6, 2, 'COFFEE MACHINE', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(7, 2, 'DIGIBOX', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(8, 2, 'DIGITAL CAMCORDER', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(9, 2, 'DIGITAL PHOTO FRAME', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(10, 2, 'DIGITAL SLR CAMERA', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(11, 2, 'DIGITAL STILLS CAMERA', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(12, 2, 'FREEVIEW BOX', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-13 15:38:58'),
	(13, 2, 'DVD/BLU RAY', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(14, 2, 'DVD/HDD', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(15, 2, 'DVD/VCR', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(16, 2, 'GAMES CONSOLE', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(17, 2, 'HOME CINEMA', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(18, 2, 'LCD PROJECTOR', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(19, 2, 'LCD TV <32inch', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(20, 2, 'LCD TV 32INCH', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(21, 2, 'LCD TV/DVD COMBI <32INCH', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(22, 2, 'LCD TV/DVD COMBI 32INCH+', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(23, 2, 'LCD/PLASMA TV 33-46INCH', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(24, 2, 'LCD/PLASMA TV 47INCH+', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(25, 2, 'LCD/PLASMA TV 33-41INCH', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(26, 2, 'LCD/PLASMA TV 42INCH+', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(27, 2, 'MICROWAVE', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(28, 2, 'MP3 PLAYER', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-10-10 19:21:15'),
	(29, 2, 'PROJECTION TV', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(30, 2, 'PVR', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(31, 2, 'SATELLITE NAVIGATION SYSTEM', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-10-10 19:21:00'),
	(32, 3, 'TABLET', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-07 13:33:54'),
	(33, 2, 'WIRELESS DONGLE', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(34, 3, 'LAPTOP', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(35, 3, 'PC', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(36, 4, 'ELECTRIC COOKER', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-26 15:02:20'),
	(37, 4, 'FRIDGE', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(38, 4, 'FRIDGE/FREEZER', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(39, 4, 'WHITE GOODS', '2012-09-03 16:19:26', '2012-11-26 14:58:45', 'In-active', NULL, NULL, '2012-11-26 14:58:45'),
	(42, 2, 'PROJECTOR', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(46, 4, 'DISHWASHER', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(47, 4, 'FREEZER', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-09-03 16:19:26'),
	(66, 2, 'DIGITAL CAMERA', '2012-10-04 11:55:05', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-10-04 11:55:05'),
	(67, 2, 'BRIDGE CAMERA', '2012-10-10 19:19:59', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-10-10 19:19:59'),
	(68, 2, 'CAMCORDER', '2012-10-10 19:20:13', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-10-10 19:20:13'),
	(69, 2, 'COMPACT CAMERA', '2012-10-10 19:20:28', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-10-10 19:20:28'),
	(70, 2, 'SKY BOX', '2012-10-10 19:20:45', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-10-10 19:20:45'),
	(71, 2, 'iPOD', '2012-10-14 14:26:32', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-10-14 14:27:01'),
	(72, 4, 'BAIN MARIE', '2012-11-26 14:31:20', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-26 14:59:40'),
	(73, 4, 'CHILLER', '2012-11-26 14:31:32', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-26 14:59:55'),
	(74, 4, 'CONTACT GRILL', '2012-11-26 14:31:50', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-26 15:00:05'),
	(75, 4, 'DEEP FRYER', '2012-11-26 14:32:09', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-26 15:00:15'),
	(76, 4, 'DISHWASHER (BUILT IN)', '2012-11-26 14:37:00', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-26 14:55:18'),
	(77, 9, 'CCTV', '2012-11-26 14:55:31', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-26 14:55:31'),
	(78, 4, 'ELECTRIC GRIDDLE', '2012-11-26 14:56:11', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-26 14:56:11'),
	(79, 4, 'ELECTRIC OVEN', '2012-11-26 14:56:25', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-26 14:56:25'),
	(80, 4, 'FREEZER (BUILT IN)', '2012-11-26 14:56:50', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-26 14:56:50'),
	(81, 4, 'FRIDGE (BUILT IN)', '2012-11-26 14:57:07', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-26 14:57:07'),
	(82, 4, 'GAS COOKER', '2012-11-26 14:57:24', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-26 14:57:24'),
	(83, 4, 'GAS HOB', '2012-11-26 14:57:39', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-26 14:57:39'),
	(84, 4, 'INDUCTION COOKER', '2012-11-26 14:57:54', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-26 14:57:54'),
	(85, 4, 'INDUCTION HOB', '2012-11-26 14:58:07', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-26 14:58:07'),
	(86, 4, 'JUICER', '2012-11-26 14:58:19', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-26 14:58:19'),
	(87, 4, 'GAS OVEN', '2012-11-26 14:58:33', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-26 14:58:33'),
	(88, 2, 'LCD/PLASMA TV 70INCH+', '2012-11-26 14:59:19', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2012-11-26 14:59:19'),
	(89, 2, 'DVD/BLURAY', '2013-01-11 10:26:31', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2013-01-11 10:26:31'),
	(90, 4, 'WASHING MACHINE', '2013-01-11 13:53:23', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2013-01-11 13:54:46'),
	(91, 4, 'TUMBLE DRYER', '2013-01-11 14:20:05', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2013-01-11 14:20:05'),
	(92, 4, 'WASHER/DRYER', '2013-01-14 16:12:39', '0000-00-00 00:00:00', 'Active', NULL, NULL, '2013-01-14 16:12:58');
/*!40000 ALTER TABLE `unit_type` ENABLE KEYS */;


-- Dumping structure for table create_new.unit_type_accessory
CREATE TABLE IF NOT EXISTS `unit_type_accessory` (
  `UnitTypeID` int(11) NOT NULL,
  `AccessoryID` int(11) NOT NULL,
  UNIQUE KEY `UnitTypeID` (`UnitTypeID`,`AccessoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.unit_type_accessory: ~0 rows (approximately)
/*!40000 ALTER TABLE `unit_type_accessory` DISABLE KEYS */;
/*!40000 ALTER TABLE `unit_type_accessory` ENABLE KEYS */;


-- Dumping structure for table create_new.unit_type_manufacturer
CREATE TABLE IF NOT EXISTS `unit_type_manufacturer` (
  `UnitTypeManufacturerID` int(11) NOT NULL AUTO_INCREMENT,
  `UnitTypeID` int(11) NOT NULL,
  `ManufacturerID` int(11) NOT NULL,
  `ProductGroup` varchar(10) DEFAULT NULL,
  `CreatedDate` timestamp NULL DEFAULT NULL,
  `EndDate` timestamp NULL DEFAULT NULL,
  `Status` enum('Active','In-active') NOT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UnitTypeManufacturerID`),
  KEY `UnitTypeID` (`UnitTypeID`),
  KEY `ManufacturerID` (`ManufacturerID`),
  KEY `FK__user` (`ModifiedUserID`),
  CONSTRAINT `FK__manufacturer` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `FK__unit_type` FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`),
  CONSTRAINT `FK__user` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.unit_type_manufacturer: ~0 rows (approximately)
/*!40000 ALTER TABLE `unit_type_manufacturer` DISABLE KEYS */;
/*!40000 ALTER TABLE `unit_type_manufacturer` ENABLE KEYS */;


-- Dumping structure for procedure create_new.UpgradeSchemaVersion
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `UpgradeSchemaVersion`(p_version varchar(20))
BEGIN

  -- Verify database schema version for use in update scripts
  -- author     Brian Etherington <b.etherington@pccsuk.com>
  -- copyright  2012 PC Control Systems
  -- link       http://www.pccontrolsystems.com
  -- Version 1.0
  --
  -- Change History
  -- Date       Version   Author                  Reason
  -- ??/??/2012 1.0       Brian Etherington       Initial Version
  
    DECLARE bad_version CONDITION FOR SQLSTATE '99001';
    DECLARE current_version varchar(20);
    SELECT `VersionNo` from `version` order by `Updated` desc limit 0,1 into current_version;
    IF p_version <> current_version THEN
         SIGNAL bad_version
            SET MESSAGE_TEXT='Cannot Upgrade Schema.';
    END IF;
END//
DELIMITER ;


-- Dumping structure for table create_new.user
CREATE TABLE IF NOT EXISTS `user` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) DEFAULT NULL,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  `BranchID` int(11) DEFAULT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `ExtendedWarrantorID` int(11) DEFAULT NULL,
  `SecurityQuestionID` int(11) NOT NULL,
  `DefaultBrandID` int(11) DEFAULT NULL,
  `Username` varchar(20) DEFAULT NULL,
  `Password` varchar(32) DEFAULT NULL,
  `Answer` varchar(40) DEFAULT NULL,
  `Position` varchar(40) DEFAULT NULL,
  `SuperAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `LastLoggedIn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CustomerTitleID` int(11) DEFAULT NULL,
  `ContactFirstName` varchar(40) DEFAULT NULL,
  `ContactLastName` varchar(40) DEFAULT NULL,
  `ContactHomePhone` varchar(40) DEFAULT NULL,
  `ContactWorkPhoneExt` varchar(10) DEFAULT NULL,
  `ContactWorkPhone` varchar(40) DEFAULT NULL,
  `ContactFax` varchar(40) DEFAULT NULL,
  `ContactMobile` varchar(40) DEFAULT NULL,
  `ContactEmail` varchar(40) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `BranchJobSearchScope` enum('Branch Only','Brand','Client') DEFAULT 'Client',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GaDial0Yellow` int(11) NOT NULL DEFAULT '60',
  `GaDial0Red` int(11) NOT NULL DEFAULT '80',
  `GaDial1Yellow` int(11) NOT NULL DEFAULT '60',
  `GaDial1Red` int(11) NOT NULL DEFAULT '80',
  `GaDial2Yellow` int(11) NOT NULL DEFAULT '60',
  `GaDial2Red` int(11) NOT NULL DEFAULT '80',
  `GaDial3Yellow` int(11) NOT NULL DEFAULT '600',
  `GaDial3Red` int(11) NOT NULL DEFAULT '800',
  `GaDial3Max` int(11) NOT NULL DEFAULT '1000',
  `GaDial4Yellow` int(11) NOT NULL DEFAULT '600',
  `GaDial4Red` int(11) NOT NULL DEFAULT '800',
  `GaDial4Max` int(11) NOT NULL DEFAULT '1000',
  `GaDial5Yellow` int(11) NOT NULL DEFAULT '600',
  `GaDial5Red` int(11) NOT NULL DEFAULT '800',
  `GaDial5Max` int(11) NOT NULL DEFAULT '1000',
  `GaDialBoundaryLower` int(11) NOT NULL DEFAULT '7',
  `GaDialBoundaryUpper` int(11) NOT NULL DEFAULT '7',
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `IDX_user_Username` (`Username`),
  KEY `IDX_user_NetworkID_FK` (`NetworkID`),
  KEY `IDX_user_ServiceProviderID_FK` (`ServiceProviderID`),
  KEY `IDX_user_ClientID_FK` (`ClientID`),
  KEY `IDX_user_BranchID_FK` (`BranchID`),
  KEY `IDX_user_DefaultBrandID_FK` (`DefaultBrandID`),
  KEY `IDX_user_SecurityQuestionID_FK` (`SecurityQuestionID`),
  KEY `IDX_user_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_user_CustomerTitleID_FK` (`CustomerTitleID`),
  CONSTRAINT `branch_TO_user` FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`),
  CONSTRAINT `brand_TO_user` FOREIGN KEY (`DefaultBrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `client_TO_user` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `customer_title_TO_user` FOREIGN KEY (`CustomerTitleID`) REFERENCES `customer_title` (`CustomerTitleID`),
  CONSTRAINT `network_TO_user` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `security_question_TO_user` FOREIGN KEY (`SecurityQuestionID`) REFERENCES `security_question` (`SecurityQuestionID`),
  CONSTRAINT `service_provider_TO_user` FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`),
  CONSTRAINT `user_TO_user` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.user: ~0 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


-- Dumping structure for table create_new.user_role
CREATE TABLE IF NOT EXISTS `user_role` (
  `UserID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  PRIMARY KEY (`UserID`,`RoleID`),
  KEY `IDX_user_role_UserID_FK` (`UserID`),
  KEY `IDX_user_role_RoleID_FK` (`RoleID`),
  CONSTRAINT `role_TO_user_role` FOREIGN KEY (`RoleID`) REFERENCES `role` (`RoleID`),
  CONSTRAINT `user_TO_user_role` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.user_role: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;


-- Dumping structure for table create_new.vat_rate
CREATE TABLE IF NOT EXISTS `vat_rate` (
  `VatRateID` int(11) NOT NULL AUTO_INCREMENT,
  `Code` varchar(255) NOT NULL,
  `VatCode` int(11) NOT NULL,
  `VatRate` double(6,2) NOT NULL,
  `Status` enum('Active','In-active') NOT NULL,
  PRIMARY KEY (`VatRateID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='This table contains Vat Rate details.';

-- Dumping data for table create_new.vat_rate: ~0 rows (approximately)
/*!40000 ALTER TABLE `vat_rate` DISABLE KEYS */;
INSERT IGNORE INTO `vat_rate` (`VatRateID`, `Code`, `VatCode`, `VatRate`, `Status`) VALUES
	(1, 'T0', 1, 0.00, 'Active'),
	(2, 'T1', 2, 20.00, 'Active'),
	(3, 'T4', 3, 0.00, 'Active'),
	(4, 'T9', 4, 0.00, 'Active');
/*!40000 ALTER TABLE `vat_rate` ENABLE KEYS */;


-- Dumping structure for table create_new.version
CREATE TABLE IF NOT EXISTS `version` (
  `VersionNo` varchar(10) NOT NULL,
  `Updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`VersionNo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.version: ~38 rows (approximately)
/*!40000 ALTER TABLE `version` DISABLE KEYS */;
INSERT IGNORE INTO `version` (`VersionNo`, `Updated`) VALUES
	('1.190', '2013-02-25 11:02:11'),
	('1.191', '2013-04-15 16:09:08'),
	('1.192', '2013-04-15 16:09:09'),
	('1.193', '2013-04-15 16:09:10'),
	('1.194', '2013-04-15 16:09:11'),
	('1.195', '2013-04-15 16:09:14'),
	('1.196', '2013-04-15 16:09:15'),
	('1.197', '2013-04-15 16:09:16'),
	('1.198', '2013-04-15 16:09:17'),
	('1.199', '2013-04-15 16:09:18'),
	('1.200', '2013-04-15 16:09:20'),
	('1.201', '2013-04-15 16:09:21'),
	('1.202', '2013-04-15 16:09:22'),
	('1.203', '2013-04-15 16:09:23'),
	('1.204', '2013-04-15 16:09:24'),
	('1.205', '2013-04-15 16:09:27'),
	('1.206', '2013-04-15 16:09:28'),
	('1.207', '2013-04-15 16:09:29'),
	('1.208', '2013-04-15 16:09:30'),
	('1.209', '2013-04-15 16:09:31'),
	('1.210', '2013-04-15 16:09:32'),
	('1.211', '2013-04-15 16:09:33'),
	('1.212', '2013-04-15 16:09:35'),
	('1.213', '2013-04-15 16:09:36'),
	('1.214', '2013-04-15 16:09:37'),
	('1.215', '2013-04-15 16:09:40'),
	('1.216', '2013-04-15 16:09:42'),
	('1.217', '2013-04-15 16:09:43'),
	('1.218', '2013-04-15 16:09:45'),
	('1.219', '2013-04-15 16:09:47'),
	('1.220', '2013-04-15 16:09:50'),
	('1.221', '2013-04-15 16:09:52'),
	('1.222', '2013-04-15 16:09:55'),
	('1.223', '2013-04-15 16:09:56'),
	('1.224', '2013-04-15 16:09:57'),
	('1.225', '2013-04-15 16:10:01'),
	('1.226', '2013-04-15 16:10:07'),
	('1.227', '2013-04-15 16:10:10');
/*!40000 ALTER TABLE `version` ENABLE KEYS */;


-- Dumping structure for table create_new.viamente_end_day
CREATE TABLE IF NOT EXISTS `viamente_end_day` (
  `ViamenteEndDayID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `RouteDate` date DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ViamenteEndDayID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.viamente_end_day: ~0 rows (approximately)
/*!40000 ALTER TABLE `viamente_end_day` DISABLE KEYS */;
/*!40000 ALTER TABLE `viamente_end_day` ENABLE KEYS */;


-- Dumping structure for table create_new.welcome_messages
CREATE TABLE IF NOT EXISTS `welcome_messages` (
  `WelcomeMessageID` int(11) NOT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `WelcomeMessageDefaultsID` int(11) DEFAULT NULL,
  `JobID` int(11) DEFAULT NULL,
  `CreatedDateTime` datetime DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Status` enum('Active','In-active') DEFAULT 'Active',
  PRIMARY KEY (`WelcomeMessageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.welcome_messages: ~0 rows (approximately)
/*!40000 ALTER TABLE `welcome_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `welcome_messages` ENABLE KEYS */;


-- Dumping structure for table create_new.welcome_message_defaults
CREATE TABLE IF NOT EXISTS `welcome_message_defaults` (
  `WelcomeMessageDefaultsID` int(11) NOT NULL,
  `Title` text,
  `HyperlinkText` varchar(100) DEFAULT NULL,
  `Message` text,
  `Type` enum('Message','Alert') DEFAULT NULL,
  `CreatedDateTime` datetime DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` date DEFAULT NULL,
  PRIMARY KEY (`WelcomeMessageDefaultsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create_new.welcome_message_defaults: ~0 rows (approximately)
/*!40000 ALTER TABLE `welcome_message_defaults` DISABLE KEYS */;
/*!40000 ALTER TABLE `welcome_message_defaults` ENABLE KEYS */;


-- Dumping structure for trigger create_new.job_insert
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `job_insert` BEFORE INSERT ON `job` FOR EACH ROW BEGIN

	IF (NEW.StatusID = 33 OR NEW.StatusID = 41) AND NEW.ClosedDate IS NULL
	THEN SET NEW.ClosedDate = CURDATE();
	END IF;

	/*DateBooked*/
	CALL trigger_insert(NEW.DateBooked, 'job', 'DateBooked', NEW.JobID, NEW.ModifiedUserID);

END//
DELIMITER ;
SET SQL_MODE=@OLD_SQL_MODE;


-- Dumping structure for trigger create_new.job_update
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `job_update` BEFORE UPDATE ON `job` FOR EACH ROW BEGIN

	/*Insert closed date if Job Status is being changed to cancelled*/
	IF (NEW.StatusID = 33 OR NEW.StatusID = 41) AND NEW.ClosedDate IS NULL
	THEN SET NEW.ClosedDate = CURDATE();
	END IF;
	
	/*Insert Service Provider Despatch Date if Job Status is being changed to cancelled*/
	IF (NEW.StatusID = 33 OR NEW.StatusID = 41) AND NEW.ServiceProviderDespatchDate IS NULL
	THEN SET NEW.ServiceProviderDespatchDate = CURDATE();
	END IF;
	
	/*Insert RepairCompleteDate if doesn't exist when OpenJobStatus is changed to awaiting_collection, customer_notified or closed*/
	IF ((NEW.OpenJobStatus = 'awaiting_collection' OR NEW.OpenJobStatus = 'customer_notified' OR NEW.OpenJobStatus = 'closed') AND OLD.RepairCompleteDate IS NULL)
	THEN SET NEW.RepairCompleteDate = CURDATE();
	END IF;
	
	/*Insert ServiceProviderDespatchDate if doesn't exist when OpenJobStatus is changed to awaiting_collection, customer_notified or closed*/
	IF ((NEW.OpenJobStatus = 'awaiting_collection' OR NEW.OpenJobStatus = 'customer_notified' OR NEW.OpenJobStatus = 'closed') AND OLD.ServiceProviderDespatchDate IS NULL)
	THEN SET NEW.ServiceProviderDespatchDate = CURDATE();
	END IF;
	
	/*Insert DateReturnedToCustomer if doesn't exist when OpenJobStatus is changed to awaiting_collection, customer_notified or closed*/
	IF ((NEW.OpenJobStatus = 'awaiting_collection' OR NEW.OpenJobStatus = 'customer_notified' OR NEW.OpenJobStatus = 'closed') AND OLD.DateReturnedToCustomer IS NULL)
	THEN SET NEW.DateReturnedToCustomer = CURDATE();
	END IF;
	
	/*Insert DateReturnedToCustomer if doesn't exist when OpenJobStatus is changed to awaiting_collection, customer_notified or closed*/
	IF (NEW.OpenJobStatus = 'closed' AND OLD.DateReturnedToCustomer IS NULL)
	THEN SET NEW.DateReturnedToCustomer = CURDATE();
	END IF;
	
	/*Insert ClosedDate if doesn't exist when OpenJobStatus is changed to awaiting_collection, customer_notified or closed*/
	IF (NEW.OpenJobStatus = 'closed' AND OLD.ClosedDate IS NULL)
	THEN SET NEW.ClosedDate = CURDATE();
	END IF;
	
	
	/*Audit logging begins here*/

	/*Parameter definitions*/
	/*CALL trigger_update([old data], [new data], [table name], [field name], [primary ID], [modified user ID]);*/

	/*StatusID*/
	CALL trigger_update(OLD.StatusID, NEW.StatusID, 'job', 'StatusID', OLD.JobID, NEW.ModifiedUserID);
	
	/*ProductID*/
	CALL trigger_update(OLD.ProductID, NEW.ProductID, 'job', 'ProductID', OLD.JobID, NEW.ModifiedUserID);
		
	/*ServiceBaseModel*/
	CALL trigger_update(OLD.ServiceBaseModel, NEW.ServiceBaseModel, 'job', 'ServiceBaseModel', OLD.JobID, NEW.ModifiedUserID);

	/*ManufacturerID*/
	CALL trigger_update(OLD.ManufacturerID, NEW.ManufacturerID, 'job', 'ManufacturerID', OLD.JobID, NEW.ModifiedUserID);
							
	/*ServiceBaseUnitType*/
	CALL trigger_update(OLD.ServiceBaseUnitType, NEW.ServiceBaseUnitType, 'job', 'ServiceBaseUnitType', OLD.JobID, NEW.ModifiedUserID);
	
	/*RepairType*/
	CALL trigger_update(OLD.RepairType, NEW.RepairType, 'job', 'RepairType', OLD.JobID, NEW.ModifiedUserID);

	/*ServiceTypeID*/
	CALL trigger_update(OLD.ServiceTypeID, NEW.ServiceTypeID, 'job', 'ServiceTypeID', OLD.JobID, NEW.ModifiedUserID);

	/*SerialNo*/
	CALL trigger_update(OLD.SerialNo, NEW.SerialNo, 'job', 'SerialNo', OLD.JobID, NEW.ModifiedUserID);
	
	/*ProductLocation*/
	CALL trigger_update(OLD.ProductLocation, NEW.ProductLocation, 'job', 'ProductLocation', OLD.JobID, NEW.ModifiedUserID);

	/*DateOfPurchase*/
	CALL trigger_update(OLD.DateOfPurchase, NEW.DateOfPurchase, 'job', 'DateOfPurchase', OLD.JobID, NEW.ModifiedUserID);
	
	/*OriginalRetailer*/
	CALL trigger_update(OLD.OriginalRetailer, NEW.OriginalRetailer, 'job', 'OriginalRetailer', OLD.JobID, NEW.ModifiedUserID);
	
	/*ReceiptNo*/
	CALL trigger_update(OLD.ReceiptNo, NEW.ReceiptNo, 'job', 'ReceiptNo', OLD.JobID, NEW.ModifiedUserID);

	/*Insurer*/
	CALL trigger_update(OLD.Insurer, NEW.Insurer, 'job', 'Insurer', OLD.JobID, NEW.ModifiedUserID);

	/*PolicyNo*/
	CALL trigger_update(OLD.PolicyNo, NEW.PolicyNo, 'job', 'PolicyNo', OLD.JobID, NEW.ModifiedUserID);

	/*AuthorisationNo*/
	CALL trigger_update(OLD.AuthorisationNo, NEW.AuthorisationNo, 'job', 'AuthorisationNo', OLD.JobID, NEW.ModifiedUserID);

	/*AgentRefNo*/
	CALL trigger_update(OLD.AgentRefNo, NEW.AgentRefNo, 'job', 'AgentRefNo', OLD.JobID, NEW.ModifiedUserID);
	    				
	/*Notes*/
	CALL trigger_update(OLD.Notes, NEW.Notes, 'job', 'Notes', OLD.JobID, NEW.ModifiedUserID);
	
	/*ReportedFault*/
	CALL trigger_update(OLD.ReportedFault, NEW.ReportedFault, 'job', 'ReportedFault', OLD.JobID, NEW.ModifiedUserID);
	
	/*ChargeableLabourCost*/
	CALL trigger_update(OLD.ChargeableLabourCost, NEW.ChargeableLabourCost, 'job', 'ChargeableLabourCost', OLD.JobID, NEW.ModifiedUserID);

	/*ChargeableLabourVATCost*/
	CALL trigger_update(OLD.ChargeableLabourVATCost, NEW.ChargeableLabourVATCost, 'job', 'ChargeableLabourVATCost', OLD.JobID, NEW.ModifiedUserID);
	
	/*ChargeablePartsCost*/
	CALL trigger_update(OLD.ChargeablePartsCost, NEW.ChargeablePartsCost, 'job', 'ChargeablePartsCost', OLD.JobID, NEW.ModifiedUserID);

	/*ChargeableDeliveryCost*/
	CALL trigger_update(OLD.ChargeableDeliveryCost, NEW.ChargeableDeliveryCost, 'job', 'ChargeableDeliveryCost', OLD.JobID, NEW.ModifiedUserID);

	/*ChargeableDeliveryVATCost*/
	CALL trigger_update(OLD.ChargeableDeliveryVATCost, NEW.ChargeableDeliveryVATCost, 'job', 'ChargeableDeliveryVATCost', OLD.JobID, NEW.ModifiedUserID);
	
	/*ChargeableVATCost*/
	CALL trigger_update(OLD.ChargeableVATCost, NEW.ChargeableVATCost, 'job', 'ChargeableVATCost', OLD.JobID, NEW.ModifiedUserID);
	
	/*ChargeableSubTotal*/
	CALL trigger_update(OLD.ChargeableSubTotal, NEW.ChargeableSubTotal, 'job', 'ChargeableSubTotal', OLD.JobID, NEW.ModifiedUserID);
	
	/*ChargeableTotalCost*/
	CALL trigger_update(OLD.ChargeableTotalCost, NEW.ChargeableTotalCost, 'job', 'ChargeableTotalCost', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddCompanyName*/
	CALL trigger_update(OLD.ColAddCompanyName, NEW.ColAddCompanyName, 'job', 'ColAddCompanyName', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddBuildingNameNumber*/
	CALL trigger_update(OLD.ColAddBuildingNameNumber, NEW.ColAddBuildingNameNumber, 'job', 'ColAddBuildingNameNumber', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddStreet*/
	CALL trigger_update(OLD.ColAddStreet, NEW.ColAddStreet, 'job', 'ColAddStreet', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddLocalArea*/
	CALL trigger_update(OLD.ColAddLocalArea, NEW.ColAddLocalArea, 'job', 'ColAddLocalArea', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddTownCity*/
	CALL trigger_update(OLD.ColAddTownCity, NEW.ColAddTownCity, 'job', 'ColAddLocalArea', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddCountyID*/
	CALL trigger_update(OLD.ColAddCountyID, NEW.ColAddCountyID, 'job', 'ColAddCountyID', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddCountryID*/
	CALL trigger_update(OLD.ColAddCountryID, NEW.ColAddCountryID, 'job', 'ColAddCountryID', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddPostcode*/
	CALL trigger_update(OLD.ColAddPostcode, NEW.ColAddPostcode, 'job', 'ColAddPostcode', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddEmail*/
	CALL trigger_update(OLD.ColAddEmail, NEW.ColAddEmail, 'job', 'ColAddEmail', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddPhone*/
	CALL trigger_update(OLD.ColAddPhone, NEW.ColAddPhone, 'job', 'ColAddPhone', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddPhoneExt*/
	CALL trigger_update(OLD.ColAddPhoneExt, NEW.ColAddPhoneExt, 'job', 'ColAddPhoneExt', OLD.JobID, NEW.ModifiedUserID);

	/*Accessories*/
	CALL trigger_update(OLD.Accessories, NEW.Accessories, 'job', 'Accessories', OLD.JobID, NEW.ModifiedUserID);
	
	/*UnitCondition*/
	CALL trigger_update(OLD.UnitCondition, NEW.UnitCondition, 'job', 'UnitCondition', OLD.JobID, NEW.ModifiedUserID);
	
	/*ServiceProviderID*/
	CALL trigger_update(OLD.ServiceProviderID, NEW.ServiceProviderID, 'job', 'ServiceProviderID', OLD.JobID, NEW.ModifiedUserID);
	
	/*DateBooked*/
	CALL trigger_update(OLD.DateBooked, NEW.DateBooked, 'job', 'DateBooked', OLD.JobID, NEW.ModifiedUserID);
	
	/*OpenJobStatus*/
	CALL trigger_update(OLD.OpenJobStatus, NEW.OpenJobStatus, 'job', 'OpenJobStatus', OLD.JobID, NEW.ModifiedUserID);
	
END//
DELIMITER ;
SET SQL_MODE=@OLD_SQL_MODE;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
