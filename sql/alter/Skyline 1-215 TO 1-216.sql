# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.215');

# ---------------------------------------------------------------------- #
# Modify Table service_provider         	                             #
# ---------------------------------------------------------------------- #
ALTER TABLE service_provider ADD PublicWebsiteAddress VARCHAR(100) NULL, 
							 ADD GeneralManagerForename VARCHAR(50) NULL, 
							 ADD GeneralManagerSurname VARCHAR(50) NULL, 
							 ADD GeneralManagerEmail VARCHAR(100) NULL;

CREATE TABLE IF NOT EXISTS accessory (
										AccessoryID int(11) NOT NULL AUTO_INCREMENT, 
										AccessoryName varchar(60) DEFAULT NULL, 
										CreatedDate timestamp NOT NULL DEFAULT '0000-00-00 00:00:00', 
										EndDate timestamp NOT NULL DEFAULT '0000-00-00 00:00:00', 
										Status enum('Active','In-active') NOT NULL DEFAULT 'Active', 
										ModifiedUserID int(11) DEFAULT NULL, 
										ModifiedDate timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
										PRIMARY KEY (AccessoryID), 
										KEY IDX_unit_type_ModifiedUserID_FK (ModifiedUserID) 
									) 	ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS unit_type_accessory ( 
													UnitTypeID int(11) NOT NULL, 
													AccessoryID int(11) NOT NULL, 
													UNIQUE KEY UnitTypeID (UnitTypeID,AccessoryID) 
												) 	ENGINE=InnoDB DEFAULT CHARSET=latin1;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.216');
