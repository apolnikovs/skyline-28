# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.245');

# ---------------------------------------------------------------------- #
# Brian Shipping and Packing Changes                                     #
# ---------------------------------------------------------------------- # 
CREATE TABLE packaging_type (
PackagingTypeID int(11) NOT NULL AUTO_INCREMENT, 
CourierID int(11) NOT NULL, 
PackagingType varchar(30) NOT NULL, 
PRIMARY KEY (PackagingTypeID), 
KEY IDX_CourierID (CourierID) 
) ;

ALTER TABLE shipping ADD COLUMN PackagingType varchar(30) NULL AFTER ConsignmentNo;

ALTER TABLE recon ADD COLUMN PackagingType varchar(30) NULL AFTER ConsignmentNo;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.246');
