# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.135');


# ---------------------------------------------------------------------- #
# Modify table "appointment"                                             #
# ---------------------------------------------------------------------- #

ALTER TABLE `appointment`
	ADD COLUMN `CompletedBy` ENUM('Remote Engineer','Manual') NULL DEFAULT NULL AFTER `AppointmentEndTime2`;


# ---------------------------------------------------------------------- #
# Modify table "service_provider"                                        #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider`
	ADD COLUMN `DiaryAdministratorEmail` VARCHAR(255) NULL DEFAULT NULL AFTER `RunViamenteToday`;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.136');