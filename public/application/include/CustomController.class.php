<?php

require_once('Functions.class.php');

/**
 * Short Description of CustomController.
 * 
 * Long description of CustomController.
 *
 * @author     Brian Etherington <brian@smokingun.co.uk>
 * @copyright  2011 Smokingun Graphics
 * @link       http://www.smokingun.co.uk
 * @version    1.7
 * 
 *  
 * Changes
 * Date        Version Author                Reason
 * 23/01/2012  1.0     Brian Etherington     Initial Version
 * ??/??/????  1.1     Brian Etherington
 * 03/08/2012  1.2     Andrew J. Williams    Added ability to add prefix to log file
 * 22/08/2012  1.3     Brian Etherington     Added try/catch to read config to return false on file not found
 * 03/10/2012  1.4     Brian Etherington     Changed log display time from GMT to current locale setting
 * 05/11/2012  1.5     Brian Etherington     Changed log to var_export non-string argument for convenience
 * 05/11/2012  1.6     Brian Etherington     Changed loadModel to accept a path within models folder
 * 17/01/2013  1.7     Brian Etherington     Add development.ini to override any values in readConfig
 ******************************************************************************/

abstract class CustomController {
    
    // beginning of docblock template area
    /**#@+
     * @access protected
     */
    protected $salt = 'WashiMVC';
    /**#@-*/
    
    // beginning of docblock template area
    /**#@+
     * @access private
     */
    private $log_dir;
    private $config_dir;
    /**#@-*/
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     */
    public function __construct() {
                
        $this->log_dir = APPLICATION_PATH . '/logs/';
        $this->config_dir = APPLICATION_PATH . '/config/';

    }
   
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param string $model
     * @return CustomModel $model object
     */
    public function loadModel($model_path) {
        include_once(APPLICATION_PATH.'/models/'.$model_path.'.class.php');
        $elts = explode('/', $model_path);
        $model = $elts[count($elts)-1];
        return new $model($this);
    }
   
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param string $controller - controller name or URL
     * @param string $action
     * @param array $args
     */   
    protected function redirect($controller, $action=null, $args=null) { 
        
        // if $action is null treat $controller as a URL
        // and redirect browser....
        if (is_null($action)) {
            // TODO: pass $args on URL....
            if (defined('SUB_DOMAIN')) 
            {
                //header('Location: '.SUB_DOMAIN.'/'.$controller); 
                
                if(isset($_SERVER['HTTP_X_REQUESTED_WITH']))
                {
                  ?>

                  <script type="text/javascript" >
                   
                   
                    alert("Sorry, your session has been expired, please click on okay button to relogin. Thank you.");
                    window.location='<?php echo SUB_DOMAIN.'/'.$controller; ?>';
                    
                    
                    </script>
                    
                  <?php
                }
                else
                {    
                    header('Location: '.SUB_DOMAIN.'/'.$controller);
                }    
                
            } 
            else 
            {
                
                //header('Location: /'.$controller);
                if(isset($_SERVER['HTTP_X_REQUESTED_WITH']))
                {
                ?>
                    
                   <script type="text/javascript" >
                   
                        
                    alert("Sorry, your session has been expired, please click on okay button to relogin. Thank you.");
                    window.location='/<?php echo $controller; ?>';
                    
                    
                    </script>
                <?php
                }
                else
                {    
                    header('Location: /'.$controller);
                } 
                
            }
            exit;
        } 
        
        include_once(APPLICATION_PATH.'/controllers/'.$controller.'.class.php');
        $con =  new $controller();
        $con->$action( $args );
    }
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param string $time
     * @return MD5 digest
     */
    protected function getFormToken($time) {
       return md5($this->salt.$time);      
    }
   
    /**
     * Return a config file as an array
     * 
     * Read and parse an ini file format config file from 
     * the config folder and return the contents as an associative array.
     * 
     * If a file called development.ini exists in te same folder it will
     * be merged with and override values in the config file.
     * 
     * @param string $config Name of config file.
     * @param int $mode
     * @return array ini file
     */
    public function readConfig($config, $mode=INI_SCANNER_NORMAL) {
        
        $config_path = $this->config_dir.$config;
        
        try {
            $config =  parse_ini_file($config_path, true, $mode);
        } catch(Exception $e){
            return false;
           
        }
        
        // Check for development.ini file in same folder as $config_path
        $devini = pathinfo($config_path, PATHINFO_DIRNAME).'/development.ini';
        
        try {
            $dev =  parse_ini_file($devini, true);
        } catch(Exception $e){
            return $config;           
        }  

        return $this->MergeConfig($config, $dev);

    }

    /**
     * Write message to log file.
     * 
     * Write a message to the current log file with timestamp prefix.
     * 
     * @param string $message
     * @param string $tag       Prefix to add to log file name
     */    
    public function log($message, $tag = "") {
        
       // convert non-string datatypes to string...
       if (!is_string($message)) $message = var_export($message,true);
       
       // remove blank lines...
       while(strpos($message, "\n\n") !== false) {
           $message = str_replace("\n\n", "\n", $message);
       }
       
       $prefix = strftime('%H:%M:%S ',time()).Functions::getIPAddress();
       $indent = strlen($prefix)+1;

       $s = str_replace("\n", "\n".str_repeat(' ',$indent), $message);
       
       file_put_contents($this->log_dir.$tag.date('Y-m-d').'.txt',
               "$prefix $s\n",FILE_APPEND);
       
    }
    
     /**
     * Merge 2 config arrays.
     * 
     * Overwrite values in first array with values from 2nd array
     * or add new vakues from 2nd array where thay do not exist in 1st array.
     * 
     * @param array 1st config array
     * @param array 2nd config array
     * @return array merged array
     */
    private function MergeConfig($config1, $config2) {
        foreach($config2 as $key => $Value) {
            if(array_key_exists($key, $config1) && is_array($Value))              
                $config1[$key] = $this->MergeConfig($config1[$key], $Value);
            else
                $config1[$key] = $Value;
        }

        return $config1;

    }
   
}
